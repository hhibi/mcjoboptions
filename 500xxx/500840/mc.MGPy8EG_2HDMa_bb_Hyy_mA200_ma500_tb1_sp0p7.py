#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------
THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] = 1.0 # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427. 
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.7 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['MXd'] = 10 # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.THDMparams['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.
THDMparams['mh2'] = 200 # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = 200 # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mhc'] = 200 # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh4'] = 500 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['MB'] = 0.0 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

initialGluons = False # Determines initial state to generate from. True --> top loop induced production (gluon Fusion). False --> b b~ -initiated production (b-anti-b annihilation). See Sections 5.4 and 6.5 of arXiv:1701.07427 for further details

# Reweighting in tanb and sinp
reweight = False # Only save weights for b-initiated samples - for ggF reweighting is broken
reweights=[
'SINP_0.1-TANB_1.0',
'SINP_0.2-TANB_1.0',
'SINP_0.3-TANB_1.0',
'SINP_0.4-TANB_1.0',
'SINP_0.5-TANB_1.0',
'SINP_0.6-TANB_1.0',
'SINP_0.7-TANB_1.0',
'SINP_0.8-TANB_1.0',
'SINP_0.9-TANB_1.0',
]

# Define that we want H->yy decays
decayChannel="monoHyy"

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob=10000

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------    
include("MadGraphControl_Py8EG_2HDMa_monoH_common.py")

