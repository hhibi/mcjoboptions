model="LightVector"
fs = "ee"
mDM1 = 250.
mDM2 = 1000.
mZp = 500.
mHD = 125.
filteff = 9.865825e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
