import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':303600, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[303600], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':[14000,25300,42560,61200,91700], # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

import fileinput
import subprocess
from AthenaCommon import Logging
JOlog = Logging.logging.getLogger('FxFxJobOption')
JOlog.info(" Loading MadGraphControl file for tt012 FxFx ")

# General settings
evgenConfig.nEventsPerJob = 2000
nevents = runArgs.maxEvents*5.0 if runArgs.maxEvents>0 else 5.0*evgenConfig.nEventsPerJob
gridpack_mode = True
gridpack_dir  = 'madevent/'

# MG Particle cuts                                                                                                
mllcut=0
# Shower/merging settings                                                                                                
maxjetflavor=5
parton_shower='PYTHIA8'

msdecay   = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all al"
nleptons  = -1
#nevents  = 4.0*nevents # Filter efficiency

JOlog.info(" MadSpin decay defined as "+msdecay)

name='ttbarjets'
keyword=['ttbar','jets','FxFx','SM']

if not is_gen_from_gridpack():
    process = """                     
    import model loop_sm-no_b_mass
    define p = p b b~
    define j = p
    generate p p > t t~ [QCD] @0
    add process p p > t t~ j [QCD] @1
    add process p p > t t~ j j [QCD] @2
    output -f """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default LO run_card.dat and set parameters                                                                                       
settings = { 'event_norm' : 'bias',
             'nevents'       :int(nevents),
             'maxjetflavor'  : maxjetflavor,
             'parton_shower' : parton_shower,
             'mll_sf'        : mllcut,
             'mll'           : mllcut,
             'ickkw'         :  3,
             'jetradius'     :  1.0,
             'ptj'           :  10,
             'etaj'          :  5,
             'isoEM'         :'True'}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# This naming is enforced by MadGraph when running from a gridpack.                                                                                    
input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz' 

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
fMadSpinCard = open(madspin_card,'w')
fMadSpinCard.write('import '+input_events+'\n')
fMadSpinCard.write('set ms_dir '+process_dir+'/MadSpin\n')                        
fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')        
fMadSpinCard.write('''set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
%s                                                    
launch'''%(msdecay))
fMadSpinCard.close()  

paramToCopy      = 'aMcAtNlo_param_card_loop_sm-no_b_mass.dat'
paramDestination = process_dir+'/Cards/param_card.dat'
paramfile        = subprocess.Popen(['get_files','-data',paramToCopy])
paramfile.wait()

if not os.access(paramToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(paramToCopy))
shutil.copy(paramToCopy,paramDestination)

scaleToCopy      = 'cuts_ttbar_HT.f'
scaleDestination = process_dir+'/SubProcesses/cuts.f'
scalefile        = subprocess.Popen(['get_files','-data',scaleToCopy])
scalefile.wait()

if not os.access(scaleToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(scaleToCopy))
shutil.copy(scaleToCopy,scaleDestination)

generate(required_accuracy=0.001,process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Helper for resetting process number
check_reset_proc_number(opts)

# Shower
evgenConfig.description    = 'aMcAtNlo_ttbar_fxfx control'
evgenConfig.contact        = ['francesco.giuli@cern.ch']
evgenConfig.keywords+=keyword
runArgs.inputGeneratorFile=outputDS
evgenConfig.inputconfcheck='MGPy8EG_A14NNPDF23LO_ttbarjets_FxFx'

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# Matching settings
genSeq.Pythia8.Commands += ["JetMatching:merge           = on",
                            "JetMatching:scheme          = 1",
                            "JetMatching:setMad          = off",
                            "SpaceShower:rapidityOrder = off",
                            "SpaceShower:pTmaxFudge = 1.0",
                            "JetMatching:qCut            = 30",
                            "JetMatching:coneRadius      = 1.0",
                            "JetMatching:etaJetMax       = 10.0",
                            "JetMatching:doFxFx          = on",
                            "JetMatching:qCutME          = 10.0",
                            "JetMatching:nJetMax         = 2",
                            "SpaceShower:alphaSuseCMW    = on",
                            "SpaceShower:alphaSorder     = 2",
                            "TimeShower:alphaSuseCMW     = on",
                            "TimeShower:alphaSorder      = 2",
                            "PDF:pSet                    = LHAPDF6:NNPDF31_nnlo_as_0118",
                            "SpaceShower:alphaSvalue    = 0.118",
                            "TimeShower:alphaSvalue     = 0.118"
                             ]

# Bit more logging info
JOlog.info("JetMatching:qCut   -> 30 GeV")
JOlog.info("SpaceShower:alphaSvalue   -> 0.118")
JOlog.info("TimeShower:alphaSvalue   -> 0.118")
JOlog.info("JetMatching:qCutME -> 10 GeV")
JOlog.info("aMcAtNlo:ptj       -> 10 GeV")

# --------------------------------------------------------------
# Apply TTbarWToLeptonFilter
# --------------------------------------------------------------
# Primarily needed for non-all hadronic decays
# nleptons == -2 means inclusive decay ergo no filtering on leptons

if nleptons != -2:
    include("GeneratorFilters/TTbarWToLeptonFilter.py")
    filtSeq.TTbarWToLeptonFilter.NumLeptons = nleptons
    filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0
