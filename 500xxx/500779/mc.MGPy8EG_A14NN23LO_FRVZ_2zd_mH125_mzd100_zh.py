mH = 125
mfd2 = 5
mfd1 = 2
mZd = 100
nGamma = 2
avgtau = 15
decayMode = 'electrons'
include("MadGraphControl_A14N23LO_FRVZdisplaced_zh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
