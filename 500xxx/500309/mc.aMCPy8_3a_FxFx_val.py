#--------------------------------------------------------------                                                                                               
# EVGEN configuration                                                                                                                                         
#--------------------------------------------------------------                                                                                               

evgenConfig.description = "MadGraph+Pythia8 samples for tri-photon production at NLO"
evgenConfig.keywords = ["SM","4photon"]
evgenConfig.contact = ["amoroso@cern.ch","ana.cueto@cern.ch" ]
evgenConfig.generators = ["aMcAtNlo","Pythia8"]
evgenConfig.nEventsPerJob = 10000


# --------------------------------------------------------------                                                                                              
# Generate events                                                                                                                                             
# --------------------------------------------------------------                                                                                              
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*2 if runArgs.maxEvents>0 else 2*evgenConfig.nEventsPerJob

gridpack_mode=True


if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > a a a [QCD] @0
    add process p p > a a a j [QCD] @1
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


#Fetch default LO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8', 
            'req_acc':0.001,
            'ickkw':3,
            'ptgmin':15,
            'etagamma':2.7,
            'R0gamma':0.1,
            'xn':2.0,
            'epsgamma':0.1,
            'isoEM':True,

            'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
#modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor','cluster_queue':'workday'})




generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here

# --------------------------------------------------------------                                                                                              
# Shower settings                                                                                                                                             
# --------------------------------------------------------------                                                                                              




include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                                                                                                  
include("Pythia8_i/Pythia8_aMcAtNlo.py")
PYTHIA8_nJetMax=1
PYTHIA8_qCut=25.


print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut


genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = %f"%PYTHIA8_qCut,
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx        = on",
                            "JetMatching:qCutME        = 10.0",
                            "JetMatching:nJetMax       = %i"%PYTHIA8_nJetMax ]

genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS = True
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
#theApp.finalize()
#theApp.exit()

