import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
	'central_pdf':247000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
	'pdf_variations':[247000], # list of pdfs ids for which all variations (error sets) will be included as weights
	'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
	'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
			    }

from MadGraphControl.MadGraphUtils import *

mode=0

gridpack_mode=False
gridpack_dir=None
#gridpack_dir='madevent/'

# Set parameters based on physics short
#from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
#phys_short = get_physics_short()
phys_short = jofile
Mass = float(phys_short.split('_')[3])
if phys_short.split('_')[5][0] == 'M':
   fw = -1.0*float(phys_short.split('_')[5].lstrip('M'))
else:
   fw = float(phys_short.split('_')[5])

if phys_short.split('_')[6][0] == 'M':
   fww = -1.0*float(phys_short.split('_')[6].lstrip('M'))
else:
   fww = float(phys_short.split('_')[6])

if Mass < 100.0:
         print 'ERROR: mass is not recognized'
else:
   print 'parameters: mass= %d, fw= %s, fww= %s' % (Mass,fw,fww)
#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in HH model 1000,0
#---------------------------------------------------------------------------------------------------
parameters={
    'dim6coeff':{ 
        'rhoH':  '5.000000e-02',
        'Lambda':  '5.000000e+03',
        'fW':  fw,
	  'fWW':  fww,
	  'fB':  '0.000000e+00',
	  'fBB':  '0.000000e+00'}
    }
#---------------------------------------------------------------------------------------------------
# Setting X0 mass and width for param_card.dat
#---------------------------------------------------------------------------------------------------
resonanceMass = Mass
HHMass  = {'254':'%e # MHH'%resonanceMass}  #Mass
HHDecay = {'254':'DECAY 254 Auto # WHH'} #Width


#---------------------------------------------------------------------------------------------------
# Generating pp -> VX0, X0 -> VV in HC LO model
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model SMwithHeavyScalarDim4Dim6_NoDecay_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define v = z w- w+
generate p p > hh > v v v
output -f""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)


#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 7.0
nevents    = 10000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor



#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------

build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat',masses=HHMass,decays=HHDecay,params=parameters)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------
extras = {'lhe_version'   : '3.0', 
          'cut_decays'    : 'F'}

#'use_syst'      : 'True',
#'pdlabel'       : "'lhapdf'",
#'lhaid'         : '247000',
#'sys_scalefact' : '1.0 0.5 2.0',
#'sys_pdf'       : 'NNPDF23_lo_as_0130_qed'
#extras["dynamical_scale_choice"]=3
#'parton_shower' : 'PYTHIA8'
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat',
               nevts=nevents,
               rand_seed=runArgs.randomSeed,
               beamEnergy=beamEnergy,
               scalefact=1.0,
               alpsfact=1.0,
               extras=extras)



madspin_card_loc = 'madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************                                                                               

#*                        MadSpin                           *                                                                                                              
#*                                                          *                                                                                                              
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                                                                                                              
#*                                                          *                                                                                                              
#*    Part of the MadGraph5_aMC@NLO Framework:              *                                                                                                              
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                                                                                                              
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                                                                                                              
#*                                                          *                                                                                                              
#************************************************************                                                                                                              
#Some options (uncomment to apply)                                                                                                                                         
#                                                                                                                                                                          
# set seed 1                                                                                                                                                               
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight                                                                                     
# set BW_cut 15                # cut on how far the particle can be off-shell                                                                                              
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event                                                                                        
#                                                                                                                                                                          
set seed %i                                                                                                                                                                
# specify the decay for the final state particles                                                                                                                          
# decay t > w+ b, w+ > all all                                                                                                                                               
# decay z > all all                                                                                                                                                          
decay z > all all                                                                                                                                                         
decay w- > all all                                                                                                                                                    
decay w+ > all all                                                                                                                                                       
# running the actual code                                                                                                                                                  
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

print_cards()


runName='run_01'

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed)

outputDS=arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)

#######
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
   print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
   njobs = os.environ.pop('ATHENA_PROC_NUMBER')
   #Try to modify the opts underfoot
   if not hasattr(opts,'nprocs'): print 'Did not see option!'
   else: opts.nprocs = 0
   print opts


#### Shower

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'pp->VHH, HH->VV with LO HH model'
evgenConfig.keywords+=['BSM', "Higgs"]
evgenConfig.contact = ['Xin Chen <xin.chen@cern.ch>']
runArgs.inputGeneratorFile=outputDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

### Set lepton filters
if not hasattr(filtSeq, "MultiLeptonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
   lepfilter = MultiLeptonFilter("lepfilter")
   filtSeq += lepfilter

filtSeq.lepfilter.Ptcut = 10000.0 #MeV
filtSeq.lepfilter.Etacut = 2.7
filtSeq.lepfilter.NLeptons = 2 #minimum
