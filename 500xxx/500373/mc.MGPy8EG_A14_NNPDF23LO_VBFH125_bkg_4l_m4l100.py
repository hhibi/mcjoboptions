evgenConfig.inputconfcheck="bkg_4l"
evgenConfig.nEventsPerJob = 2000

proc_name="VBF4l_B"
m4lmin="100"
m4lmax=None

include("MadGraphControl_Pythia8EvtGen_lllljj_EW6.py")
