evgenConfig.nEventsPerJob=5000

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os, re, subprocess, shutil

### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]

jobConfig=jofiles[0]


###################
### setupEFT.py
###################

include("MadGraphControl/setupEFT.py")

###################
### ZZ4l setup
###################

# create dictionary of processes this JO can create
definitions="""define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define l+ = e+ mu+
define l- = e- mu-
"""
processes={'4e':'generate p p > e+ e- e+ e- EFTORDER\n',
           '2e2mu':'generate p p > e+ e- mu+ mu- EFTORDER\n',
           '4mu':'generate p p > mu+ mu- mu+ mu- EFTORDER\n',
           'llll':'generate p p > l+ l- l+ l- EFTORDER\n',
}
for p in processes:
    processes[p]=definitions+processes[p]

# define cuts
extras={
        'ptl':'4', #min charged lepton pt
        'etal':'3.0', #max charged lepton abs eta
        'drll':'0.03', #min dr charged leptons
}

# general settings
run_mode=0 # 0: single core, 1: cluster, 2: multicore
njobs=1
nevents=10000
runName='run_01'
if evgenConfig.nEventsPerJob >0:
    nevents=int(evgenConfig.nEventsPerJob*1.10)
beamEnergy = runArgs.ecmEnergy / 2.  


# get EFT cards, determined from 4 last blocks of JO name
proc_card,param_card,reweight_card=create_SMEFT_cards(jobConfig,processes) 

# create and run process (using proc, param, and reweight cards provided by helper function)
process_dir = new_process(proc_card)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
 
generate(run_card_loc='run_card.dat',param_card_loc=param_card,reweight_card_loc=reweight_card,mode=run_mode,proc_dir=process_dir,run_name=runName,njobs=njobs)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)

# add meta data
runArgs.inputGeneratorFile = runName+'._00001.events.tar.gz'
evgenConfig.description = 'llll production with SMEFTSIM'
evgenConfig.keywords+=['diboson']
evgenConfig.inputfilecheck = runName
evgenConfig.contact = ['Hannes Mildner <hannes.mildner@cern.ch>', 'Lailin Xu <lailin.xu@cern.ch>']

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in jobConfig:
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']
