evgenConfig.inputconfcheck="sbi5_4l"
evgenConfig.nEventsPerJob = 2000

proc_name="VBF4l_SBI5"
m4lmin="130"
m4lmax=None

include("MadGraphControl_Pythia8EvtGen_lllljj_EW6.py")
