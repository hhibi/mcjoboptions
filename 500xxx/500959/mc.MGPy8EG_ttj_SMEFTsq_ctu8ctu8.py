from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphParamHelpers import *

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------

evgenConfig.contact = [ 'alexander.basan@cern.ch', 'baptiste.ravina@cern.ch', 'peter.berta@cern.ch' ]
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------

process='''
import model SMEFTatNLO_U2_2_U3_3_cG_4F_LO_UFO-LO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > t t~ j QCD=3 QED=0 NP=2 NP^2==4
output -f'''

process_dir = new_process(process)

# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = 2.5* nevents # account for filter efficiency
settings = {
    'nevents':nevents,
    'maxjetflavor': '5',
    'ptheavy':'300.0',
    'ptj':'70.0',
    'fixed_ren_scale' : 'True',
    'fixed_fac_scale' : 'True',
    'scale':'172.5',
    'dsqrt_q2fact1' : '172.5',
    'dsqrt_q2fact2' : '172.5',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'
modify_param_card(process_dir=process_dir,params=params)

# Set SMEFT@NLO parameters
params = dict() 
params['dim6'] = dict()
params['dim62f'] = dict()
params['dim64f'] = dict()
params['dim64f2l'] = dict()
params['dim64f4l'] = dict()
for i in range(2,11) :
    params['dim6'][str(i)] = '1.000000e-10'
for i in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,19,22,23,24] :
    params['dim62f'][str(i)] = '1.000000e-10'
for i in [1,2,3,4,6,7,8,10,11,12,13,14,16,17,19,20,21,23,25] :
    params['dim64f'][str(i)] = '1.000000e-10'
for i in [1,2,3,4,5,6,7,8,9,10,15,16,17,18,19,22,23,24] :
    params['dim64f2l'][str(i)] = '1.000000e-10'
for i in range(1,10) :
    params['dim64f4l'][str(i)] = '1.000000e-10'
params['dim64f']['7'] = '1.000000e+00'
params['dim64f']['7'] = '1.000000e+00'
modify_param_card(process_dir=process_dir,params=params)

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
evgenConfig.description = 'aMcAtNlo_ttbar'
evgenConfig.keywords+=['ttbar','jets']
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_aMcAtNlo.py')

# --------------------------------------------------------------
# Apply TTbarWToLeptonFilter
# --------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0
