model="LightVector"
fs = "mumu"
mDM1 = 5.
mDM2 = 1030.
mZp = 1000.
mHD = 125.
filteff = 8.879418e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
