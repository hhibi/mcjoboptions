#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'aMC@NLO+MadSpin+Herwig7.1.6+EvtGen single-top-quark s-channel (2->3) production, ME NNPDF3.05f NLO, H7.1 Default tune'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'olga.bylund@cern.ch','baptiste.ravina@cern.ch' ]
evgenConfig.generators += ["aMcAtNlo", "Herwig7", "EvtGen"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1


#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
""")

# run Herwig7
Herwig7Config.run()
