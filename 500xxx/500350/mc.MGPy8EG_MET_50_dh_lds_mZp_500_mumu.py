model="darkHiggs"
fs = "mumu"
mDM1 = 5.
mDM2 = 5.
mZp = 500.
mHD = 125.
filteff = 8.350731e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
