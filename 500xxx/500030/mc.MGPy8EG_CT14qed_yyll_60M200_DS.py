import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':13300, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[26500,26200,26000,26300,82200,245800,13300,82350,20463], # pdfs     generate a a > l+ l- for which all variations (error sets) will be included as weights
#    'alternative_pdfs':[82200], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=15*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
mode=0

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = a g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate a a > l+ l-
output -f""")
fcard.close()

runName='run_01'
name = "yyWW"
stringy = 'madgraph.'+str(runArgs.jobConfig)[-8:-2] +'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")
    


#Fetch default LO run_card.dat and set parameters
extras = {
    'cut_decays' :'T', 
    'mmll' : '60',
    'mmllmax' : '200',
    'lpp1'      : '1',
    'lpp2'      : '2'
}


process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  


evgenConfig.generators = ["MadGraph"]

############################
# Shower JOs will go here
evgenConfig.description = 'MadGraphPy8EG_yyWW'
evgenConfig.keywords+=["SM", "Z"]
evgenConfig.inputfilecheck = runName
evgenConfig.contact     = [ 'Kristin.Lohwasser@cern.ch' ]
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_DS_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")
    
include("Pythia8_i/Pythia8_MadGraph.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
chtrkfilter = ChargedTracksFilter("ChargedTracksFilter")
chtrkfilter.NTracks=-1
chtrkfilter.NTracksMax=20
chtrkfilter.Ptcut=500.
chtrkfilter.Etacut=2.5

filtSeq += chtrkfilter
