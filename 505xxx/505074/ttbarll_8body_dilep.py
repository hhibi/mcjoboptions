from MadGraphControl.MadGraphUtils import *

# PDF base fragment
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

evgenConfig.nEventsPerJob=30000

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

doProc=True
gridpack=True

shortname = get_physics_short()
if 'dilep_ee' in shortname:
    dilstring = ['e+ e-']
    dilshort = 'ee'
elif 'dilep_mm' in shortname:
    dilstring = ['mu+ mu-']
    dilshort = 'mm'
elif 'dilep_tt' in shortname:
    dilstring = ['ta+ ta-']
    dilshort = 'tt'
else:
    raise ValueError("Can't figure out what to generate")

coreprocesses = ''
for dil in dilstring:
    coreprocesses += """
add process p p > t t~ > e+ ve b e- ve~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > e+ ve b mu- vm~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > e+ ve b ta- vt~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > mu+ vm b e- ve~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > mu+ vm b mu- vm~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > mu+ vm b ta- vt~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > ta+ vt b e- ve~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > ta+ vt b mu- vm~ b~ %(dil)s / h QCD=2 QED=6
add process p p > t t~ > ta+ vt b ta- vt~ b~ %(dil)s / h QCD=2 QED=6
""" % {'dil': dil}

coreprocesses.replace('add process', 'generate', 1)

process="""
import model sm-lepton_masses
# in the SM model CKM matrix is diagonal
define l+ = e+ mu+ ta+
define l- = l- mu- ta-
%s
output -f""" % (coreprocesses,)

import os
process_dir = new_process(process) if doProc else os.path.join(os.getcwd(), 'PROC_sm-lepton_masses_0')

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0',
             'cut_decays':'T',
             'ptj':'0.0',
             'ptl':'0.0',
             'etaj':'-1.0',
             'etab':'-1.0',
             'etal':'-1.0',
             'drjj':'0.0',
             'drll':'0.0',
             'drjl':'0.0',
             'mmll':'1.0',
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})

generate(process_dir=process_dir,runArgs=runArgs, grid_pack=gridpack)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  
runArgs.inputGeneratorFile=outputDS

evgenConfig.generators = ["MadGraph"]

############################
# Shower JOs will go here
evgenConfig.description = 'MadGraph ttbar+%s 8 body LO production, dilepton' % dilshort
evgenConfig.keywords+=['top','ttbar','ttZ']
evgenConfig.contact=['peter.onyisi@cern.ch']
#evgenConfig.inputfilecheck = runName
#runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

