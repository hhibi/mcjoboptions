THDMparams = {}
THDMparams["GPXD"] = 1.0
THDMparams["TANBETA"] = 1.0
THDMparams["SINBMA"] = 1.0
THDMparams["LAM3"] = 3.0
THDMparams["LAP1"] = 3.0
THDMparams["LAP2"] = 3.0
THDMparams["SINP"] = 0.35
THDMparams["MXD"] = 10
THDMparams["MH2"] = 2000
THDMparams["MH3"] = 2000
THDMparams["MHC"] = 2000
THDMparams["MH4"] = 300
THDMparams["MB"] = 0.0

initialGluons = True
reweight = True
reweights=[
           "SINP_0.35-TANB_0.3",
           "SINP_0.35-TANB_0.5",
           "SINP_0.35-TANB_1.0",
           "SINP_0.35-TANB_2.0",
           "SINP_0.35-TANB_3.0",
           "SINP_0.35-TANB_5.0",
           "SINP_0.35-TANB_7.0",
           "SINP_0.35-TANB_10.0",
           "SINP_0.35-TANB_20.0",
           "SINP_0.35-TANB_30.0",
           "SINP_0.7-TANB_0.3",
           "SINP_0.7-TANB_0.5",
           "SINP_0.7-TANB_1.0",
           "SINP_0.7-TANB_2.0",
           "SINP_0.7-TANB_3.0",
           "SINP_0.7-TANB_5.0",
           "SINP_0.7-TANB_7.0",
           "SINP_0.7-TANB_10.0",
           "SINP_0.7-TANB_20.0",
           "SINP_0.7-TANB_30.0",
           ]
evgenConfig.nEventsPerJob=100
multiplier=1.2
include("MadGraphControl_Py8EG_2HDMa_monoZ_common.py")