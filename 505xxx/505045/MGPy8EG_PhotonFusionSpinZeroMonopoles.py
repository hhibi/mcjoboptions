###############################################################
#
# Job options file for Evgen MadGraph Pythia8 Monopole Generation
# W.Y. Song, 2020-09-02
#==============================================================

#--------------------------------------------------------------
# MadGraph on the fly
#--------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *

if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  beamEnergy = 6500.

PDGId = 4110000
masses = {}
masses[PDGId] = float(mass)

process_str="import model mono_spinzero\ngenerate a a  > mm+ mm-\noutput -f"
process_dir = new_process(process_str)

# Create run card
run_card_extras = {}
run_card_extras['lhaid'] = '82200' # LUXqed17_plus_PDF4LHC15_nnlo_100 pdf set 
safefactor=1.1 #generate extra 10% events in case any fail showering
run_card_extras['nevents'] = evgenConfig.nEventsPerJob*safefactor
run_card_extras['pdlabel'] = 'lhapdf'
run_card_extras['lhe_version'] = '2.0'
run_card_extras['ptheavy'] = '%s' % ptcut
modify_run_card(process_dir=process_dir, settings=run_card_extras)

# Create param_card                                                           
param_card_extras={'GCH':{'GCH':'%s' % gcharge}, 'MASS':{'MMM':'%s' % mass}}
modify_param_card(process_dir=process_dir,params=param_card_extras)

print_cards()

# Generating events in MG
generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=2, saveProcDir=True)


#--------------------------------------------------------------
# General MC15 configuration
#--------------------------------------------------------------
include( "Pythia8_A14_LUXqed_EvtGen_Common.py" )
include( "Pythia8_i/Pythia8_MadGraph.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Photon fusion spin-0 magnetic monopole generation for Mass=%s, Gcharge=%s with MadGraph+Pythia8 and the A14_LUXqed tune in MC16" % (mass,gcharge)
evgenConfig.keywords = ["exotic", "magneticMonopole", "photonFusion", "BSM"]
evgenConfig.contact = ["wen.yi.song@cern.ch"]
evgenConfig.specialConfig = 'MASS=%s;GCHARGE=%s;preInclude=SimulationJobOptions/preInclude.Monopole.py' % (mass,gcharge)

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with monopole mass
#--------------------------------------------------------------
ALINE1="M 4110000                          %s.E+03       +0.0E+00 -0.0E+00 Monopole        0" % (mass)
ALINE2="W 4110000                          0.E+00         +0.0E+00 -0.0E+00 Monopole        0"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

#--------------------------------------------------------------
# Edit G4particle_whitelist.txt with monopole
#--------------------------------------------------------------

ALINE1="4110000   mm  %s.E+06 (Mev/c) lepton %s" % (mass,gcharge)
ALINE2="-4110000  mmbar  %s.E+06 (Mev/c) lepton -%s" % (mass,gcharge)

import os
import sys

pdgmod = os.path.isfile('G4particle_whitelist.txt')
if pdgmod is True:
    os.remove('G4particle_whitelist.txt')
os.system('get_files -data G4particle_whitelist.txt')
f=open('G4particle_whitelist.txt','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

































































