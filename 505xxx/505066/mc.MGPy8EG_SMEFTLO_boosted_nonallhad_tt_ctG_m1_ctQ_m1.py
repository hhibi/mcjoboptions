from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphParamHelpers import *
from AthenaCommon.AppMgr import ServiceMgr

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------

evgenConfig.contact = [ 'kevin.sedlaczek@cern.ch' ]
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------

process='''
import model SMEFTatNLO-LO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > t t~ QCD=3 QED=0 NP=2 NP^2=2
output -f'''

process_dir = new_process(process)

# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents*11 if runArgs.maxEvents>0 else 11*evgenConfig.nEventsPerJob
settings = {
    'nevents':nevents,
    'maxjetflavor': '5',
    'fixed_ren_scale' : 'True',
    'fixed_fac_scale' : 'True',
    'scale':'172.5',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'
modify_param_card(process_dir=process_dir,params=params)

# Set SMEFT@NLO parameters
params = dict() 
params['dim6'] = dict()
params['dim62f'] = dict()
params['dim64f'] = dict()
params['dim64f2l'] = dict()
params['dim64f4l'] = dict()
for i in range(2,11) :
    params['dim6'][str(i)] = '1.000000e-10'
for i in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,19,22,23,24] :
    params['dim62f'][str(i)] = '1.000000e-10'
for i in [1,2,3,4,6,7,8,10,11,12,13,14,16,17,19,20,21,23,25] :
    params['dim64f'][str(i)] = '1.000000e-10'
for i in [1,2,3,4,5,6,7,8,9,10,13,14,15,16,17,19,20,21] :
    params['dim64f2l'][str(i)] = '1.000000e-10'
for i in range(1,10) :
    params['dim64f4l'][str(i)] = '1.000000e-10'
params['dim62f']['24'] = '-1.000000e+00' # ctg
params['dim64f']['4'] = '-1.000000e+00' # ctq8
modify_param_card(process_dir=process_dir,params=params)

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,
        runArgs=runArgs,
        )
arrange_output(process_dir=process_dir,
            runArgs=runArgs,
            lhe_version=3,
            saveProcDir=True
            )

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'aMcAtNlo_ttbar'
evgenConfig.keywords+=['ttbar']
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_aMcAtNlo.py')

# --------------------------------------------------------------
# Apply TTbarWToLeptonFilter
# --------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 1.0)

if not hasattr(filtSeq, "TruthJetFilter"):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"
filtSeq.TruthJetFilter.Njet = -1
filtSeq.TruthJetFilter.NjetMinPt = 160000.
filtSeq.TruthJetFilter.NjetMaxEta = 2.5
filtSeq.TruthJetFilter.jet_pt1 = 160000.
filtSeq.TruthJetFilter.applyDeltaPhiCut = False
filtSeq.TruthJetFilter.MinDeltaPhi = 0.2
filtSeq.Expression = "TTbarWToLeptonFilter and TruthJetFilter"
