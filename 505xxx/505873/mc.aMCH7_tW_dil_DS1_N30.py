from MadGraphControl.MadGraphUtils import *

evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 1

#############

mscard = open("madspin_card.dat",'w')

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************

#Some options (uncomment to apply)
#

 set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
 set seed %i

# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
decay w+ > l+ vl
decay w- > l- vl~
decay z > all all
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

import tarfile
if tarfile.is_tarfile(runArgs.inputGeneratorFile):
  myTarball = tarfile.open(runArgs.inputGeneratorFile)
  myEvents = None
  for afile in myTarball.getnames():
    if afile.endswith('.events'): myEvents = afile
    if myEvents is None:
      raise RuntimeError('No input events file found!')
    else:
      events_file = myTarball.extractfile( myEvents )
      madspin_on_lhe(myEvents,"madspin_card.dat",runArgs=runArgs,keep_original=True)
myTarball.close()


#### Shower
evgenConfig.description = 'aMC@NLO+Herwig7 tW dilepton DS1'
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.keywords+= ['SM', 'top', 'singleTop']
evgenConfig.contact = ['Olga Bylund <olgabylund@gmail.com>']
evgenConfig.tune = "H7.1-Default"
runArgs.inputGeneratorFile=myEvents

check_reset_proc_number(opts)
include("Herwig7_i/Herwig72_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118", max_flav=5)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()    
