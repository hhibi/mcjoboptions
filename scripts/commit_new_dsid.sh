#!/usr/bin/env bash

# For coloured output
source scripts/helpers.sh
# Whitelist of objects that can be added to the repo
source scripts/whitelist.sh

# Defaults
git=true
dryrun=false
newDSID=()
dOptionUsed=false
skipAthena=false
commitMessage=""
DEFAULTIFS=$IFS
IFS=' '

####################
# Helper functions #
####################

# Usage
printHelp() {
  printInfo -f "\nUsage:"
  printInfo -f "./scripts/commit_new_dsid.sh -d=DSID1-DSID5,DSID6... -m=\"Commit message\" [-b=branchName -n]\n"
  echo -e "-d|--dsid\ttakes as arguments a comma-separated list of DSIDs to commit (DSID1-DSID5 specifies a range of DSIDs)\n"
  echo -e "-b|--branch\tspecify the name of the branch to be created\n"
  echo -e "--dry-run\tDry run: will perform jO consistency checks, and print out the list of DSIDs to be assigned. It will not copy DSIDs to the correct location or push to git\n"
  echo -e "-n|--nogit\tSame as dry run, but including the assignment of the DSIDs (copy dummy DSIDs to correct location). Nothing will be pushed to git\n"
  echo -e "-m=\"Commit message\"\tcommit message to be added to git commit (here you can also specify which CI jobs to skip)\n"
  echo -e "-h|--help\tprints this help message\n"
}

# A function to store the DSIDs to be committed to git
parseDSIDList() {
  # Change IFS to comma for simple reading of DSID list
  IFS=','
  for string in $1 ; do
    # If there is a "-" in the requested DSIDs interpret it as a range
    if [[ $string == *"-"* ]] ; then
      # Create the sequence of DSIDs based on the numbers on the left and right from "-"
      dsid=($(echo $string | awk 'BEGIN {FS="-"} ; {printf "%i %i\n",$1,$2}' | xargs seq | tr '\n' ','))
      newDSID=( "${newDSID[@]}" "${dsid[@]}" )
    else
      # Otherwise it's a single DSID - add it in the list of DSIDs to submit
      newDSID+=($string)
    fi
  done
  # Restore IFS
  IFS=' '
}

# A function to parse the commit message
checkCommitMessage() {
  if [[ "$1" =~ "[skip" ]] ; then
     if [[ !("$1" =~  "[skip all]")       && \
           !("$1" =~  "[skip modfiles]")  && \
           !("$1" =~  "[skip size]")      && \
           !("$1" =~  "[skip athena]")    && \
           !("$1" =~  "[skip logparser]")      ]] ; then
      printError "ERROR: you are trying to skip a pipeline with an invalid option."
      printError "Valid options are: [skip all],[skip modfiles],[skip size],[skip athena],[skip logparser]"
      return 1
    elif [[ ("$1" =~  "[skip athena]") || ("$1" =~  "[skip logparser]") ]] ; then
      skipAthena=true
    fi
  fi
  return 0
}

# A function to reassign DSIDs
bookDSIDs() {
  local lenDSIDs=$2
  # Change IFS to newline
  SAVEIFS=$IFS
  IFS=$'\n'
  # Loop over DSIDs that have to be moved
  for dsid in $1 ; do
    # $1 is a string like: "Move 999xxx/999995 -> 800xxx/800037" printed out by scripts/check_jo_consistency_main.py
    initialDSIDdir=$(echo $dsid | awk '{print $2}')
    finalDSIDdir=$(echo $dsid | awk '{print $4}')
    finalDSID=$(echo $finalDSIDdir | awk -F '/' '{print $2}')
    if [[ $initialDSIDdir =~ [0-9]{3}xxx/[0-9]{6} ]] ; then # this covers cases where the initial DSIDs are placed in dummy directories like 100xxx/100000
      initialDSID=$(echo $initialDSIDdir | awk -F '/' '{print $2}')
    else # this covers cases where the initial DSIDs are assigned arbitrary names
      initialDSID=$initialDSIDdir
    fi
    # Dry-run: inform the user which DSIDs will be assigned
    if $dryrun; then
      printWarning -f "\t\tWill move $initialDSIDdir to $finalDSIDdir"
    # book the DSID
    else
      # If the NNNxxx directory does not exist then we have to create it first
      if [[ ! -d $(dirname $finalDSIDdir) ]] ; then mkdir $(dirname $finalDSIDdir) ; fi
      if [ -d $finalDSIDdir ] ; then
        printError -f "\t\tERROR: Attempt to copy $initialDSIDdir to $finalDSIDdir but the latter already exists in your local repository."
        exit 20
      else
        printGood -f "\t\tCopying $initialDSIDdir to $finalDSIDdir"
        cp -a $initialDSIDdir $finalDSIDdir
      fi
      # Keep track of the DSIDs that are being assigned
      DSIDdictionary+=([$initialDSIDdir]=$finalDSIDdir)
      # Replace original DSIDs with new ones - this is done so that the printouts below make sense
      for (( i=0; i<$lenDSIDs; i++ )); do
        if [[ "${newDSID[i]}" == "$initialDSID" ]] ; then
          newDSID[i]=$finalDSID
        fi
      done
    fi
  done
  # Now that we have the dictionary of moved DSIDs, loop once again over moved DSIDs and fix the links pointing to old DSIDs
  for old_dsid in "${!DSIDdictionary[@]}"; do
    # Check if there are any links in the new directory
    links=($(find ${DSIDdictionary[$old_dsid]} -type l 2> /dev/null))
    for link in "${links[@]}" ; do
      # Read where the link is pointing to
      oldLinkedFile=$(readlink $link)
      if [[ $oldLinkedFile =~ /eos ]] ; then continue ; fi # Nothing to be done for links pointing to eos
      # Extract the directory where the link is pointing to
      oldDir=$(echo $oldLinkedFile | sed 's|.*\([0-9]\{6\}\).*|\1|') # this will retain only the 6-digit DSID
      # Find the corresponding new directory from the dictionary
      newDir=${DSIDdictionary[${oldDir:0:3}xxx/$oldDir]}
      # Find where newDir is located relative to the oldDir
      if [[ $oldLinkedFile =~ ^\. ]] ; then
        relPath=$(python -c "import os.path; print(os.path.relpath('$newDir/$(basename $oldLinkedFile)', '${oldDir:0:3}xxx/$oldDir]'))") # linked file in different DSID directory
      else
        relPath="./$oldLinkedFile" # Linked file in same DSID directory
        echo "ERROR: $link is not a relative link or is pointing to a file in the same directory"
        exit 20
      fi
      # Replace old link with new one
      rm -f $link
      ln -s $relPath $link
    done
  done
  # Restore IFS
  IFS=$SAVEIFS
}

####################
#   Main script    #
####################

# The absolute path of where the mcjoboptions directory resides
MCJODIR=$( cd "$(dirname "$0/..")" >& /dev/null ; pwd -P )

# Check bash version
if [[ $(echo $BASH_VERSION | awk -F '.' '{print $1}') < 4 ]] ; then
  printError "ERROR: you are using bash version: $BASH_VERSION. Version >= 4 is needed to run the script. Upgrade your bash or run the script from lxplus"
  exit 1
fi

# Get command line arguments
if [[ "$#" < 1 ]] ; then
  printError "ERROR: no arguments specified"
  printHelp
  exit 2
fi

for i in "$@" ; do
  case $i in
    -h|--help)
      printHelp
      exit 0
      ;;
    -d=*|--dsid=*)    # DSIDs to commit
      parseDSIDList "${i#*=}"
      dOptionUsed=true
      shift
      ;;
    -b=*|--branch=*)  # branch name
      branchName="${i#*=}"
      shift
    ;;
    -n|--nogit)       # do not commit anything to git
      git=false
      shift
      ;;
    --dry-run)        # Dry run: run jO consistency checks without assigning DSIDs or pushing anything to git
      dryrun=true
      git=false
      shift
      ;;
    -m=*)             # add flags in the commit message to skip certain CI jobs
      commitMessage="${i#*=}"
      checkCommitMessage "$commitMessage"
      if [[ $? == 1 ]] ; then exit 3 ; fi
      shift
      ;;
    *)
      if [[ -d $1 ]] ; then  # DSIDs to be committed live in directories not names like NNNxxx
        newDSID+=(${i%/})    # Remove potential trailing slashes which cause problems downstream
        shift
      else
        printError "ERROR: Unrecognised option $1"
        printHelp
        exit 4
      fi
      ;;
  esac
done

# Make sure obligatory arguments have been specified
if [ -z "$commitMessage" ] ; then
  printError "ERROR: you need to specify a commit message with -m=\"message\""
  exit 5
fi
if [[ ${#newDSID[@]} == 0 ]]; then
  printError "ERROR: no DSIDs specified to commit. Use -d=DSID1-DSID5,DSID6..."
  exit 6
fi

# Get name of remote for pushing
remoteName=$(git remote -v | awk '$2 ~ /atlas-physics\/pmg\/mcjoboptions.git/ && $3 == "(push)" {print $1}' | head -1)
if [ -z $remoteName ] ; then
  printError "ERROR: cannot find remote name corresponding to atlas-physics/pmg/mcjoboptions.git"
  exit 7
else
  printInfo "INFO: will use following remote for pushing: $remoteName"
fi

# Update all the local branches
git fetch $remoteName --prune >& /dev/null

# Get the name of the current branch
currentBranch=$(git rev-parse --abbrev-ref HEAD)
if [[ $git == "true" && $currentBranch != "master" ]] ; then
  printError "ERROR: running from branch: $currentBranch. You must switch to the master branch before executing the script"
  exit 8
fi

# Make sure that there are no staged commits
stagedCommits=$(git diff --name-status --cached)
if [ ! -z $stagedCommits ] ; then
  printError "ERROR: you are on the master branch and have the following staged commits:"
  echo $stagedCommits
  printError "You need to unstage these commits before running the script"
  exit 9
fi

# Check python version
pythonversion=$(python3 --version | awk '{printf "%f\n",$2}')
if [[ $pythonversion < 3.6 ]] ; then
  printError "ERROR: python version < 3.6 being used. Please update to v3.6 or higher"
  exit 10
fi


# Switch to a new git branch
if (( ${#newDSID[@]} > 0 )) ; then
    # Name of new branch
    if [ -z $branchName ] ; then
      branchName=$(echo dsid"_"$USER"_"${newDSID[0]} | tr -d "." | tr -d "/")  # Remove dots and slashes from branchName
    fi
    printInfo "Will use branch: $branchName..."
    # Check if branch already exists
    git fetch --all >& /dev/null
    git branch | grep $branchName >& /dev/null && branchExists=true || branchExists=false
    # If branch exists check it out
    if $branchExists ; then
      if $git ; then
        printInfo "Branch: $branchName exists. Checking it out..."
        git checkout $branchName || exit 11
      else
        printInfo "Branch: $branchName exists. Will just check it out..."
      fi
    # Otherwise create new one
    else
      if $git ; then
        printInfo "Creating new branch: $branchName"
        # Checkout a new branch starting from $remoteName/master
        git checkout -b $branchName $remoteName/master || exit 12
      else
        printInfo "Will create new branch: $branchName"
      fi
    fi
fi

# Check jO naming and DSID ranges
declare -A DSIDdictionary # associative array that holds {old dsid, new dsid} pair
printInfo "Checking jO consistency and DSID ranges ..."
jOs=()
for dsid in "${newDSID[@]}" ; do
  # Directory should be located in
  if [[ $dsid =~ [0-9]{6} && "$dOptionUsed" == "true" ]] ; then
    dsiddir=${dsid:0:3}xxx/$dsid
  else
    dsiddir=$dsid
  fi
  # Check if the directory conntainns any files
  if [ -z $(ls $dsiddir/mc.*.py 2> /dev/null) ] ; then
    printError "ERROR: Directory $dsiddir does not contain any jO files"
    exit 13
  fi
  jO=$(ls $dsiddir/mc.*.py)
  jOs+="$jO "
done
jOConsistencyChecks=$(python scripts/check_jo_consistency_main.py ${jOs[@]})
# Check jO consistency and DSID assignment
if [[ $? != 0 ]] ; then
  DSIDtoMove=$(echo $jOConsistencyChecks | grep Move)
  # jOs are in the wrong DSID range and have to be moved
  if [[ ! -z $DSIDtoMove ]] ; then
    bookDSIDs "$DSIDtoMove" ${#newDSID[@]}
  # Serious error in jO consistency
  else
    printError -f "\tERROR in jO consistency checks"
    echo $jOConsistencyChecks
    exit 14
  fi
# DSID is correct and no jO concistency error found
else
  printGood -f "\t${jOs[@]} in correct DSID range"
fi

# Loop over new DSID directories to be added
lenDSIDs=${#newDSID[@]}
for dsid in "${newDSID[@]}" ; do
  # Directory should be located in
  if [[ $dsid =~ [0-9]{6} ]] ; then
    dsiddir=${dsid:0:3}xxx/$dsid
  else
    dsiddir=$dsid
  fi
  # Check if directory exists
  if [ ! -d $dsiddir ] ; then
    printError "ERROR: Directory $dsiddir does not exist"
    exit 15
  else
    printInfo "New DSID directory: $dsiddir ..."
  fi

  # Check if they contain a log.generate or log.afterburn file
  logType=""
  if [ -f $dsiddir/log.generate -a ! -f  $dsiddir/log.afterburn ] ; then
    logType="log.generate"
    printGood -f "\tOK: log.generate file found."
  elif [ -f $dsiddir/log.afterburn -a ! -f $dsiddir/log.generate ] ; then
    logType="log.afterburn"
    printGood -f "\tOK: log.afterburn file found."
  elif [ -f $dsiddir/log.generate -a -f  $dsiddir/log.afterburn ] ; then
    printError -f "\tERROR: $dsiddir contains both a log.generate and a log.afterburn file. You must only keep one."
    exit 16
  else
    printWarning -f "\tWARNING: $dsiddir does not contain log.generate or log.afterburn"
    printWarning -f "\tYou should athena locally and add the log.generate or log.afterburn file in $dsiddir"
    printWarning -f "\tThe athena and logParser jobs will not run in the CI "
  fi

  # If athena was run locally and the log file is found in the directory to commit
  # perform some checks to make sure that the new DSIDs conform to the rules
  if [ ! -z $logType ] ; then
    # Run the logParser and store the output in a temporary file
    tmpLogParserOut=$(mktemp -u)
    joFile=$(ls $dsiddir/mc.*.py)
    python3 scripts/logParser.py -c -i $dsiddir/$logType -j $joFile > $tmpLogParserOut
    if (( $? != 0 )); then
      cat $tmpLogParserOut
      printError -f "\tERROR: logParser run failed."
      exit 17
    fi
     
    # Check if local log file passes the logParser checks
    Nerror=$(grep -E 'Errors.*Warnings' $tmpLogParserOut | awk '{print $3}')
    if (( Nerror != 0 )) ; then
      python3 scripts/logParser.py -i $dsiddir/$logType -j $joFile
      printError -f "\tERROR: log file contains errors"
      printError -f "\tFix them before committing anything!"
      exit 18
    else
      printGood -f "\tOK: log file contains no errors"
    fi

    # Extract necessary information from log file and put it into log.generate.short
    rm -f $dsiddir/log.generate.short
    # The following parameters are needed for running athena in the CI
    grep 'estimated CPU for CI job' $tmpLogParserOut >  $dsiddir/log.generate.short
    grep 'using release' $tmpLogParserOut >> $dsiddir/log.generate.short
    grep 'ecmEnergy' $tmpLogParserOut >> $dsiddir/log.generate.short
    # Input 
    grep 'inputGeneratorFile' $tmpLogParserOut >> $dsiddir/log.generate.short
    grep 'inputEVNT_PreFile' $tmpLogParserOut >> $dsiddir/log.generate.short
    grep 'EVNT to EVNT' $tmpLogParserOut >> $dsiddir/log.generate.short
    grep 'LHEonly' $tmpLogParserOut >> $dsiddir/log.generate.short
    grep 'ATHENA_PROC_NUMBER' $tmpLogParserOut >> $dsiddir/log.generate.short
        
    # Check if job lasts more than 1h - this will cause the pipeline to fail
    cpu=$(grep CPU $dsiddir/log.generate.short | awk '{print $8}')
    if [[ $(echo "$cpu > 1.0" | bc -l) == 1 ]] ; then
      if [[ $skipAthena == false ]] ; then
        printError -f "\tERROR: CI job is expected to last more than 1h - time estimate: $cpu hours"
        printError -f "\tIt is not possible to run this job in the CI. Contact the MC software coordinators"
        exit 19
      else
        printGood -f "\tOK: CI job time estimate: $cpu hours, but athena will not run in the CI"
      fi
    else
      printGood -f "\tOK: CI job expected to last less than 1h - time estimate: $cpu hours"
    fi
    
    # Remove logParser output file
    rm -f $tmpLogParserOut
  fi

  # Loop over files in new DSID directory and add them to the commit
  printInfo -f "\tWill now add files to git commit"
  SAVEIFS=$IFS
  IFS=$DEFAULTIFS
  for file in $(find $dsiddir/* -not -type d); do
    # Check if the file is in the whitelist and add it in the commit
    if checkWhiteList $file ; then
      # Special case: [skip athena] or [skip logparser] have been specified in the commit message
      # Should not commit log.generate.short
      if [[ $(basename $file) == "log.generate.short" && $skipAthena == true ]] ; then
         echo -e "\t\tSkipping: $file since the logParser CI job will not be run"
         continue
      else
        if $git ; then
          git add $file
          echo -e "\t\tAdded: $file"
        else
          echo -e "\t\tWill add: $file"
        fi
      fi
    else
      echo -e "\t\tFile: $file cannot be added to the commit. Skipping."
    fi
  done
  IFS=$SAVEIFS

done

# If DSIDs have been assigned print them out and suggest the command to run
NbookedDSIDs=${#DSIDdictionary[@]}
if (( $NbookedDSIDs > 0 && ! $git)) ; then
  DSID_RANGES=$(python3 scripts/jo_utils.py --dsid-parser ${newDSID[*]})
  printGood -f "\nThe following DSIDs have been assigned:"
  for old_dsid in "${!DSIDdictionary[@]}"; do
    printf "\t%6s -> %6s\n" $old_dsid ${DSIDdictionary[$old_dsid]}
  done | sort -n -k 1 # Need to sort so that the values are printed in numerical order
  printGood -f "Run: ./scripts/commit_new_dsid.sh -d=${DSID_RANGES} -m=\"$commitMessage\" to push them to git\n"
fi

# Push the changes
if $git ; then
  if [[ -z $(git diff --cached) ]] ; then
    printError "ERROR: nothing added to the commit. Please check error messages above."
  else
    git commit -q -m "$commitMessage"
    printInfo -f "\nYou have prepared branch $(git rev-parse --abbrev-ref HEAD)."
    printInfo "This contains the following changes:"
    git diff --stat master HEAD
    printInfo -f "\nIf this looks good, you should now run 'git push -u $remoteName $branchName' to upload."
  fi
fi

exit 0
