#!/bin/bash
source scripts/whitelist.sh

# Make sure that if a command fails the script will continue
set +e

echo "Find files that have been added in last commit..."

# Find number of files that have been:
# Copied (C), Deleted (D), Modified (M), Renamed (R), have their type
# (i.e. regular file, symlink, submodule, …​) changed (T), are Unmerged (U),
# are Unknown (X), or have had their pairing Broken (B).
files=($(git diff-tree --no-commit-id --name-only -r origin/master..HEAD --diff-filter=A))

if (( ${#files[@]} == 0 )) ; then
  echo "No files added"
  exit 0
fi

fail=false
for file in "${files[@]}" ; do
  # Check if file is in whitelist
  if checkWhiteList $file ; then
    echo "OK: file: $file in white list"
  else
    echo "ERROR: file: $file not in white list"
    fail=true
  fi
  # If it's a file in a DSID directory check that only 1 jO is inside the DSID directory
  if [[ $file =~ [0-9]{3}xxx ]] ; then
    fileDir=$(dirname $file)
    # Exclude directories inside main DSID directory (MadGraphControl etc)
    if [[ ($fileDir =~ [0-9]{3}xxx/[0-9]{6}/*) ]] ; then continue ; fi
    countJOinDSID=$(ls $fileDir/mc.*.py | wc -l)
    if (( countJOinDSID == 1)) ; then
      echo "OK: only 1 jO file found in $fileDir"
    else
      echo "ERROR: $countJOinDSID jO files found in $fileDir"
      fail=true
    fi
  fi
done

if $fail ; then
  exit 1
fi

exit 0
