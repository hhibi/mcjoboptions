include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

# Don't disable TestHepMC, but allow for highly displaced vertices
#evgenLog.info("Removing TestHepMC sanity checker")
#del testSeq.TestHepMC
testSeq.TestHepMC.MaxVtxDisp = 10000*10000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 10000*10000 #in mm
testSeq.TestHepMC.DumpEvent = True

## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short()

# Read in arguments
#gentype = str(shortname.split('_')[2])
decaytype = str(shortname.split('_')[2])
N1mass = float(shortname.split('_')[3])
N1lifetime_str = shortname.split('_')[4]
filtertype = shortname.split('_')[-1]

# C1/N2/N1 roughly mass denegenerate  mC1=mN2=mN1+1 GeV
masses['1000022'] = N1mass # N1
masses['1000023'] = -(N1mass+1.0) # N2
masses['1000024'] = N1mass+1.0 # C1 is degenerate with N2 # ALTERNATIVELY: C1 is between N1 and N2
masses['23'] = 9.11876000E+01
masses['32'] = 9.11876000E+01
masses['25'] = 125.0
masses['35'] = 125.0

# Set Lifetime
neutralinoLifetime = N1lifetime_str.replace("ns","").replace(".py","").replace("p","0.")
hbar = 6.582119514e-16
decayWidth = hbar/float(neutralinoLifetime)
decayWidthStr = '%e' % decayWidth

# Decay headers
header32 = 'DECAY 32 2.49520000E+00' # SM Z 23 decay width
header35 = 'DECAY 35 5.748014e-01' # SM Higgs 25 decay width
headerN1 = 'DECAY   1000022  ' + decayWidthStr
#headerC1 = 'DECAY   1000024  ' + decayWidthStr # jgonski we don't want these to be long lived
#headerN2 = 'DECAY   1000023  ' + decayWidthStr
evgenLog.info('lifetime of 1000022 is set to %s ns'% neutralinoLifetime)

# Branching ratios

BR_32 = """
#          BR         NDA          ID1       ID2       ID3       ID4
     1.00000000E+00    2           11        -11         # BR(Z->ee)
"""

BR_35 = """
#          BR         NDA          ID1       ID2       ID3       ID4
     1.00000000E+00    2           22        22         # BR(H->yy)
"""

BR_N1 = """
#          BR         NDA          ID1       ID2       ID3       ID4 """

if "Hyy" in filtertype:
  BR_N1 += """
     0.50000000E+00    2     1000039        35         # BR(~chi_10 -> ~G H) and the H is forced to decay to photons """

if "HSM" in filtertype:
  BR_N1 += """
     0.50000000E+00    2     1000039        25         # BR(~chi_10 -> ~G H) with SM BRs """

if "Zee" in filtertype:
  BR_N1 += """
     0.50000000E+00    2     1000039        32         # BR(~chi_10 -> ~G Z) and the Z is forced to decay to e+ e- """

if "ZSM" in filtertype:
  BR_N1 += """     
     0.50000000E+00    2     1000039        23         # BR(~chi_10 -> ~G Z) with SM BRs """

BR_N1 += """
"""

BR_C1 = """
#          BR         NDA          ID1       ID2       ID3       ID4
     3.64604242E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.64604242E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.21534824E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.21534824E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.77218666E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
"""

BR_N2 = """
#          BR         NDA          ID1       ID2       ID3       ID4
     1.37284605E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.84624124E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.37284605E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.84624124E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.98007010E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.98007010E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.98835450E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.88991980E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.88991980E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.88991980E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
"""

evgenLog.info('lifetime of 1000022 is set to %s ns'% neutralinoLifetime)

decays['32'] = header32 + BR_32
decays['35'] = header35 + BR_35        
decays['1000022'] = headerN1 + BR_N1        #decays comes from PreInclude.py generator
decays['1000024'] = 'Auto'  #need this to keep Geant4 happy
decays['1000023'] = 'Auto' 

# Got the mixing matrix from: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/500xxx/500195/SUSY_SimplifiedModel_VBFewkino_N2ZC1W.py#L97

# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11 bino 
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 7.07E-01'   # N_13
param_blocks['NMIX']['1  4']='-7.07E-01'   # N_14 
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21 
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23 higgsino
param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24 higgsino 
param_blocks['NMIX']['3  1']=' 1.00E+00'   # N_31 
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32 
param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42 wino
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44



#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safefactor)
else: nevents = int(evgenConfig.nEventsPerJob*safefactor)

# x1+x1- + 2 partons inclusive
process = '''
generate p p > x1+ x1- / susystrong @1
add process p p > x1+ x1- j / susystrong @2
add process p p > x1+ x1- j j / susystrong @3
add process p p > x1+ n1 / susystrong @4
add process p p > x1+ n1 j / susystrong @5
add process p p > x1+ n1 j j / susystrong @6
add process p p > x1- n1 / susystrong @7
add process p p > x1- n1 j / susystrong @8
add process p p > x1- n1 j j / susystrong @9
add process p p > x1+ n2 / susystrong @10
add process p p > x1+ n2 j / susystrong @11
add process p p > x1+ n2 j j / susystrong @12
add process p p > x1- n2 / susystrong @13
add process p p > x1- n2 j / susystrong @4
add process p p > x1- n2 j j / susystrong @15
add process p p > n1 n2 / susystrong @16
add process p p > n1 n2 j / susystrong @17
add process p p > n1 n2 j j / susystrong @18
'''

# this is required for the n1 from the n1n2 events to be an llp
run_settings.update({'time_of_flight':'1E-25'})

njets = 2
evgenLog.info('Registered generation of ~hino pair production, decay to gravitino; grid point '+str(runArgs.jobConfig[0])+' decoded into mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))

evgenConfig.contact = [ "kiley.elizabeth.kennedy@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'chargino', 'neutralino', 'gravitino']
evgenConfig.description = '~hino pair production, decay to gravitino simplified model, m_C1N2 = %s GeV, m_N1 = %s GeV'%(masses['1000023'],masses['1000022'])
evgenConfig.nEventsPerJob = 1000

# Filter and event multiplier 
# jgonski do we need these at all? 
#evt_multiplier = 3
#
#if '4L4' in shortname.split('_')[-1]:
#  evgenLog.info('4lepton4 filter is applied')
#  
#  include ( 'MadGraphControl/MultiElecMuTauFilter.py' )
#  filtSeq.MultiElecMuTauFilter.NLeptons  = 4
#  filtSeq.MultiElecMuTauFilter.MinPt = 4000.         # pt-cut on the lepton
#  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
#  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
#  filtSeq.Expression = "MultiElecMuTauFilter"
#  
#  if masses['1000022'] >= 600 :
#    evt_multiplier = 100
#  elif masses['1000022'] >= 200 :
#    evt_multiplier = 170
#  elif masses['1000022'] >= 130 :
#    evt_multiplier = 220
#  else:
#    evt_multiplier = 290
#
#    
#elif '2L8' in shortname.split('_')[-1]:
#  evgenLog.info('2lepton8 filter is applied')
#  
#  include ( 'MadGraphControl/MultiElecMuTauFilter.py' )
#  filtSeq.MultiElecMuTauFilter.NLeptons  = 2
#  filtSeq.MultiElecMuTauFilter.MinPt = 8000.         # pt-cut on the lepton
#  #filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 15000.   # pt-cut on the hadronic taus
#  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
#  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
#  filtSeq.Expression = "MultiElecMuTauFilter"
#  
#  evt_multiplier = 50
#
#elif '3L4' in shortname.split('_')[-1]:
#  evgenLog.info('3lepton4 filter is applied')
#  
#  include ( 'MadGraphControl/MultiElecMuTauFilter.py' )
#  filtSeq.MultiElecMuTauFilter.NLeptons  = 3
#  filtSeq.MultiElecMuTauFilter.MinPt = 4000.         # pt-cut on the lepton
#  filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 15000.   # pt-cut on the hadronic taus
#  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
#  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 1    # include hadronic taus
#  filtSeq.Expression = "MultiElecMuTauFilter"
#  
#  evt_multiplier = 30
#
#elif '2G15' in shortname.split('_')[-1]:
#  evgenLog.info('2photon15 filter is applied')
#  
#  include ( 'GeneratorFilters/DirectPhotonFilter.py' )
#  filtSeq.DirectPhotonFilter.NPhotons  = 2
#  filtSeq.DirectPhotonFilter.Ptmin = [15000.]         # pt-cut on the photon
#  filtSeq.DirectPhotonFilter.Etacut = 2.8          # eta cut on the photon
#  filtSeq.Expression = "DirectPhotonFilter"
#  
#  evt_multiplier = 50

# My filters #

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter

if 'ZSM' in filtertype:
  evgenLog.info('N1->Z(23)+X  filter is applied')
  filtSeq += ParentChildFilter("N1Z23Filter")
  filtSeq.N1Z23Filter.PDGParent = [1000022]
  filtSeq.N1Z23Filter.PDGChild  = [23]

if 'Zee' in filtertype:
  evgenLog.info('N1->Z(32)+X  filter is applied')
  filtSeq += ParentChildFilter("N1Z32Filter")
  filtSeq.N1Z32Filter.PDGParent = [1000022]
  filtSeq.N1Z32Filter.PDGChild  = [32]

if 'HSM' in filtertype:
  evgenLog.info('N1->H(25)+X  filter is applied')  
  filtSeq += ParentChildFilter("N1H25Filter")
  filtSeq.N1H25Filter.PDGParent = [1000022]
  filtSeq.N1H25Filter.PDGChild  = [25]

if "Hyy" in filtertype:
  evgenLog.info('N1->H(35)+X  filter is applied')
  filtSeq += ParentChildFilter("N1H35Filter")
  filtSeq.N1H35Filter.PDGParent = [1000022]
  filtSeq.N1H35Filter.PDGChild  = [35]

#for LHE file ending before Pythia
evt_multiplier = 10

#if N1mass > 250: evt_multiplier = 15
#if N1mass > 400: evt_multiplier = 20
#if N1mass > 600: evt_multiplier = 35

evt_multiplier *= 1.1

genSeq.Pythia8.Commands += [ "SLHA:minMassSM = 90.",  # min. mass below which SLHA inputs are ignored (default =100.0) 
                             # Below: failed attempt at overriding use of SLHA decay tables:
                             # "SLHA:useDecayTable = on", # allows decay parameters to be read in from <slha> block, see https://lost-contact.mit.edu/afs/cern.ch/work/h/hrussell/public/jobOptionSSn.py
                             # "SLHA:keepSM = off", # allows particle data for ids 1-24 and 81- 999,999 to be changed (default - on) 
                             # "32:onIfAny = 11", # see https://lost-contact.mit.edu/afs/cern.ch/work/s/schaffer/public/work-19.prophecy4f/run/higgscontrol/aMcAtNloPythia8EvtGenControl_bbA_Zh.py
                             # "SLHA:allowUserOverride = on", # see http://home.thep.lu.se/~torbjorn/pythia82html/SUSYLesHouchesAccord.html
                            ]

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0: 
 genSeq.Pythia8.Commands += ["Merging:Process = guess",
                             "1000024:spinType = 1",
                             "1000022:spinType = 1",
                             "1000023:spinType = 1" ]

 genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
