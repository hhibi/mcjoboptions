#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------
THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] = 10 # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427. 
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.35 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['MXd'] = 10. # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.THDMparams['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.
THDMparams['mh2'] = 1000 # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = 1000 # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mhc'] = 1000 # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh4'] = 450 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['MB'] = 0.0 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

initialGluons = False # Determines initial state to generate from. True --> top loop induced production (gluon Fusion). False --> b b~ -initiated production (b-anti-b annihilation). See Sections 5.4 and 6.5 of arXiv:1701.07427 for further details

# Reweighting in tanb and sinp
reweight = True # If set to True additional OTF weights are written in the LHE file, if False no additional weights written
reweights=[
'SINP_0.35-TANB_0.3',
'SINP_0.35-TANB_0.5',
'SINP_0.35-TANB_1.0',
'SINP_0.35-TANB_2.0',
'SINP_0.35-TANB_3.0',
'SINP_0.35-TANB_5.0',
'SINP_0.35-TANB_10.0',
'SINP_0.35-TANB_20.0',
'SINP_0.7-TANB_0.3',
'SINP_0.7-TANB_0.5',
'SINP_0.7-TANB_1.0',
'SINP_0.7-TANB_2.0',
'SINP_0.7-TANB_3.0',
'SINP_0.7-TANB_5.0',
'SINP_0.7-TANB_10.0',
'SINP_0.7-TANB_20.0',
]

# Define that we want H->bb decays
decayChannel="monoHbb"

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob=5000

# Event multiplier
if THDMparams['mh4'] == 100 and THDMparams['mh3'] == 250 and initialGluons == False:
    multiplier=15
elif (THDMparams['mh4'] == 150 and THDMparams['mh3'] == 300) or (THDMparams['mh4'] == 100 and (THDMparams['mh3'] in [250,900,1200,1500,1800])):
    multiplier=10
else:    
    multiplier=4

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------    
include("MadGraphControl_Py8EG_2HDMa_monoH_common_fix.py")
