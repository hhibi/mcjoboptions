import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

safefactor = 4.
nevents = 10000*safefactor
if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents*safefactor
    

#---------------------------------------------------------------------------------------------------
# Whitelisting PIDs from the model
#---------------------------------------------------------------------------------------------------
pid_list = ["252","253","254","255","256","257"]
whitelist_file = open("pdgid_extras.txt", "w")
for pid in pid_list:
    whitelist_file.write(pid+"\n-"+pid+"\n")
whitelist_file.close()

testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

#---------------------------------------------------------------------------------------------------
# Setting some parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {'MASS':{'w+':8.038500e+01,'H':0.389744e+03,'H3z':0.326956e+03,'H3p':0.326956e+03,'H5z':1.300000e+02,'H5p':1.300000e+02,'H5pp':1.300000e+02},'POTENTIALPARAM':{'lam2':0.253553e+00,'lam3':0.396961e-01,'lam4':0.369737e+00,'lam5':-0.148463e+01,'M1coeff':0.872481e-01,'M2coeff':4.000000e+01},'SMINPUTS':{'aEWM1':1.322330e+02,'Gf':1.166380e-05,'aS':1.180000e-01},'VEV':{'tanth':1.000000e-04}}

#---------------------------------------------------------------------------------------------------
# Generating the singly charged and uncharged Higgs
#---------------------------------------------------------------------------------------------------

process = """
        import model GM_UFO
        define p = g u c d s u~ c~ d~ s~
        generate p p > H5p H5z > w+ a a a
        add process p p > H5p~ H5z > w- a a a
        output -f
        """

    
runName='run_01'

settings = {'nevents':nevents,'cut_decays':'True'}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Print the cards
#---------------------------------------------------------------------------------------------------

print_cards()

#---------------------------------------------------------------------------------------------------
# Generate the events
#---------------------------------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------

# -- NumElec + NumMu + NumTau >= 1
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("ElecMuTauFilter")
filtSeq.ElecMuTauFilter.NLeptons = 1
filtSeq.ElecMuTauFilter.IncludeHadTaus = True
filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV
filtSeq.ElecMuTauFilter.MaxEta = 2.8

# -- Leading Lepton Pt Filter at pt>20GeV
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingLepFilter")
filtSeq.LeadingLepFilter.Ptcut = 20000.
filtSeq.LeadingLepFilter.Etacut = 2.8

# -- The event must have at least one photon with pT>20GeV
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
filtSeq += PhotonFilter("PhoFilter")
filtSeq.PhoFilter.NPhotons = 1
filtSeq.PhoFilter.PtMin = 20000.
filtSeq.PhoFilter.EtaCut = 2.5



# -- Require at least one lepton
# -- Require the leading lepton (electron or muon) pt be greater than 20GeV
# -- Require at least one photon with pt > 20GeV
filtSeq.Expression = "(ElecMuTauFilter and LeadingLepFilter) and PhoFilter"

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["Utsav Patel <utsav.mukesh.patel@cern.ch>"]
evgenConfig.description = 'MadGraph_GM_Wa_lva'
evgenConfig.keywords+=['BSM','exotic','BSMHiggs','leptonic']
