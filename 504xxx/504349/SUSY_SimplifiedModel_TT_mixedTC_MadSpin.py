include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

#from AthenaCommon.AppMgr import ServiceMgr
#ServiceMgr.MessageSvc.OutputLevel = DEBUG

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

#get the file name and split it on _ to extract relavent information                                                                                                                                        
jobConfigParts = JOName.split("_")

decayType=""
if "MadSpin" in JOName:
    mstop=float(jobConfigParts[5])
    mneutralino=float(jobConfigParts[6].split('.')[0])
    decayType="_".join([jobConfigParts[7],jobConfigParts[8]]) 
else:
    mstop=float(jobConfigParts[4])
    mneutralino=float(jobConfigParts[5].split('.')[0])
    decayType="_".join([jobConfigParts[6],jobConfigParts[7]]) 

if not any(decayType == x for x in ["bWN_cN","bffN_cN","cN_bffN","cN_bWN","tN_cN","cN_tN"]):
    evgenLog.error("Unrecognized decay channel (%s) for tcMET 2-body/3-body/4-body samples. Exiting"%decayType)

#set the stop and neutralino masses                                                                                                                                                                         
masses['1000006'] = mstop
masses['1000022'] = mneutralino
if masses['1000022']<0.5: masses['1000022']=0.5


for mass  in masses.keys():
    if not any(mass==x for x in ["1000006","1000022","2000006","1000021"]) and int(mass)>= 1000000:
        del masses[mass]
    if any(mass==x for x in ["35","36","37"]):
        del masses[mass]

process = '''
import model stop-scharm_UFO 
display particles
display multiparticles
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~  
generate p p > t1 t1~ $ go t2 t2~ @1
add process p p > t1 t1~ j $ go t2 t2~ @2
add process p p > t1 t1~ j j $ go t2 t2~ @3
'''
    
njets = 2

run_settings["asrwgtflavor"]=4
run_settings["maxjetflavor"]=4



evgenLog.info('Registered generation of stop pair production, stop to c+LSP; grid point '+str(runArgs.jobConfig[0])+' decoded into mass point mstop=' + str(masses['1000006']) + ', mlsp='+str(masses['1000022']))


#--------------------------------------------------------------
#                      Madspin configuration
#--------------------------------------------------------------


#if 'MadSpin' in JOName:
evgenLog.info('Running w/ MadSpin option for decay type %s'%decayType)

msdecaystring=""

if decayType == "bWN_cN":
    msdecaystring="decay t1 > b w+ n1 \n decay t1~ > c~ n1 \n"
elif decayType == "bffN_cN":
    msdecaystring="decay t1 > b fu fd~ n1 \n decay t1~ > c~ n1 \n"
elif decayType == "cN_bWN":
    msdecaystring="decay t1 > c n1 \n decay t1~ > b~ w- n1 \n"
elif decayType == "cN_bffN":
    msdecaystring="decay t1 > c n1 \n decay t1~ > b~ fu~ fd n1 \n"
elif decayType == "tN_cN":
    msdecaystring="decay t1 > t n1 \n decay t1~ > c~ n1 \n"
elif decayType == "cN_tN":
    msdecaystring="decay t1 > c n1 \n decay t1~ > t~ n1 \n"


madspin_card='madspin_card_directTC.dat'
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************   
#*                        MadSpin                           *   
#*                                                          *   
#*  P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer   *   
#*                                                          *   
#*         Part of the MadGraph5_aMC@NLO Framework:         *   
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *   
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *   
#*                                                          *   
#************************************************************   
#Some options (uncomment to apply)  
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event  
#   
set seed %i 
set spinmode none

# specify the decay for the final state particles

define fu = u c e+ mu+ ta+
define fu~ = u~ c~ e- mu- ta-
define fd = d s ve~ vm~ vt~
define fd~ = d~ s~ ve vm vt   
define ll- = e- mu- ta-
define ll+ = e+ mu+ ta+

%s 

# running the actual code       
launch"""%(runArgs.randomSeed,msdecaystring))
mscard.close()


if 'MET' in JOName.split('_')[-1]:
    include ( 'GeneratorFilters/MissingEtFilter.py' )

    metFilter = JOName.split('_')[-1]
    metFilter = int(metFilter.split("MET")[1].split(".")[0])
   
    print "Using MET Filter: " + str(metFilter)
    filtSeq.MissingEtFilter.METCut = metFilter*GeV
    evt_multiplier = 15.0

else:
    print "No MET Filter applied"    

evgenConfig.contact  = [ "alvaro.lopez.solis@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','charm','top']
evgenConfig.description = 'stop direct pair production, st->bff+LSP , st~->c~+LSP in simplified model'

fixEventWeightsForBridgeMode=True
 
if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
 





