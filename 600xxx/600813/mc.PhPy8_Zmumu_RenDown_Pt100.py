# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG NC DY production"
evgenConfig.keywords = ["SM", "Z"]
evgenConfig.contact = ["amoroso@cern.ch", "jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 1000
# Dilepton pT cut of >100 GeV has about 1.2% efficiency -> 100x more LHE events
filterMultiplier = 100

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Z_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.decay_mode = "z > mu+ mu-"
PowhegConfig.no_ew=1
PowhegConfig.ptsqmin=4
PowhegConfig.mass_low=60
PowhegConfig.PHOTOS_enabled = False
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier

#Gmu EW scheme inputs matching what used in the Powheg V1 samples
PowhegConfig.scheme=0
PowhegConfig.alphaem=0.00781653
PowhegConfig.mass_W=79.958059

# tune Powheg settings
PowhegConfig.rwl_group_events = 100000
PowhegConfig.ncall1       = 200000
PowhegConfig.ncall2       = 200000
PowhegConfig.nubound      = 200000
PowhegConfig.itmx1        = 10
PowhegConfig.itmx2        = 20

# Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 1
PowhegConfig.foldy        = 1



PowhegConfig.PDF = 10800 #CT10
PowhegConfig.mu_F = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0]
PowhegConfig.mu_R = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5]
# with PDF weights?
#PowhegConfig.PDF = range(10800, 10852+1) #CT10
#PowhegConfig.PDF.extend(range(13000, 13056+1)) #CT14nnlo
#PowhegConfig.PDF.extend(range(303600,303700+1)) #NNPDF3.1nnlo


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------    
###JO for nominal####
#include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#include('Pythia8_i/Pythia8_Photospp.py')
####JO for variations#### 
#include('./Pythia8_AZNLO_CTEQ6L1_MPIUp_EvtGen_Common.py')
#include('./Pythia8_AZNLO_CTEQ6L1_MPIDown_EvtGen_Common.py')
include('./Pythia8_AZNLO_CTEQ6L1_RenDown_EvtGen_Common.py')
#include('./Pythia8_AZNLO_CTEQ6L1_RenUp_EvtGen_Common.py')
#include('./Pythia8_AZNLO_CTEQ6L1_Var1Down_EvtGen_Common.py')
#include('./Pythia8_AZNLO_CTEQ6L1_Var1Up_EvtGen_Common.py')
#include('./Pythia8_AZNLO_CTEQ6L1_Var2Down_EvtGen_Common.py')
#include('./Pythia8_AZNLO_CTEQ6L1_Var2Up_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
# --------------------------------------------------------------
# filter
# --------------------------------------------------------------    
include("GeneratorFilters/DiLeptonMassFilter.py")
DiLeptonMassFilter = filtSeq.DiLeptonMassFilter
DiLeptonMassFilter.MinPt = 5000.
DiLeptonMassFilter.MaxEta = 3.0
DiLeptonMassFilter.MinMass = 10000.
DiLeptonMassFilter.MaxMass = 1E9
DiLeptonMassFilter.MinDilepPt = 100000.
DiLeptonMassFilter.AllowSameCharge = False
