#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361604

include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = 'z z > v v~ l+ l-'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = [10800,13000, 303600, 25100] #CT10nnlo, CT14nnlo, NNPDF3.1nnlo, MMHT2014nlo68cl
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 26
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ->vvll production with AZNLO CTEQ6L1 tune and mllmin4'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'oldrich.kepka@cern.ch' ]
evgenConfig.nEventsPerJob   = 2000

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 7
filtSeq.ChargedTracksWeightFilter.NchMax = 20
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 10, 15, 25, 35 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.001, 0.001, 0.0025, 0.0045, 0.0055, 0.007, 0.007 ]

