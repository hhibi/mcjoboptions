evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO without PDF and Scale variations, mT = 172.0 GeV -> MPI off'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mshapiro@lbl.gov','steffen.henkelmann@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.nEventsPerJob =200


include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"

PowhegConfig.mass_t     = 173.0
PowhegConfig.width_t    = 1.333
PowhegConfig.hdamp      = 259.5       # 1.5 * mtop
PowhegConfig.PDF        = 260000      # nominal PDF set           
PowhegConfig.nEvents    = 400*100*4
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")


genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#myAdd : MPI off
genSeq.Pythia8.Commands += [ 'PartonLevel:MPI = off']

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


## JET FILTERING ##
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 1.0)

if not hasattr( filtSeq, "TruthJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()
    pass

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"
filtSeq.TruthJetFilter.Njet = -1
filtSeq.TruthJetFilter.NjetMinPt = 500*GeV
filtSeq.TruthJetFilter.NjetMaxEta = 2.5
filtSeq.TruthJetFilter.jet_pt1 = 500*GeV
filtSeq.TruthJetFilter.applyDeltaPhiCut = False
filtSeq.TruthJetFilter.MinDeltaPhi = 0.2

filtSeq.Expression = "TTbarWToLeptonFilter and TruthJetFilter"
