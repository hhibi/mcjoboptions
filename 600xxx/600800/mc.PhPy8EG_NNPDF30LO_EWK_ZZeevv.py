#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEGV2+Pythia8 EWK ZZ(eevmuvmu)+2j production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "VBS", "ZZ", "2jet"]
evgenConfig.contact = ["bili@cern.ch"]
evgenConfig.process     = "VBS ZZ(eevmuvmu)"
evgenConfig.nEventsPerJob = 10000
#evgenConfig.inputfilecheck = 'PhPy8EG_NNPDF30LO_EWK_ZZeevv'
#runArgs.inputGeneratorFile = 'PhPy8EG_NNPDF30LO_EWK_ZZeevv._00001.events.tar.gz'

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("Pythia8_i/Pythia8_Powheg.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = -1']

