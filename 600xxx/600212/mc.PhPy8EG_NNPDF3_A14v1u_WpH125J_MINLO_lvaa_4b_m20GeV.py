evgenConfig.nEventsPerJob = 10000 
evgenConfig.inputFilesPerJob = 200 

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125" ]

evgenConfig.description = "POWHEG+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
evgenConfig.process     = "hSM->h1h2->4b, Z->ll or W->lnu"
evgenConfig.contact     = [ 'rafael.lopesdesa@cern.ch' ]

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------

mh1 = 20.
wh1 = 0.00407
mh2 = 20.
wh2 = 0.00407
mh3 = 125.
wh3 = 0.00407

# Printing some settings
evgenLog.info("mh1 = %.2f"%mh1)
evgenLog.info("wh1 = %.2f"%wh1)
evgenLog.info("mh2 = %.2f"%mh2)
evgenLog.info("wh2 = %.2f"%wh2)
evgenLog.info("mh3 = %.2f"%mh3)
evgenLog.info("wh3 = %.2f"%wh3)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]

############
# Var 1 Up #
############
genSeq.Pythia8.Commands += [ 'MultipartonInteractions:alphaSvalue = 0.131' ]    
genSeq.Pythia8.Commands += [ 'ColourReconnection:range = 1.73' ]    

include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ["POWHEG:nFinal = 3"]

genSeq.Pythia8.Commands += [
    'Higgs:useBSM = on',
    '35:m0 = %.1f' % mh3,
    '35:mWidth = %.6f' % wh3,
    '35:doForceWidth = on',
]

genSeq.Pythia8.Commands += [
    'HiggsH2:coup2A3H1 = 1',
    '35:oneChannel = 1 1.0 0 25 36',
]

genSeq.Pythia8.Commands += [
    '25:oneChannel = 1 1.0 0 5 -5',
    '25:m0 = %.1f' % mh1,
    '25:mWidth = %.6f' % wh1,
    '25:doForceWidth = on',
    '25:mMin = 0',
    '25:tau0 = 0',
]
    
genSeq.Pythia8.Commands += [
    '35:oneChannel = 1 1.0 0 36 36',
]

genSeq.Pythia8.Commands += [
    '36:m0 = %.1f' % mh2,
    '36:mWidth = %.6f' % wh2,
    '36:doForceWidth = on',
    '36:tau0 = 0',
    '36:mMin = 0',
    '36:mMax = 100',
    '36:oneChannel = 1 1.0 0 5 -5',
]

