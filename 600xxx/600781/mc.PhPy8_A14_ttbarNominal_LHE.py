evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO without PDF and Scale variations, mT = 172.0 GeV -> MPI off'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mshapiro@lbl.gov','steffen.henkelmann@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.inputFilesPerJob=180
evgenConfig.nEventsPerJob=500


#--------------------------------------------------------------
# Event-LHE filter
#--------------------------------------------------------------
include('MCJobOptionUtils/LHEFilter.py')
include('./LHEFilter_SubLeadingTopPt.py')
SubleadTopPt = LHEFilter_SubLeadingTopPt()
SubleadTopPt.Ptcut = 700.
lheFilters.addFilter(SubleadTopPt)
lheFilters.run_filters()


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#myAdd : MPI off
genSeq.Pythia8.Commands += [ 'PartonLevel:MPI = off']
#-------------------------------------------------------------(
# Event-FinalState filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


filtSeq.Expression = "(not TTbarWToLeptonFilter)"

