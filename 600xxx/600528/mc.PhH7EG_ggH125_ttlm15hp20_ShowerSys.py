#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.description = "Powheg showered with Herwig7, mH=125 GeV, for systematics"
evgenConfig.process     = "ggF H->tautau->l-h+"
evgenConfig.keywords += ['Higgs', 'SMHiggs']
evgenConfig.contact  = ["Daniele Zanzi <daniele.zanzi@cern.ch>"]
#evgenConfig.minevents   = 500
#evgenConfig.inputfilecheck = "TXT"
evgenConfig.nEventsPerJob=10000
evgenConfig.inputFilesPerJob = 30
evgenConfig.tune = "H71-Default"

#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# To modify Higgs BR:
Herwig7Config.add_commands("""
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
 
set /Herwig/Particles/h0:NominalMass 125*GeV
set /Herwig/Particles/h0:Width 0.00407*GeV
set /Herwig/Particles/h0:WidthCut 0.00407*GeV
set /Herwig/Particles/h0:WidthLoCut 0.00407*GeV
set /Herwig/Particles/h0:WidthUpCut 0.00407*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->s,sbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 1

set /Herwig/Particles/tau+/tau+->nu_taubar,nu_e,e+;:OnOff 0
set /Herwig/Particles/tau+/tau+->nu_taubar,nu_mu,mu+;:OnOff 0
set /Herwig/Particles/tau-/tau-->nu_tau,nu_ebar,e-;:OnOff 1
set /Herwig/Particles/tau-/tau-->nu_tau,nu_mubar,mu-;:OnOff 1

set /Herwig/Particles/tau+/tau+->f_1,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,nu_taubar,[omega->pi0,gamma;];:OnOff 1
set /Herwig/Particles/tau+/tau+->phi,K+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K0,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K0,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,gamma,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,Kbar0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,Kbar0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,K-,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->eta,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K_S0,pi+,K_L0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi+,pi-,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K0,Kbar0,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi0,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,omega,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi+,pi-,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K_S0,K_S0,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,K_L0,K_L0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,eta,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K*+,eta,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->eta,pi+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->eta,pi+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->omega,pi+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->omega,pi+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,K-,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau-/tau-->f_1,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,nu_tau,[omega->pi0,gamma;];:OnOff 0
set /Herwig/Particles/tau-/tau-->phi,K-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->Kbar0,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->Kbar0,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi+,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,gamma,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,K0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,K0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K+,K-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->eta,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K_S0,pi-,K_L0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi+,pi-,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K0,Kbar0,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi0,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,omega,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi+,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K_S0,K_S0,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,K_L0,K_L0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,eta,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K*-,eta,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->eta,pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->eta,pi+,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->omega,pi+,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->omega,pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K+,K-,pi-,pi0,nu_tau;:OnOff 0

do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/tau-:PrintDecayModes
do /Herwig/Particles/tau+:PrintDecayModes

""")

# run Herwig7
Herwig7Config.run()


# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep15had20filter = TauFilter("lep15had20filter")
  filtSeq += lep15had20filter

filtSeq.lep15had20filter.UseNewOptions = True
filtSeq.lep15had20filter.Ntaus = 2
filtSeq.lep15had20filter.Nleptaus = 1
filtSeq.lep15had20filter.Nhadtaus = 1
filtSeq.lep15had20filter.EtaMaxlep = 2.6
filtSeq.lep15had20filter.EtaMaxhad = 2.6
filtSeq.lep15had20filter.Ptcutlep = 15000.0 #MeV
filtSeq.lep15had20filter.Ptcutlep_lead = 15000.0 #MeV
filtSeq.lep15had20filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep15had20filter.Ptcuthad_lead = 20000.0 #MeV
filtSeq.lep15had20filter.filterEventNumber = 1 # keep odd EventNumber events


