#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nnlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen, does not work in H7.2                                                                                               
#include("Herwig7_i/Herwig7_EvtGen.py") 

Herwig7Config.add_commands("""
 # force H->Zy decays
 do /Herwig/Particles/h0:SelectDecayModes h0->Z0,gamma;
 do /Herwig/Particles/h0:PrintDecayModes
 # print out decays modes and branching ratios to the terminal/log.generate
 do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+;
 do /Herwig/Particles/Z0:PrintDecayModes  
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description     = 'Powheg+Herwig7 VBF H->Zgamma->llgamma mh=125 GeV CPS'
evgenConfig.keywords        = [ "Higgs", "SMHiggs"]
evgenConfig.contact         = [ 'ana.cueto@cern.ch' ]
evgenConfig.generators     += [ 'Powheg', 'Herwig7' ]
evgenConfig.inputFilesPerJob = 2
evgenConfig.tune = "H7UE"
