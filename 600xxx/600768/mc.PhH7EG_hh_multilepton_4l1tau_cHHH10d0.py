# EVGEN Configuration
evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.description = "BSM diHiggs production, decay to multi-lepton, with Powheg-Box-V2 for ME and Herwig7 for shower"
evgenConfig.keywords = ["BSM", "hh", "nonResonant", "multilepton"]
evgenConfig.contact = ['Xiaozhong Huang <xiaozhong.huang@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 60


# Herwig 7 showering setup                                    
# -- initialize Herwig7 generator configuration for showering
include("Herwig7_i/Herwig7_LHEF.py")

# -- configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_30_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7.1-Default"

# -- add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# -- modify the BR to increase the filter efficiency
Herwig7Config.add_commands ("""
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->W+,W-; h0->Z0,Z0;
do /Herwig/Particles/h0:PrintDecayModes
""")


# Generator Filters
# -- #Ele + #Mu + #Tau >= 4
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("ElecMuTauFourFilter")
filtSeq.ElecMuTauFourFilter.IncludeHadTaus = True
filtSeq.ElecMuTauFourFilter.NLeptons = 4
filtSeq.ElecMuTauFourFilter.MinPt = 7000.
filtSeq.ElecMuTauFourFilter.MinVisPtHadTau = 15000.
filtSeq.ElecMuTauFourFilter.MaxEta = 2.8

# -- #Tau >= 1
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("TauOneFilter")
filtSeq.TauOneFilter.Ntaus = 1
filtSeq.TauOneFilter.Ptcuthad = 15000.
filtSeq.TauOneFilter.EtaMaxhad = 2.8
filtSeq.TauOneFilter.Ptcute = 1e9
filtSeq.TauOneFilter.EtaMaxe = -1.0
filtSeq.TauOneFilter.Ptcutmu = 1e9
filtSeq.TauOneFilter.EtaMaxmu = -1.0

# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

# -- #Ele + #Mu >= 1
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("ElecMuOneFilter")
filtSeq.ElecMuOneFilter.NLeptons = 1
filtSeq.ElecMuOneFilter.Ptcut = 7000.
filtSeq.ElecMuOneFilter.Etacut = 2.8

# -- Requirement 1: #Ele + #Mu + #Tau >= 4, #Tau >= 1
# -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
#                   Otherwise, we require pt > 13 GeV for leading light lepton
filtSeq.Expression="ElecMuTauFourFilter and TauOneFilter and (LeadingElecMuFilter or not ElecMuOneFilter)"


# Run Herwig7
Herwig7Config.run()
