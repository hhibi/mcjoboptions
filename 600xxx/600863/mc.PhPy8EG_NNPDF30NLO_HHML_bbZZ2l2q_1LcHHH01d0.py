#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")

fname = "merged_lhef._0.events"

f = open(fname, "r")
lines = f.readlines()
f.close()

f = open(fname, 'w')
for line in lines:
  if not "#pdf" in line:
    f.write(line)
f.close()

include("Pythia8_i/Pythia8_Powheg_Main31.py")

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5   100 23 23  ",   # ZZ decay
                             "25:addChannel = on 0.5   100 5 -5   ",   # bb decay 
                             "23:onMode = off",
                             "23:onIfAny= 1 2 3 4 5 11 13 15",
                             "24:mMin = 0", # W minimum mass
                             "24:mMax = 99999", # W maximum mass
                             "23:mMin = 0", # Z minimum mass
                             "23:mMax = 99999", # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "SM diHiggs production, decay to multi-lepton, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "ggF", "bbbar","ZZ"]
evgenConfig.contact        = ['Shuiting Xin <Shuiting.Xin@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 20
#evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 

# Generator Filters
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hZZFilter", PDGParent = [25], PDGChild = [23])
filtSeq += ParentChildFilter("ZllFilter", PDGParent = [23], PDGChild = [11,13,15])
filtSeq += ParentChildFilter("ZqqFilter", PDGParent = [23], PDGChild = [1,2,3,4,5])


# -- require HH->bbZZ
filtSeq.Expression = "hbbFilter and hZZFilter and ZllFilter and ZqqFilter"
