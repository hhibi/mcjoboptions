
process="ggH"
A_Mass=0.4

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
#infile = 'PowhegOTF._1.events'
for f in glob.glob("*.events"):
  infile = f
  f1 = open( infile )
  newfile = infile+'.temp'
  f2 = open(newfile,'w')
  for line in f1:
    if line.startswith('      25     1'):
      f2.write(line.replace('      25     1','      35     1'))
    else:
      f2.write(line)
  f1.close()
  f2.close()
  os.system('mv %s %s '%(infile, infile+'.old') )
  os.system('mv %s %s '%(newfile, infile) )
#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2'] #had to change .UserModes to .Commands and Main31:Nfinal to Powheg:NFinal
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']


# Can instead not use EvtGen. From James' D*gamma search.
# He doesn't know if mistake or because inclusive production made it necessary
# #--------------------------------------------------------------
# # Pythia8 showering
# # note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_Common.py
# #--------------------------------------------------------------


#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 1' ] # these user modes not present in the Haa2mu2tau JOs for some reason. This is the number of outgoing particles from the Born-level process. 2 by default, 1 in most other files
genSeq.Pythia8.Commands += [ 'WeakZ0:gmZmode = 2'] #
# Or use BSM style decay
H_Mass = 125.0
H_Width = 0.00407
A_Width = ( float(A_Mass) / 100. ) * 0.1 #100 MeV width for 100 GeV a
A_MassMin = float(A_Mass) - 100*float(A_Width)
A_MassMax = float(A_Mass) + 100*float(A_Width)
genSeq.Pythia8.Commands += [
  'Higgs:useBSM = on',
  '35:m0 = '+str(H_Mass),
  '35:mWidth = '+str(H_Width),
  '35:doForceWidth = on',
  '35:onMode = off',
  '35:onIfMatch = 23 36', # h->aa
  '23:onMode = off',
  '23:onIfAny = 11 13 15',
  '36:onMode = on', # decay of the a
  '36:m0 = '+str(A_Mass), #scalar mass
  '36:mWidth = '+str(A_Width), # narrow width
  '36:mMin = '+str(A_MassMin), # narrow width
  '36:mMax = '+str(A_MassMax) # narrow width
  ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->a0Z(ll)"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","Z", "mH125" ]
evgenConfig.contact     = [ 'mihaela.marinescu@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen" ]
evgenConfig.inputFilesPerJob = 10 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000
