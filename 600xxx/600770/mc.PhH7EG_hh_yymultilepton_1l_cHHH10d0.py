# EVGEN Configuration
evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.description = "BSM diHiggs production, decay to yy+multi-lepton, with Powheg-Box-V2 for ME and Herwig7 for shower"
evgenConfig.keywords = ["BSM", "BSMHiggs", "nonResonant", "multilepton"]
evgenConfig.contact = ['Xiaozhong Huang <xiaozhong.huang@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 12


# Herwig 7 showering setup                                    
# -- initialize Herwig7 generator configuration for showering
include("Herwig7_i/Herwig7_LHEF.py")

# -- configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_30_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7.1-Default"

# -- add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# -- modify the BR to increase the filter efficiency
Herwig7Config.add_commands ("""
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes

do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma; h0->tau-,tau+; h0->Z0,Z0; h0->W+,W-;
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
do /Herwig/Particles/h0:PrintDecayModes
""")


# Generator Filters
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq += ParentChildFilter("hXXFilter", PDGParent = [25], PDGChild = [15,23,24])

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepOneFilter")
filtSeq.LepOneFilter.IncludeHadTaus = True
filtSeq.LepOneFilter.NLeptons = 1
filtSeq.LepOneFilter.MinPt = 7000
filtSeq.LepOneFilter.MinVisPtHadTau = 15000
filtSeq.LepOneFilter.MaxEta = 3

filtSeq += MultiElecMuTauFilter("LepTwoFilter")
filtSeq.LepTwoFilter.IncludeHadTaus = True
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.MinPt = 7000
filtSeq.LepTwoFilter.MinVisPtHadTau = 15000
filtSeq.LepTwoFilter.MaxEta = 3

filtSeq.Expression = "hyyFilter and hXXFilter and LepOneFilter and not LepTwoFilter"


# Run Herwig7
Herwig7Config.run()
