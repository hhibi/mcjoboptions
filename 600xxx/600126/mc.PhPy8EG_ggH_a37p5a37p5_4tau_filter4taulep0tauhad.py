mmu = 0.10566 # GeV
mel = 0.000510 # GeV
ma = 37.5 #GeV
mH = 125. #GeV
mtau = 1.776 #GeV
process = "ggH"

dict_pdgIds = {}
dict_pdgIds["b"]   = 5
dict_pdgIds["mu"]  = 13
dict_pdgIds["tau"] = 15
dict_pdgIds["g"]   = 21
dict_pdgIds["y"]   = 22

#---------------
#Tau decay function 
#---------------
def fixNeutrinoMasses(nus):
    betaN = nus.BoostToCM()
    boostN = ROOT.Math.Boost(-betaN.X(),-betaN.Y(),-betaN.Z())
    theta = random.uniform(0,math.pi)
    phi = random.uniform(0,2.*math.pi)
    nu1 = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')((nus.M()/2.)*math.sin(theta)*math.cos(phi),(nus.M()/2.)*math.sin(theta)*math.sin(phi),(nus.M()/2.)*math.cos(theta),(nus.M()/2.))
    nu2 = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(-(nus.M()/2.)*math.sin(theta)*math.cos(phi),-(nus.M()/2.)*math.sin(theta)*math.sin(phi),-(nus.M()/2.)*math.cos(theta),(nus.M()/2.))
    nu1 = boostN(nu1)
    nu2 = boostN(nu2)
    return [nu1,nu2]

def triangleFunction(m1, m2, m3):
    return math.sqrt(math.pow(m1,4.)+math.pow(m2,4.)+math.pow(m3,4.)-2.*(math.pow(m1,2)*math.pow(m2,2)+math.pow(m1,2)*math.pow(m3,2)+math.pow(m2,2)*math.pow(m3,2)))/(2.*m3)

def decayScalar(higgs,md):
    
    mH = higgs.M()
    betaH = higgs.BoostToCM()
    boostH = ROOT.Math.Boost(-betaH.X(),-betaH.Y(),-betaH.Z())

    thetaa = random.uniform(0,math.pi)
    phia = random.uniform(0,2.*math.pi)
    sa = random.choice([-1,1])
    ca = random.choice([-1,1])
    pa = triangleFunction(md, md, mH)
    ea = math.sqrt(pa*pa+md*md)
    a1 = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(pa*math.sin(thetaa)*math.cos(phia),pa*math.sin(thetaa)*math.sin(phia),pa*math.cos(thetaa),ea)
    a2 = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(-pa*math.sin(thetaa)*math.cos(phia),-pa*math.sin(thetaa)*math.sin(phia),-pa*math.cos(thetaa),ea)
    a1_lab = boostH(a1)
    a2_lab = boostH(a2)

    return [a1_lab,sa,ca,a2_lab,-sa,-ca]

def partialWidth(m,e,costheta,s):
    prob = e*e*(2*m+(m-4*e)*(1+s*costheta))/(m*m*m/2)
    if prob > 1:
        print('WARNING: BAD PROBABILITY VALUE')
    return prob

def decayTau(tau,s,c):

    global mtau
    global mmu
    global mel

    betaT = tau.BoostToCM()
    boostT = ROOT.Math.Boost(-betaT.X(),-betaT.Y(),-betaT.Z())

    x = random.uniform(0,1)
    el = random.uniform(0,mtau/2)
    costhetal = random.uniform(-1,1) 
    phil = random.uniform(0,2.*math.pi)
    while (x > partialWidth(mtau,  el, costhetal, s)):
        el = random.uniform(0,mtau/2)
        costhetal = random.uniform(-1,1) 
        x = random.uniform(0,1)
    thetal = math.acos(costhetal)

    flavor = random.choice(['e', 'm'])
    lep = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(el*math.sin(thetal)*math.cos(phil),el*math.sin(thetal)*math.sin(phil),el*math.cos(thetal),el)
    nus = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(-el*math.sin(thetal)*math.cos(phil),-el*math.sin(thetal)*math.sin(phil),-el*math.cos(thetal),mtau-el) #this is both neutrinos

    pdgid = -1
    if flavor == 'e':
        lep.SetE(math.sqrt(el*el + mel*mel))
        pdgid = -11*c
    elif flavor == 'm':
        lep.SetE(math.sqrt(el*el + mmu*mmu))
        pdgid = -13*c
    nus = nus*(mtau - lep.e())/nus.e()
    
    lep_lab = boostT(lep)
    nus_lab = boostT(nus)    
    
    return [lep_lab, nus_lab, pdgid, c]

#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

import os, sys, glob
import random, math, ROOT
from datetime import datetime
random.seed(datetime.now())

for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      5  10001'):
            newline = '     19' + line[7:] 
            f2.write(newline)
        elif line.startswith('      25     1'):
            evtinfo = line.split()
            higgs = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(float(evtinfo[6]),float(evtinfo[7]),float(evtinfo[8]),float(evtinfo[9]))
            a1,s1,c1,a2,s2,c2 = decayScalar(higgs, ma)
            t11,s11,c11,t12,s12,c12 = decayScalar(a1, mtau)
            t21,s21,c21,t22,s22,c22 = decayScalar(a2, mtau)
            l11, n11, f11, c11 = decayTau(t11,s11,c11)
            l12, n12, f12, c12 = decayTau(t12,s12,c12)
            l21, n21, f21, c21 = decayTau(t21,s21,c21)
            l22, n22, f22, c22 = decayTau(t22,s22,c22)
            n111, n112 = fixNeutrinoMasses(n11)
            n121, n122 = fixNeutrinoMasses(n12)
            n211, n212 = fixNeutrinoMasses(n21)
            n221, n222 = fixNeutrinoMasses(n22)
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(25,2,1,2,0,0,higgs.Px(),higgs.Py(),higgs.Pz(),higgs.E(),higgs.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(36,2,3,3,0,0,a1.Px(),a1.Py(),a1.Pz(),a1.E(),a1.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(36,2,3,3,0,0,a2.Px(),a2.Py(),a2.Pz(),a2.E(),a2.M(),0.,9.))

            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(f11,1,4,4,0,0,l11.Px(),l11.Py(),l11.Pz(),l11.E(),l11.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(16,1,4,4,0,0,n111.Px(),n111.Py(),n111.Pz(),n111.E(),n111.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(-16,1,4,4,0,0,n112.Px(),n112.Py(),n112.Pz(),n112.E(),n112.M(),0.,9.))

            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(f12,1,4,4,0,0,l12.Px(),l12.Py(),l12.Pz(),l12.E(),l12.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(16,1,4,4,0,0,n121.Px(),n121.Py(),n121.Pz(),n121.E(),n121.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(-16,1,4,4,0,0,n122.Px(),n122.Py(),n122.Pz(),n122.E(),n122.M(),0.,9.))

            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(f21,1,5,5,0,0,l21.Px(),l21.Py(),l21.Pz(),l21.E(),l21.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(16,1,5,5,0,0,n211.Px(),n211.Py(),n211.Pz(),n211.E(),n211.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(-16,1,5,5,0,0,n212.Px(),n212.Py(),n212.Pz(),n212.E(),n212.M(),0.,9.))

            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(f22,1,5,5,0,0,l22.Px(),l22.Py(),l22.Pz(),l22.E(),l22.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(16,1,5,5,0,0,n221.Px(),n221.Py(),n221.Pz(),n221.E(),n221.M(),0.,9.))
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(-16,1,5,5,0,0,n222.Px(),n222.Py(),n222.Pz(),n222.E(),n222.M(),0.,9.))

        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )
#    os.system('cat %s '%(infile) )

#f2 = open(infile)
#print("+++++++++++++++++++++++++")
#for line in f2:
#    print(line)
#f2.close()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2']
genSeq.Pythia8.Commands += [
                            #'Higgs:useBSM = on',
                            #'35:m0 = 125',
                            #'35:mWidth = 0.00407',
                            #'35:doForceWidth = on',
                            #'35:onMode = off',
                            '35:onMode = off',
                            '36:onMode = off',
                            '25:onMode = off',
                            ]


evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->4tau mh=125 GeV"
evgenConfig.process     = "ggH H->aa->4tau"
evgenConfig.contact     = [ 'peter.tornambe@cern.ch']

# Filter
# from GeneratorFilters.GeneratorFiltersConf import TauFilter
# lfvfilter = TauFilter("lfvfilter")
# filtSeq += lfvfilter
# filtSeq.lfvfilter.UseNewOptions = True
# filtSeq.lfvfilter.Ntaus = 4
# filtSeq.lfvfilter.Nleptaus = 4
# filtSeq.lfvfilter.Nhadtaus = 0
# filtSeq.lfvfilter.EtaMaxlep = 2.7
# filtSeq.lfvfilter.EtaMaxhad = 2.7
# filtSeq.lfvfilter.Ptcutlep = 3000.0 MeV                                                                                                                                      
# filtSeq.lfvfilter.Ptcutlep_lead = 3000.0 MeV                                                                                                                                 
# filtSeq.lfvfilter.Ptcuthad = 12500.0 MeV                                                                                                                                     
# filtSeq.lfvfilter.Ptcuthad_lead = 12500.0 MeV                                                                                                                                

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 5
