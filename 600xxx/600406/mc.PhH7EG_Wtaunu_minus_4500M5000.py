# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

decay_mode = "w- > tau- vt~"


mass_low=4500
mass_high=5000

include("PowhegControl_W_H7.py")

evgenConfig.nEventsPerJob = 2000
