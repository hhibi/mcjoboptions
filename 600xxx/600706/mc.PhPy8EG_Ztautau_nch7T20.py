#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361108

include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 550
PowhegConfig.running_width = 1
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["oldrich.kepka@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.nEventsPerJob= 200

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
### Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

from GeneratorFilters.GeneratorFiltersConf import TauFilter
tauLepFilter = TauFilter("tauLepFilter")
filtSeq += tauLepFilter

filtSeq.tauLepFilter.Ntaus = 2 
filtSeq.tauLepFilter.EtaMaxe = 2.7 
filtSeq.tauLepFilter.EtaMaxmu = 2.7 
filtSeq.tauLepFilter.EtaMaxhad = 2.7 # no hadronic tau decays
filtSeq.tauLepFilter.Ptcute = 12000.0
filtSeq.tauLepFilter.Ptcutmu = 12000.0

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 7
filtSeq.ChargedTracksWeightFilter.NchMax = 20
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 15, 25, 35 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.01, 0.01, 0.03, 0.065, 0.085, 0.085 ]
