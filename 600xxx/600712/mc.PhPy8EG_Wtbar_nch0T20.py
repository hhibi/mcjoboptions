#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen Wt production (top), DR scheme, dilepton, with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', '2lepton']
evgenConfig.contact     = [ 'filip.nechansky@cern.ch', 'oldrich.kepka@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.nEventsPerJob   = 50

#--------------------------------------------------------------
# Powheg Wt setup - V2
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_Wt_DR_Common.py')
if hasattr(PowhegConfig, 'topdecaymode'):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 11100 # leptonic W decays
    PowhegConfig.wdecaymode = 11100 # leptonic W decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode_top = 't~ > b~ l- vl~'
    PowhegConfig.decay_mode_W = 'w > l vl'
PowhegConfig.hdamp        = 258.75 # 1.5 * mtop
PowhegConfig.nEvents     *= 4500    # Add safety factor
PowhegConfig.PDF = 260000
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 0
filtSeq.ChargedTracksWeightFilter.NchMax = 20
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 10, 15, 20, 30, 35 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 9e-05, 9e-05, 0.0005, 0.003, 0.009, 0.02, 0.04, 0.05 ]


