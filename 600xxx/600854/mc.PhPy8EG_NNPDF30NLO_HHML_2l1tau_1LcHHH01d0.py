#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")

fname = "merged_lhef._0.events"

f = open(fname, "r")
lines = f.readlines()
f.close()

f = open(fname, 'w')
for line in lines:
  if not "#pdf" in line:
    f.write(line)
f.close()

include("Pythia8_i/Pythia8_Powheg_Main31.py")

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 ",   # WW decay
                             "25:addChannel = on 0.086 100 23 23  ",   # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 ",   # tautau decay 
                             "24:mMin = 0", # W minimum mass
                             "24:mMax = 99999", # W maximum mass
                             "23:mMin = 0", # Z minimum mass
                             "23:mMax = 99999", # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "SM diHiggs production, decay to multi-lepton, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "ggF", "multlepton"]
evgenConfig.contact        = ['Shuiting Xin <Shuiting.Xin@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 14

# #---------------------------------------------------------------------------------------------------
# # Generator Filters
# #---------------------------------------------------------------------------------------------------
# Generator Filters
# -- #Ele + #Mu + #Tau >= 2
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("ElecMuTauTwoFilter")
filtSeq.ElecMuTauTwoFilter.IncludeHadTaus = True
filtSeq.ElecMuTauTwoFilter.NLeptons = 2
filtSeq.ElecMuTauTwoFilter.MinPt = 7000.
filtSeq.ElecMuTauTwoFilter.MinVisPtHadTau = 15000.
filtSeq.ElecMuTauTwoFilter.MaxEta = 2.8

# -- #Ele + #Mu + #Tau >= 3
filtSeq += MultiElecMuTauFilter("ElecMuTauThreeFilter")
filtSeq.ElecMuTauThreeFilter.IncludeHadTaus = True
filtSeq.ElecMuTauThreeFilter.NLeptons = 3
filtSeq.ElecMuTauThreeFilter.MinPt = 7000.
filtSeq.ElecMuTauThreeFilter.MinVisPtHadTau = 15000.
filtSeq.ElecMuTauThreeFilter.MaxEta = 2.8

# -- #Ele + #Mu >= 2
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("ElecMuTwoFilter")
filtSeq.ElecMuTwoFilter.NLeptons = 2
filtSeq.ElecMuTwoFilter.Ptcut = 7000.
filtSeq.ElecMuTwoFilter.Etacut = 2.8

# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

# -- #Ele + #Mu >= 1
filtSeq += MultiLeptonFilter("ElecMuOneFilter")
filtSeq.ElecMuOneFilter.NLeptons = 1
filtSeq.ElecMuOneFilter.Ptcut = 7000.
filtSeq.ElecMuOneFilter.Etacut = 2.8

# -- Requirement 1: #Ele + #Mu + #Tau == 2,  #Tau >= 1
# -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
#                   Otherwise, we require pt > 13 GeV for leading light lepton
filtSeq.Expression="(ElecMuTauTwoFilter and not ElecMuTauThreeFilter) and not ElecMuTwoFilter and (LeadingElecMuFilter or not ElecMuOneFilter)"
