#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->ll minus [cbarc, bbarb] production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs"]
evgenConfig.contact     = [ 'stephen.jiggins@cern.ch' ]
evgenConfig.process = "ZH, H->[gg,qq,ll,ZZ,WW] off, Z->ll"
evgenConfig.inputFilesPerJob=2

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']


#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = on', # decay of Higgs
                             '25:offIfAny = 4 5' ] # Turn off for b\bar{b} & c\bar{c}

