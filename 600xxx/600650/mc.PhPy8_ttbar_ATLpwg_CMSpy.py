#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'ATLAS POWHEG ttbar production with hdamp equal 1.5*top mass, ME NNPDF30 NLO, CMS Pythia8 settings.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'm.fenton@cern.ch']
evgenConfig.generators  = [ 'Powheg', 'Pythia8' ]



include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
#PowhegConfig.topdecaymode = 22222                                         # Inclusive
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"
PowhegConfig.hdamp        = 258.75                                        # 1.5 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260000, 25200, 13165, 90900]                 # NNPDF30, MMHT, CT14, PDF4LHC - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260001, 260101))                            # Include the NNPDF error set
PowhegConfig.PDF.extend(range(90901 , 90931 ))                            # Include the PDF4LHC error set

# Explicitly set the current ATLAS parameter set, which has already been set above, from these files:
# https://gitlab.cern.ch/atlas/athena/blob/21.6/Generators/PowhegControl/share/control/PowhegControl_tt_Common.py
# https://gitlab.cern.ch/atlas/athena/blob/21.6/Generators/PowhegControl/python/parameters/atlas_common.py
# 
# Taken on 06.03.2020
#PowhegConfig._generate_run_card()
PowhegConfig.__setattr__( 'mass_b',       4.95,   )# desc='b quark mass in t decay' )
PowhegConfig.__setattr__( 'mass_c',       1.55,   )# desc='c quark mass' )
PowhegConfig.__setattr__( 'mass_u',       0.32,   )# desc='u quark mass' )
PowhegConfig.__setattr__( 'mass_d',       0.32,   )# desc='d quark mass' )
PowhegConfig.__setattr__( 'mass_s',       0.50,   )# desc='s quark mas
PowhegConfig.__setattr__( 'width_t',      1.32,   )# desc='top width' )
PowhegConfig.__setattr__( 'width_W',      2.085,  )# desc='W width' )
PowhegConfig.__setattr__( 'mass_W',       80.399, )# desc='W mass for top decay' )
PowhegConfig.__setattr__( 'sin2cabibbo', 0.051,  )# desc='sine of Cabibbo angle squared' )

PowhegConfig.minlo        = -1
PowhegConfig.__setattr__( 'mass_t',  172.5 )

#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Define a weight group configuration for scale variations with different PDFs
# Nominal mu_F = mu_R = 1.0 is not required as this is captured by the PDF variation above
PowhegConfig.define_event_weight_group( group_name='scales_pdf', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_MMHT',                   parameter_values=[ 2.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_MMHT',                 parameter_values=[ 0.5, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_MMHT',                   parameter_values=[ 1.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_MMHT',                 parameter_values=[ 1.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_MMHT',          parameter_values=[ 0.5, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_MMHT',              parameter_values=[ 2.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_MMHT',            parameter_values=[ 0.5, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_MMHT',            parameter_values=[ 2.0, 0.5, 25200] )

# Scale variations, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_CT14',                   parameter_values=[ 2.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_CT14',                 parameter_values=[ 0.5, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_CT14',                   parameter_values=[ 1.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_CT14',                 parameter_values=[ 1.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_CT14',          parameter_values=[ 0.5, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_CT14',              parameter_values=[ 2.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_CT14',            parameter_values=[ 0.5, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_CT14',            parameter_values=[ 2.0, 0.5, 13165] )

# Scale variations, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_PDF4LHC15',              parameter_values=[ 2.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_PDF4LHC15',            parameter_values=[ 0.5, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_PDF4LHC15',              parameter_values=[ 1.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_PDF4LHC15',            parameter_values=[ 1.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_PDF4LHC15',     parameter_values=[ 0.5, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_PDF4LHC15',         parameter_values=[ 2.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_PDF4LHC15',       parameter_values=[ 0.5, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_PDF4LHC15',       parameter_values=[ 2.0, 0.5, 90900] )

#PowhegConfig.nEvents     *= 3. # compensate filter efficiency
PowhegConfig.generate()


#################################
# CMS Pythia8 Settings
#################################
from Pythia8_i.Pythia8_iConf import Pythia8_i
genSeq += Pythia8_i("Pythia8")
evgenConfig.generators += ["Pythia8"]
if "StoreLHE" in genSeq.Pythia8.__slots__.keys():
    print "Pythia8_Base_Fragment.py: DISABLING storage of LHE record in HepMC by default. Please reenable storeage if desired"
    genSeq.Pythia8.StoreLHE = False

genSeq.Pythia8.UseLHAPDF=False

assert hasattr(genSeq,"Pythia8")
genSeq.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)

#genSeq.Pythia8.Commands += [
#"Tune:ee = 7",
#"Tune:pp = 14",
#"PDF:useLHAPDF = on",
#"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
#]

#checked - matches cms (removed some related to uncertainty bands
genSeq.Pythia8.Commands += [ 'TimeShower:mMaxGamma = 1.0',
'6:m0 = 172.5']

genSeq.Pythia8.Commands += [
'Tune:pp 14',
'Tune:ee 7',
'MultipartonInteractions:ecmPow=0.03344',
'PDF:pSet=20',
'MultipartonInteractions:bProfile=2',
'MultipartonInteractions:pT0Ref=1.41',
'MultipartonInteractions:coreRadius=0.7634',
'MultipartonInteractions:coreFraction=0.63',
'ColourReconnection:range=5.176',
'SigmaTotal:zeroAXB=off',
'SpaceShower:alphaSorder=2',
'SpaceShower:alphaSvalue=0.118',
'SigmaProcess:alphaSvalue=0.118',
'SigmaProcess:alphaSorder=2',
'MultipartonInteractions:alphaSvalue=0.118',
'MultipartonInteractions:alphaSorder=2',
'TimeShower:alphaSorder=2',
'TimeShower:alphaSvalue=0.118']

#checked - matches CMS
genSeq.Pythia8.Commands += ['Tune:preferLHAPDF = 2',
'Main:timesAllowErrors = 10000',
'Check:epTolErr = 0.01',
'Beams:setProductionScalesFromLHEF = off',
'SLHA:keepSM = on',
'SLHA:minMassSM = 1000.',
'ParticleDecays:limitTau0 = on',
'ParticleDecays:tau0Max = 10',
'ParticleDecays:allowPhotonRadiation = on']
        
#checked - matches CMS        
genSeq.Pythia8.Commands += ['POWHEG:veto = 1',
'POWHEG:pTdef = 1',
'POWHEG:emitted = 0',
'POWHEG:pTemt = 0',
'POWHEG:pThard = 0',
'POWHEG:vetoCount = 100',
'SpaceShower:pTmaxMatch = 2',
'TimeShower:pTmaxMatch = 2']
 
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
#include('GeneratorFilters/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

