# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG NC DY production with NLO EW correction"
evgenConfig.keywords = ["SM", "Z"]
evgenConfig.contact = ["amoroso@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 5000                                                                                                                       

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Z_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.decay_mode = "z > mu+ mu-"
PowhegConfig.no_ew=0
PowhegConfig.ew_ho=1
PowhegConfig.ptsqmin=4
PowhegConfig.mass_low=40
PowhegConfig.PHOTOS_enabled = True
PowhegConfig.nEvents *= 1.3

#Gmu EW scheme inputs matching what used in the Powheg V1 samples
PowhegConfig.scheme=0
PowhegConfig.alphaem=0.00781653
PowhegConfig.mass_W=79.958059

PowhegConfig.PDF = range(10800, 10852+1) #CT10nnlo
PowhegConfig.PDF.extend(range(13000, 13056+1)) #CT14nnlo                                                                                               
PowhegConfig.PDF.extend(range(303600,303700+1)) #NNPDF3.1nnlo                                                                                                
PowhegConfig.mu_F = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0]                                                                                            
PowhegConfig.mu_R = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5]                              

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------                                                                                      
# Shower settings                                                                                                                                     
# --------------------------------------------------------------                                                                                         
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#Switching off Pythia8 QED showering and shower weights                                                                                                      
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByOther = off",
                            "TimeShower:QEDshowerByL = off",
                            "TimeShower:QEDshowerByGamma = off",
                            "TimeShower:QEDshowerByL = off",
                            "SpaceShower:QEDshowerByQ = off",
                            "SpaceShower:QEDshowerByL = off",
                            "UncertaintyBands:doVariations=off"]
