#--------------------------------------------------------------
# Powheg W setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_W_Common.py')
PowhegConfig.decay_mode = "w+ > mu+ vm"

# Configure Powheg setup
PowhegConfig.withdamp   = 1
PowhegConfig.ptsqmin    = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 1.2 # increase number of generated events by 20%
PowhegConfig.running_width = 1

PowhegConfig.generate()

########################################################
# replace the W decay products with the pi gamma decay
########################################################
import os, sys, math, random
random.seed()
from ROOT import TLorentzVector
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      24     2'):
        sline=line.split()        
        Z_px=float(sline[6])
        Z_py=float(sline[7])
        Z_pz=float(sline[8])
        Z_E=float(sline[9])

        Zmom = TLorentzVector()
        Zmom.SetPxPyPzE(Z_px,Z_py,Z_pz,Z_E)
        # Rho Mass - Pythia8 number
        mC1=0.77549
        # Gamma Mass
        mC2=0.0

        mP = Zmom.M()
        E1 = (mP*mP + mC1*mC1)/(2.0*mP)
        E2 = (mP*mP - mC1*mC1)/(2.0*mP)
        P = 0.5*math.sqrt( (mP - mC1) * (mP + mC1) * (mP + mC1) * (mP - mC1) ) / mP

        PrtBoost = Zmom.BoostVector()
        cosTheta = 2. * random.random() - 1.  
        sinTheta = math.sqrt(1. - cosTheta*cosTheta)
        phi12    = 2. * math.acos(-1.0) * random.random()
        pX       = P * sinTheta * math.cos(phi12);  
        pY       = P * sinTheta * math.sin(phi12);  
        pZ       = P * cosTheta;  

        dir=PrtBoost.Unit();
        Child1=TLorentzVector()
        Child2=TLorentzVector()
        Child1.SetPxPyPzE(pX, pY, pZ, E1);
        Child2.SetPxPyPzE(-pX, -pY, -pZ, E2);
        Child1.RotateUz(dir);
        Child2.RotateUz(dir);
        Child1.Boost(PrtBoost);
        Child2.Boost(PrtBoost);
	
        #Write out the W entry
        f2.write(line)
        #Write out the pion entry
        f2.write('     213     1 {0:5d} {1:5d}     0     0 {2:16.9E} {3:16.9E} {4:16.9E} {5:16.9E} {6:16.9E} {7:12.5E} {8:10.3E}\n'.format(3,3,Child1.Px(),Child1.Py(),Child1.Pz(),Child1.E(),Child1.M(),0,9))
        #Write out the gamma entry
        f2.write('      22     1 {0:5d} {1:5d}     0     0 {2:16.9E} {3:16.9E} {4:16.9E} {5:16.9E} {6:16.9E} {7:12.5E} {8:10.3E}\n'.format(3,3,Child2.Px(),Child2.Py(),Child2.Pz(),Child2.E(),0.0,0,9))
        
    elif line.startswith('      13     1')  or line.startswith('     -13     1'):
        # remove the muons
        pass
    elif line.startswith('      14     1')  or line.startswith('     -14     1'):
    	pass
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Wplus->rho+ gamma production with AZNLO CT10 tune'
evgenConfig.contact = ["Carlo A. Gottardo <carlo.gottardo@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'W', 'drellYan']
evgenConfig.generators  = [ "Pythia8", "Powheg","EvtGen", "Photos"]


