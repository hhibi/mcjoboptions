'''
asetup 21.6.17,AthGeneration,here
Gen_tf.py --ecmEnergy=13000. --firstEvent=1 --randomSeed=1 --outputEVNTFile=evnt.test.pool.root --jobConfig=999xxx/999999/ --maxEvents=50 --inputGeneratorFile=/afs/cern.ch/user/b/bnachman/work/public/3dbumphuntsignals/5TeV/haa/mc15_13TeV.345055.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT.evgen.TXT.e5706/TXT.14277286._035906.tar.gz.1
'''

shower = "H7" #P8
decay = "gg" #bb
ma = 4. #GeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Powheg + '+shower+' ZH, H->aa->'+decay+decay+' with m_a = '+str(ma)+' GeV  By-hand decay of the H and a using a flat matrix element.'
evgenConfig.keywords    =['Higgs','BSM']
if (shower=="H7"):
    evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
    evgenConfig.tune        = "H7.1-Default"
elif (shower=="P8"):
    evgenConfig.generators += ["Powheg", "Pythia8", "EvtGen"]
    evgenConfig.tune = "A14NNPDF23LO"
evgenConfig.contact     = [ 'bnachman@cern.ch']

import random
import math

class particle:

    def __init__(self, px=0, py=0, pz=0, m=0):
        self.px = px
        self.py = py
        self.pz = pz
        self.m = m
        pass

    def p(self):
        return (self.px ** 2 + self.py ** 2 + self.pz ** 2) ** 0.5

    def E(self):
        return (self.px ** 2 + self.py ** 2 + self.pz ** 2 + self.m ** 2) ** 0.5

    def boost(self, betavec):
        bx = betavec[0]
        by = betavec[1]
        bz = betavec[2]
        beta = (bx ** 2 + by ** 2 + bz ** 2) ** 0.5
        gamma = 1. / (1. - beta ** 2) ** 0.5
        E = self.E()
        mydotprod = bx * self.px + by * self.py + bz * self.pz
        Enew = gamma * (E + mydotprod)
        self.px = self.px + (gamma - 1.) / (beta ** 2) * \
            mydotprod * bx + gamma * E * bx
        self.py = self.py + (gamma - 1.) / (beta ** 2) * \
            mydotprod * by + gamma * E * by
        self.pz = self.pz + (gamma - 1.) / (beta ** 2) * \
            mydotprod * bz + gamma * E * bz
        E = Enew
    pass

def two_body_decay(mM, mD, mS):
    #M = mother, D = daughter, S = son
    pD = abs((mM ** 2 - (mD - mS) ** 2) * (mM ** 2 - (mD + mS) ** 2)) ** 0.5 / (2. * mM)
    cosThetaD = random.uniform(-1., 1.)
    sinThetaD = (1. - pow(cosThetaD, 2)) ** 0.5
    phiD = random.uniform(0., 2. * math.pi)
    p4D = particle(pD * sinThetaD * math.cos(phiD), pD *
                   sinThetaD * math.sin(phiD), pD * cosThetaD, mD)
    p4S = particle(-pD * sinThetaD * math.cos(phiD), -pD *
                   sinThetaD * math.sin(phiD), -pD * cosThetaD, mS)
    return p4D, p4S

def scinot(myin):
    if (myin < 0):
        return format(myin, "1.9E")
    else:
        return " "+format(myin, "1.9E")

md = 1.
if (decay == 'gg'):
    md = 0.1 #had problems with square root of negative number if this was not set to some small but positive value.
elif (decay == 'bb'):
    md = 5. #should check what Pythia/Herwig actually use
    pass

import os, sys, glob
myfiles = []
for f in glob.glob("*.events"):
    myfiles+=[f]
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            #f2.write(line.replace('      25     1','      35     1')) #Had problems with Herwig7 since it doesn't know about the 35 particle unless you explicitly tell it.
            px = float(line.split()[6])
            py = float(line.split()[7])
            pz = float(line.split()[8])
            E = float(line.split()[9])
            m = float(line.split()[10])
            p4H = particle(px,py,pz,m)
            p4a1,p4a2 = two_body_decay(m, ma, ma)
            betaH = [p4H.px / p4H.E(), p4H.py / p4H.E(), p4H.pz / p4H.E()]
            p4a1.boost(betaH)
            p4a2.boost(betaH)
            p4a1a,p4a1b = two_body_decay(ma, md, md)
            p4a2a,p4a2b = two_body_decay(ma, md, md)
            betaa1 = [p4a1.px / p4a1.E(), p4a1.py / p4a1.E(), p4a1.pz / p4a1.E()]
            betaa2 = [p4a2.px / p4a2.E(), p4a2.py / p4a2.E(), p4a2.pz / p4a2.E()]
            p4a1a.boost(betaa1)
            p4a1b.boost(betaa1)
            p4a2a.boost(betaa2)
            p4a2b.boost(betaa2)
            p4a1px = float(scinot(p4a1.px))
            p4a1py = float(scinot(p4a1.py))
            p4a1pz = float(scinot(p4a1.pz))
            p4a1E = float(scinot(p4a1.E()))
            p4a2px = float(scinot(px-p4a1px))
            p4a2py = float(scinot(py-p4a1py))
            p4a2pz = float(scinot(pz-p4a1pz))
            p4a2E = float(scinot(E-p4a1E))
            p4a1apx = float(p4a1a.px)
            p4a1apy = float(p4a1a.py)
            p4a1apz = float(p4a1a.pz)
            p4a1aE = float(p4a1a.E())
            p4a1bpx = float(scinot(p4a1px - p4a1apx))
            p4a1bpy = float(scinot(p4a1py - p4a1apy))
            p4a1bpz = float(scinot(p4a1pz - p4a1apz))
            p4a1bE = float(scinot(p4a1E - p4a1aE))
            p4a2apx = float(p4a2a.px)
            p4a2apy = float(p4a2a.py)
            p4a2apz = float(p4a2a.pz)
            p4a2aE = float(p4a2a.E())
            p4a2bpx = float(scinot(p4a2px - p4a2apx))
            p4a2bpy = float(scinot(p4a2py - p4a2apy))
            p4a2bpz = float(scinot(p4a2pz - p4a2apz))
            p4a2bE = float(scinot(p4a2E - p4a2aE))
            p4a1m = (p4a1E**2-p4a1px**2-p4a1py**2-p4a1pz**2)**0.5
            p4a2m = (p4a2E**2-p4a2px**2-p4a2py**2-p4a2pz**2)**0.5
            p4a1am = (p4a1aE**2-p4a1apx**2-p4a1apy**2-p4a1apz**2)**0.5
            p4a2am = (p4a2aE**2-p4a2apx**2-p4a2apy**2-p4a2apz**2)**0.5
            p4a1bm = (p4a1bE**2-p4a1bpx**2-p4a1bpy**2-p4a1bpz**2)**0.5
            p4a2bm = (p4a2bE**2-p4a2bpx**2-p4a2bpy**2-p4a2bpz**2)**0.5
            f2.write('      25     2     1     2     0     0 '+scinot(p4a1px)+" "+scinot(p4a1py)+" "+scinot(p4a1pz)+" "+scinot(p4a1E)+" "+scinot(p4a1m)+"  0.00000E+00  9.000E+00\n")
            f2.write('      25     2     1     2     0     0 '+scinot(p4a2px)+" "+scinot(p4a2py)+" "+scinot(p4a2pz)+" "+scinot(p4a2E)+" "+scinot(p4a2m)+"  0.00000E+00  9.000E+00\n")
            f2.write('      21     1     3     3   520   521 '+scinot(p4a1apx)+" "+scinot(p4a1apy)+" "+scinot(p4a1apz)+" "+scinot(p4a1aE)+" "+scinot(p4a1am)+"  0.00000E+00  9.000E+00\n")
            f2.write('      21     1     3     3   521   520 '+scinot(p4a1bpx)+" "+scinot(p4a1bpy)+" "+scinot(p4a1bpz)+" "+scinot(p4a1bE)+" "+scinot(p4a1bm)+"  0.00000E+00  9.000E+00\n")
            f2.write('      21     1     4     4   522   523 '+scinot(p4a2apx)+" "+scinot(p4a2apy)+" "+scinot(p4a2apz)+" "+scinot(p4a2aE)+" "+scinot(p4a2am)+"  0.00000E+00  9.000E+00\n")
            f2.write('      21     1     4     4   523   522 '+scinot(p4a2bpx)+" "+scinot(p4a2bpy)+" "+scinot(p4a2bpz)+" "+scinot(p4a2bE)+" "+scinot(p4a2bm)+"  0.00000E+00  9.000E+00\n")
        elif '4     4' in line:
            f2.write(line.replace("4     4","9     9"))
        elif '8  10001' in line:
            f2.write(line.replace("8  10001","13  10001"))
        elif '7  10001' in line:
            f2.write(line.replace("7  10001","12  10001"))
        elif '6  10001' in line:
            f2.write(line.replace("6  10001","11  10001"))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    #if (shower=="H7"):
    #    os.system('mv %s %s '%(newfile, 'PowhegOTF._1.events') )
    #elif (shower=="P8"):
    os.system('mv %s %s '%(newfile, infile) )
    pass

if (shower=="H7"):
    
    # initialize Herwig7 generator configuration for showering of LHE files 
    include("Herwig7_i/Herwig7_LHEF.py")

    # configure Herwig7  
    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_powhegbox_commands(lhe_filename='merged_lhef._0.events', me_pdf_order="NLO")
    #Herwig7Config.lhef_powhegbox_commands(lhe_filename="PowhegOTF._1.events", me_pdf_order="NLO")
    
    # add EvtGen 
    include("Herwig7_i/Herwig71_EvtGen.py")
    Herwig7Config.run()

elif (shower=="P8"):

    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
    include("Pythia8_i/Pythia8_Powheg_Main31.py")
    genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 4' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
    genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ] 

evgenConfig.inputFilesPerJob = 200
evgenConfig.nEventsPerJob = 10000
