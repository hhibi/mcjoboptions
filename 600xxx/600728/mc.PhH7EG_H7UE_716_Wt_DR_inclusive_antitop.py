#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark t-channel (2->3) production (antitop),MadSpin, ME NNPDF3.04f NLO, H7.1 Default tune'
evgenConfig.keywords    = ['SM', 'top']
evgenConfig.contact     = ['baptiste.ravina@cern.ch' ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.generators += ['Powheg','Herwig7','EvtGen']
#--------------------------------------------------------------
# Herwig7 (H71UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune        = "H7.1-Default"

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()
