# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

decay_mode = "w+ > mu+ vm"


mass_low=1000
mass_high=1250

include("PowhegControl_W_H7.py")

evgenConfig.nEventsPerJob = 2000
