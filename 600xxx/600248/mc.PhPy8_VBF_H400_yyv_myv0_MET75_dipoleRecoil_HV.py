#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 400.
PowhegConfig.width_H = 0.00407

PowhegConfig.complexpolescheme = 0 # do not use CPS

# Increase number of events requested to compensate for potential Pythia losses
PowhegConfig.nEvents *= 2

PowhegConfig.PDF = range(90400,90433) + range(260000,260101) + [11068] + [25200] + [13165]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0]

PowhegConfig.generate()

#--------------------------------------------------------------
# Extract out the parameters
#--------------------------------------------------------------
dict_pdgIds = {}
dict_pdgIds["y"]   = 22

def getParameters():
    import re

    #--- Read parts of the job option
    #--- Assumes the form MC15.111111.PowhegPy8EG_ggH_H125_Xy_Zd${m}_ctauYY
    jonamelist = jofile.rstrip(".py").split("_") 
    process = jonamelist[1]
    decayChan = jonamelist[3]
    mzd = float(jonamelist[4].split("myv")[1].replace("p", "."))

    return process, decayChan, mzd

process, decayChan, mzd  = getParameters()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
# For VBF, Nfinal = 3
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs-> y yv in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '25:m0 = 400.',
                            '25:mWidth = 0.00407',
                            '25:doForceWidth = on',
                            '25:onMode = off',
                            '25:addChannel = 1 1. 103 22 4900022',
                            '25:onIfMatch = 22 4900022',
                            '4900022:m0 = %.1f' % mzd,
                            '4900022:onMode = off',
                            '4900022:tau0 = 0',
                            '4900022:onIfAny = 12 14 16' # Just in case it does decay...neutrinos
                            ]

#--------------------------------------------------------------
# MET Filter 
#--------------------------------------------------------------
include("GeneratorFilters/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 75*GeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs"]
evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->yyv, Hidden Valley, mh=400 GeV NWA"
evgenConfig.process     = "VBF H->yyv"
evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ben.carlson@cern.ch' ]
evgenConfig.nEventsPerJob = 2000
