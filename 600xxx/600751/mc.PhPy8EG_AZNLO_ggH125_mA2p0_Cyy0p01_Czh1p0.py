#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------

def decaywidth(cAA = 1, MALP = 1):
    import math

    sw2=0.23
    cBB = cAA*(1 - sw2)
    cWW = cAA*sw2
    aEWM1=127.9
    aEW=1/aEWM1
    ee = 2*math.sqrt(aEW)*math.sqrt(math.pi)
    Lambda=1000

    return (pow(MALP,2)*((8*cBB*cBB*pow(ee,4)*pow(MALP,4))/pow(Lambda,2) + (16*cBB*cWW*pow(ee,4)*pow(MALP,4))/pow(Lambda,2) + (8*cWW*cWW*pow(ee,4)*pow(MALP,4))/pow(Lambda,2)))/(32.*math.pi*pow(MALP,3))

def lifetime(width=1):
    return 3.0E11*6.582E-16/float(width)/1E9 ##mm


A_Mass = float(2.0)
Cyy = float(0.01)
Czh = float(1.0)


A_Width = decaywidth(Cyy,A_Mass)
A_Ctau = lifetime(A_Width)
print '================================================================='
print 'Mass of ALP: ',A_Mass,' Cyy: ',Cyy,' Czh: ',Czh,' Ctau: ',A_Ctau
print '================================================================='



#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

# Can instead not use EvtGen. From James' D*gamma search.
# He doesn't know if mistake or because inclusive production made it necessary
# #--------------------------------------------------------------
# # Pythia8 showering
# # note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_Common.py
# #--------------------------------------------------------------


#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
#genSeq.Pythia8.Commands += [ 'WeakZ0:gmZmode = 2'] #
# Or use BSM style decay
H_Mass = 125.0
H_Width = 0.00407
A_MassMin = float(A_Mass) - float(A_Width)
A_MassMax = float(A_Mass) + float(A_Width)
genSeq.Pythia8.Commands += [
  '35:new = a0 a0 1 0 0 '+str(A_Mass)+' 0 0 0 '+str(A_Ctau),
  '35:oneChannel = 1 1.0 0 22 22',
  #'HiggsSM:gg2H = on',
  '25:onMode = off',
  '25:addChannel = 0 0.1 103 23 35',
  '25:onIfMatch = 23 35',
  #Force H to have 750 GeV mass and yet be narrow.
  '25:m0 = 125.',
  '25:mWidth = 0.00407',
  '25:doForceWidth = on',
  ##
  ##
#  'Higgs:useBSM = on',
#  '35:m0 = '+str(H_Mass),
#  '35:mWidth = '+str(H_Width),
#  '35:doForceWidth = on',
#  '35:onMode = off',
#  '35:onIfMatch = 23 36', # h->Za
  '23:onMode = off',
  '23:onIfAny = 11 13 15',
#  '36:onMode = off', # decay of the a
#  '36:onIfAny = 22', # decay of the yy
#  '36:m0 = '+str(A_Mass), #scalar mass
#  '36:mWidth = '+str(A_Width), # narrow width
#  #'36:mMin = '+str(A_MassMin), # narrow width
#  #'36:mMax = '+str(A_MassMax), # narrow width
#  '36:tau0 = '+str(A_Ctau), #scalar lifetime [mm/c]
#  'ParticleDecays:tau0Max = 10000.0', #some very large value
#  'ParticleDecays:limitTau0 = off',
#
  ]

#testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]


from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
filtSeq += DiLeptonMassFilter()
filtSeq.DiLeptonMassFilter.MinPt = 10000.
filtSeq.DiLeptonMassFilter.MaxEta = 2.7
filtSeq.DiLeptonMassFilter.MinMass = 76000.
filtSeq.DiLeptonMassFilter.MaxMass = 106000.
filtSeq.DiLeptonMassFilter.MinDilepPt = 0.
filtSeq.DiLeptonMassFilter.AllowSameCharge = False

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->a0(yy)Z(ll), mH=125 GeV, mA0="+str(A_Mass)+" GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","Z", "mH125" ]
evgenConfig.contact     = [ 'renjie.wang@cern.ch', 'cristiano.sebastiani@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen" ]

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 15

