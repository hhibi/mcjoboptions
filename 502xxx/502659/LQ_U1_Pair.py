# Job options for vector leptoquark (U1) pair production
# To be used for MC16 in r21.6 (tested with 21.6.45)

from MadGraphControl.MadGraphUtils import *
import os
import re

# maxEvents given when running Gen_tf.py
nevents=runArgs.maxEvents

bonus_file = open("pdgid_extras.txt", "w")
bonus_file.write("9000007\n-9000007")
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print job_option_name

matchesMass = re.search("M([0-9]+).*\.py", job_option_name)
if matchesMass is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matchesMass.group(1))

# Set couplings
decays = []
JOlist = job_option_name.split("_")

gU = 3.0
if "gU" in JOlist:
    gU = eval( JOlist[ JOlist.index("gU")+1 ].replace('p','.'))

betaL33 = 0.0
if "bL33" in JOlist:
    betaL33 = eval( JOlist[ JOlist.index("bL33")+1 ].replace('p','.'))
if betaL33 != 0:
    decays.extend(['btaL', 'tvtL'])

betaRd33 = 0.0
if "bR33" in JOlist:
    betaRd33 = eval( JOlist[ JOlist.index("bR33")+1 ].replace('p','.'))
if betaRd33 != 0:
    decays.append('btaR')

betaL23 = 0.0
if "bL23" in JOlist:
    betaL23 = eval( JOlist[ JOlist.index("bL23")+1 ].replace('p','.'))
if betaL23 != 0:
    decays.extend(['staL', 'cvtL'])
betaL32 = 0.0
if "bL32" in JOlist:
    betaL32 = eval( JOlist[ JOlist.index("bL32")+1 ].replace('p','.'))
if betaL32 != 0:
    decays.extend(['bmuL', 'tvmL'])

kappaU = 0.0
kappaUtilde = 0.0
if "YM" in JOlist:
    kappaU = 0.0
    kappaUtilde = 0.0
elif "min" in JOlist:
    kappaU = 1.0
    kappaUtilde = 1.0

# assign correct decays
decayLines = []
for dec in decays:
    if dec[0] in ['c', 't']:
        if dec[1] in ['m', 't']:
            raise RuntimeError("Fermion charges cannot add up to LQ charge.")
        decayStatement = 'decay vlq > '+dec[0]+' '+dec[1]+dec[2]+'~, (t > w+ b, w+ > all all)'
        decayLines.append(decayStatement)
        decayStatement = 'decay vlq~ > '+dec[0]+'~ '+dec[1]+dec[2]+', (t~ > w- b~, w- > all all)'
        decayLines.append(decayStatement)
    elif dec[0] in ['s', 'b']:
        if dec[1] == 'v':
            raise RuntimeError("Fermion charges cannot add up to LQ charge.")
        decayStatement = 'decay vlq > '+dec[0]+' '+dec[1]+dec[2]+'+'
        decayLines.append(decayStatement)
        decayStatement = 'decay vlq~ > '+dec[0]+'~ '+dec[1]+dec[2]+'-'
        decayLines.append(decayStatement)
    else:
        raise RuntimeError("Unexpected quark flavour.")

# ecmEnergy given when running Gen_tf.py
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process='import model vector_LQ_UFO\ngenerate p p > vlq vlq~\noutput -f')

# Fetch default NLO run_card.dat and set parameters
# 260800 corresponds to NNPDF30_nlo_as_0118_mc
settings = {
    'nevents'      :1.1*nevents,
    'pdlabel'      :'lhapdf',
    'lhaid'        :'260800',
    'scale'        :lqmass,
    'dsqrt_q2fact1':lqmass,
    'dsqrt_q2fact2':lqmass
}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Parameters
parameters = {
    'MASS':{
        '9000007' : '{:e}'.format(lqmass)
    },
    'NPLQCOUP':{
        '1'       : '{:e}'.format(gU),
        '2'       : '{:e}'.format(betaL33),
        '3'       : '{:e}'.format(betaRd33),
        '4'       : '{:e}'.format(betaL23),
        '5'       : '{:e}'.format(betaL32),
        '6'       : '{:e}'.format(kappaU),
        '7'       : '{:e}'.format(kappaUtilde)
    },
    'DECAY':{
        '9000007' : 'Auto'
    }
}
modify_param_card(process_dir=process_dir, params=parameters)

# MadSpin
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card, 'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer * 
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#*    Manual:                                               *
#*    cp3.irmp.ucl.ac.be/projects/madgraph/wiki/MadSpin     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                 # cut on how far the particle can be off-shell
# set spinmode onshell          # Use one of the madspin special mode
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event

# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all\n"""%runArgs.randomSeed)

for l in decayLines:
    mscard.write(l+'\n')

mscard.write("""
# running the actual code
launch""")
mscard.close()

# Print cards on screen
print_cards(run_card='run_card.dat', param_card='param_card.dat', madspin_card='madspin_card.dat')

generate(process_dir=process_dir, runArgs=runArgs)

#outputDS = arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)
arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)

# Metadata
evgenConfig.description = ('Pair production of U1 vector leptoquarks, mLQ={0:d}').format(int(lqmass))
evgenConfig.keywords+=['BSM','exotic','leptoquark']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> vlq vlq~'
evgenConfig.contact = ["Kyeong Ro Lee <kyeong.ro.lee@cern.ch>"]
#runArgs.inputGeneratorFile=outputDS

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#evt_multiplier = 8
