import math
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

#<CHANGE THESE SETTINGS. keywords MUST BE IN LIST OF ALLOWED KEYWORDS>

evgenConfig.description = "Z prime ("+str(mZp)+") to ttbar"
evgenConfig.process = "pp>Zprime>ttbar>bbardilep"
evgenConfig.keywords = ["BSM", "Zprime" ,"resonance", "ttbar"]
evgenConfig.generators = ["MadGraph","Pythia8"]
evgenConfig.contact = ["Giancarlo Panizzo <giancarlo.panizzo@cern.ch>"]

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)

# Number of events
# safe factor applied to nevents, to account for the filter efficiency
nevents = runArgs.maxEvents*1.3 if runArgs.maxEvents>0 else 1.3*evgenConfig.nEventsPerJob
nevents = int(nevents)

safefactor = 1.3
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor



# Compute dependent parameters
Gf = '1.207e-05'

process= """
import model sm
import model VPrime_NLO
generate  p p > zp > t t~

# Output processes to MadEvent directory
output -f  
"""
params = {}
params = {  "SMINPUTS":    { 'gf':Gf }, # sell gf for correct MW. Correct left/right Zp couplings impossible to be achieved independently
            "MASS":        { 'mzp':'%.8e' % mZp, 
                             '6':'1.725e+2' },
            "DECAY":       { '32':'%.8e' % wZp, 
                             '6':'1.33e+00', 
                             '24':'2.09780e+00'} # fix decay widths of Zp, t, w
         }

settings = {}

settings = {      'dynamical_scale_choice':'4', # center of mass energy, since in Pythia8 this was considered a 2->1 process
             'nevents': int(nevents),
             'bwcutoff' : 50.0}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)

#---------------------------------------------------------------------------
# creating mad spin card
#---------------------------------------------------------------------------

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')                                                                                               

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
 set BW_cut 50.0                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
# set spinmode full
define fall = u c s d b u~ c~ s~ d~ b~ e- e+ ta- ta+ mu- mu+ ve vm vt ve~ vm~ vt~
define l = mu- mu+ e- e+
define vl = ve vm
define vl~ = ve~ vm~
%s
# running the actual code
launch"""% (runArgs.randomSeed, decay_str) )

mscard.close()


generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)

# hacking LHE file
# basename for madgraph LHEF file
rname = 'run_01'

arrange_output(runArgs=runArgs, process_dir=process_dir, lhe_version=3, saveProcDir=False) 

from MadGraphControl.MadGraphUtils import check_reset_proc_number
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

evgenConfig.inputconfcheck=""


