include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
jobConfig = get_physics_short()

masses['1000021'] = float(jobConfig.split('_')[4])
masses['1000022'] = float(jobConfig.split('_')[5])
if masses['1000022']<0.5: masses['1000022']=0.5

decaytype = str(jobConfig.split('_')[3])

process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > go go $ susysq susysq~ @1
add process p p > go go j $ susysq susysq~ @2
add process p p > go go j j $ susysq susysq~ @3
'''

decays['1000021']="""DECAY 1000021 7.40992706E-02 # Wgo
##  BR         NDA      ID1       ID2       ID3
2.5000000e-01  3        1         -1        1000022
2.5000000e-01  3        2         -2        1000022
2.5000000e-01  3        3         -3        1000022
2.5000000e-01  3        4         -4        1000022"""

decays['1000022']="""DECAY   1000022     0.100000   # neutralino1 decays  
##          BR         NDA      ID1       ID2
1.25000000E-01    3         -11        -2         1     
1.25000000E-01    3          11         2        -1    
1.25000000E-01    3         -13        -2         1     
1.25000000E-01    3          13         2        -1     
1.25000000E-01    3         -12        -1         1     
1.25000000E-01    3          12         1        -1    
1.25000000E-01    3         -14        -1         1     
1.25000000E-01    3          14         1        -1   """

njets = 2
evt_multiplier = 4
#evgenLog.info('Registered generation of gluino grid '+str(runArgs.runNumber))

evgenConfig.contact  = [ "nils.ruthmann@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','gluino']
evgenConfig.description = 'gluino production, glu->qq+N1(->LQD) in simplified model, m_glu = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
        genSeq.Pythia8.Commands += ["Merging:Process = guess"]
        genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
