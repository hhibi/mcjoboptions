##
## 13.0GeV leptophilic Zprime from pp --> 2mu+Zp --> 4mu
##
zpm=13.0
gzpmul=1.000000e-02
include("./MGCtrl_Py8EG_NNPDF30nlo_Leptophilicmutau_2muZp_4mu_4pt2.py")
