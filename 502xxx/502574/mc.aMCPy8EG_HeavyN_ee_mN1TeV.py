import subprocess
retcode = subprocess.Popen(['get_files', '-jo', 'HeavyNCommon.py'])
if retcode.wait() != 0:
    raise IOError('could not locate HeavyNCommon.py')

import HeavyNCommon

HeavyNCommon.process = HeavyNCommon.available_processes['eechannel']
HeavyNCommon.parameters_paramcard['mass']['mN1'] = 1000
HeavyNCommon.parameters_paramcard['numixing']['VeN1'] = 1
HeavyNCommon.parameters_paramcard['numixing']['VeN2'] = 1
HeavyNCommon.parameters_paramcard['numixing']['VeN3'] = 1

HeavyNCommon.run_evgen(runArgs, evgenConfig, opts)
