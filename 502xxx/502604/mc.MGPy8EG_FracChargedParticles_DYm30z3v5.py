mass = 30
charge = 6.0/10.0
evgenConfig.nEventsPerJob = 30000
get_FractionChargesControlFile = subprocess.Popen(['get_files', '-jo', 'FractionalChargeParticles.py'])
if get_FractionChargesControlFile.wait():
        print "Could not get hold of FractionalChargeParticles.py, exiting..."
        sys.exit(2)
include ( "FractionalChargeParticles.py" )



