from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigMatchbox import Hw7ConfigMatchbox

genSeq += Herwig7()

# Disable TestHepMC for the time being
#if hasattr(testSeq, "TestHepMC"):
#    testSeq.remove(TestHepMC())

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.2-DEFAULT"
evgenConfig.description = "Herwig7.2.0 EW VBF Z(mumu)jj NLO QCD Subtractive Matched Dipole shower"
evgenConfig.keywords    = ["SM","Z"]
evgenConfig.contact     = ["Douglas Schaefer (schaed@uchicago.edu)"]
evgenConfig.nEventsPerJob = 10000

## initialize generator configuration object
Herwig7Config = Hw7ConfigMatchbox(genSeq, runArgs, run_name="HerwigMatchbox", beams="pp")

## configure generator
Herwig7Config.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
Herwig7Config.tune_commands()

Herwig7Config.add_commands("""
##################################################
## Process selection
##################################################

## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in 
cd /Herwig/MatrixElements/Matchbox 

## Set the hard process
set /Herwig/MatrixElements/Matchbox/Factory:OrderInAlphaS 0
set /Herwig/MatrixElements/Matchbox/Factory:OrderInAlphaEW 4
do /Herwig/MatrixElements/Matchbox/Factory:Process p p -> mu+ mu- j j

#Bias the phase space to higher HT (and hence PtV)
cd /Herwig/MatrixElements/Matchbox
create Herwig::Merger Merger
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert Factory:Preweighters 0  MPreWeight

set MPreWeight:HTPower 2
set MPreWeight:MaxPTPower 0
set MPreWeight:OnlyColoured Yes


##################################################
## Matrix element library selection
##################################################

read Matchbox/VBFNLO.in
read Matchbox/VBFDiagramsOnly.in

##################################################
## Cut selection
## See the documentation for more options
##################################################

cd /Herwig/Cuts

set /Herwig/Cuts/ChargedLeptonPairMassCut:MinMass 60*GeV
set /Herwig/Cuts/ChargedLeptonPairMassCut:MaxMass 120*GeV

## cuts on additional jets
read Matchbox/DefaultPPJets.in

insert JetCuts:JetRegions 0 FirstJet
insert JetCuts:JetRegions 1 SecondJet
# insert JetCuts:JetRegions 2 ThirdJet
# insert JetCuts:JetRegions 3 FourthJet


##################################################
## Scale choice
## See the documentation for more options
##################################################

cd /Herwig/MatrixElements/Matchbox
set Factory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/HTScale
set /Herwig/MatrixElements/Matchbox/Scales/HTScale:JetPtCut 15.*GeV

##################################################
## Matching and shower selection
## Please also see flavour scheme settings
## towards the end of the input file.
##################################################

read Matchbox/MCatNLO-DipoleShower.in
#read Matchbox/Powheg-DipoleShower.in

##################################################
## PDF choice
##################################################
#PDF choice set in interface above, matches H7.2.0 default

#full 5F for AO, no B Mass for Dipoles
#read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
read Matchbox/FiveFlavourNoBMassScheme.in
""")
Herwig7Config.sampler_commands("MonacoSampler", 20000, 4, 50000, 4, 500)

if runArgs.generatorRunMode == 'build':
  # use the --generatorJobNumber command line parameter to dynamically
  # specify the total number of parallel integration jobs
  Herwig7Config.do_build(integration_jobs=runArgs.generatorJobNumber)

elif runArgs.generatorRunMode == 'run':
  # generate events using the specified gridpack
  Herwig7Config.do_run()
