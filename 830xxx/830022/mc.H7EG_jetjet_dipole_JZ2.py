## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = "Herwig7 dijet LO"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "nishu@cern.ch" ]

## Configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6 )
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(2,filtSeq)

command = """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

## Dipole Shower
set /Herwig/Generators/EventGenerator:EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
read snippets/DipoleShowerFiveFlavours.in

##  - - -   bias to high pt.
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert /Herwig/MatrixElements/SubProcess:Preweights 0  MPreWeight
set MPreWeight:MaxPTPower 4
##

"""

print command

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()
