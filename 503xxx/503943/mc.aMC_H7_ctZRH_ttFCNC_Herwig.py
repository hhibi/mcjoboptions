#### Shower
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 10000
evgenConfig.description = 'aMC@NLO_ctZRH_Decay'
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.keywords+= ['FCNC', 'top', 'Z','lepton']
evgenConfig.contact = ['Ana Peixoto <ana.peixoto@cern.ch>','Petr Jacka <petr.jacka@cern.ch>']
evgenConfig.tune = "H7.1-Default"

include("Herwig7_i/Herwig7_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118", max_flav=4)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
