#### Shower
## run Pythia8 on-the-fly -----------------------------------------------------
## Provide config information
evgenConfig.inputFilesPerJob = 1
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = "MG5aMCatNLO+MadSpin+Pythia8 t-channel leptonic"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["olga.bylund@cern.ch"]
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
