include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

decayflavor = phys_short.split('_')[3]

masses['1000021'] = float(phys_short.split('_')[4]) #go
masses['1000022'] = float(phys_short.split('_')[5].split('.')[0]) #N1
longlived = False
if (len(phys_short.split('_'))>6):
  longlived = True
  neutralinoLifetime = phys_short.split('_')[6].replace("ns","").replace(".py","").replace("p","0.")
  hbar = 6.582119514e-16
  decayWidth = hbar/float(neutralinoLifetime)
  decayWidthStr = '%e' % decayWidth
  decayStringHeader = 'DECAY   1000022  '
  header = decayStringHeader + decayWidthStr 
  evgenLog.info('lifetime of 1000022 is set to %s ns'% neutralinoLifetime)


process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate    p p > go go QED=0 RPV=0     / susysq susysq~ @1
add process p p > go go j QED=0 RPV=0   / susysq susysq~ @2
add process p p > go go j j QED=0 RPV=0 / susysq susysq~ @3
'''

# Set up the decays
if (decayflavor == "rpvHF" and not longlived):
  decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    1000022  6  -6
        0.2500  3    6        3   5
        0.2500  3   -6       -3  -5
  """
  decays['1000022'] = """DECAY   1000022     1.0  # neutralino decays
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    6   3   5
        0.5000  3   -6  -3  -5
  #"""

elif (decayflavor == "rpvLF" and not longlived):  
  decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
  #          BR          NDA       ID1       ID2       ID3
        0.1250  3    1000022  1  -1 
        0.1250  3    1000022  2  -2 
        0.1250  3    1000022  3  -3 
        0.1250  3    1000022  4  -4 
        0.1250  3    2        1   3
        0.1250  3    4        1   3
        0.1250  3   -2       -1  -3
        0.1250  3   -4       -1  -3
  """
  decays['1000022'] = """DECAY   1000022     1.0   # neutralino decays
  #          BR          NDA       ID1       ID2       ID3
        0.2500  3    2  1  3
        0.2500  3    4  1  3
        0.2500  3   -2 -1 -3
        0.2500  3   -4 -1 -3
  #"""         

elif (decayflavor == "rpvHF" and longlived):
  decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
  #          BR          NDA       ID1       ID2       ID3
        1.0000  3    1000022  6  -6
  """
  branchingRatios = """
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    6   3   5
        0.5000  3   -6  -3  -5
  #"""         
  decays['1000022'] = header + branchingRatios

elif (decayflavor == "rpvLF" and longlived):
  decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
  #          BR          NDA       ID1       ID2       ID3
        0.2500  3    1000022  1  -1 
        0.2500  3    1000022  2  -2 
        0.2500  3    1000022  3  -3 
        0.2500  3    1000022  4  -4 
  """
  branchingRatios="""
  #          BR          NDA       ID1       ID2       ID3
        0.2500  3    2  1  3
        0.2500  3    4  1  3
        0.2500  3   -2 -1 -3
        0.2500  3   -4 -1 -3
  #"""         
  decays['1000022'] = header + branchingRatios

# Set up a default event multiplier
evt_multiplier = 2

njets = 2

usePMGSettings = False

evgenConfig.contact  = ["emily.anne.thompson@cern.ch"]

if (decayflavor == "rpvHF" and not longlived):
  evgenConfig.description = 'gluino pair production and decay via RPV lamba323, or decay to top quarks and neutralino, which then decays via RPV lambda323, m_gluino = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

elif (decayflavor == "rpvLF" and not longlived):
  evgenConfig.description = 'gluino pair production and decay via RPV lamba112, or decay to LF quarks and neutralino, which then decays via RPV lambda112, m_gluino = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

elif (decayflavor == "rpvHF" and longlived):
  evgenConfig.description = 'gluino pair production and decay to top quarks and long-lived neutralino, which then decays via RPV lambda323, m_gluino = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

elif (decayflavor == "rpvLF" and longlived):
  evgenConfig.description = 'gluino pair production and decay to LF quarks and long-lived neutralino, which then decays via RPV lambda112, m_gluino = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])


if not longlived:
  evgenConfig.keywords += [ 'SUSY', 'RPV', 'gluino', 'simplifiedModel', 'neutralino']

if longlived:
  evgenConfig.keywords += [ 'SUSY', 'RPV', 'gluino', 'simplifiedModel', 'neutralino', 'longLived']
  testSeq.TestHepMC.MaxVtxDisp = 1e8 # in mm
  testSeq.TestHepMC.MaxTransVtxDisp = 1e8

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]

