evgenConfig.description = "SuperChic4 MC gamma + gamma pp collisions at 13000 GeV to 2 gamma ALP mediated"
evgenConfig.keywords = ["2photon","2photon"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["gen.tateno@cern.ch"]

if not os.path.exists('inputs'):
    os.makedirs('inputs')
if not os.path.exists('evrecs'):
    os.makedirs('evrecs')
if not os.path.exists('outputs'):
    os.makedirs('outputs')

evgenConfig.generators += ["Superchic"]

from Superchic_i.SuperChicUtils import *

#class with the superchic initialization parameters.  Please see SuperChicUtils for a complete list of tunable parameters.
Init = SuperChicConfig()

Init.rts = "13d3"
Init.isurv = "4"                    # Model of soft survival (from 1 -> 4, corresponding to arXiv:1306.2149)
Init.intag = "'in13'"               # for input files
Init.PDFname = "'MMHT2015qed_nnlo'" # PDF set name
Init.PDFmember = "0"                # PDF member
Init.proc = "68"                    # Process number (59 = gg->gg, 56 = gg->ee, 68 = gg->a->gg ); Please consult Superchic Manual https://superchic.hepforge.org/
Init.beam = "'prot'"                # Beam type ('prot', 'ion')
Init.outtg = "'out'"                # for output file name
Init.sfaci = ".true."               # Include soft survival effects
Init.diff = "'el'"                  # interaction: elastic ('el'), single ('sd','sda','sdb') and double ('dd') dissociation.
Init.ncall = "10000"                # Number of calls for preconditioning
Init.itmx = "10"                    # Number of iterations for preconditioning
Init.prec = "0.5d0"                 # precision
Init.inccall = "10000"              # Number of increase calls per iteration
Init.itend = "1000"                 # Maximum number of iterations
hasSeed = hasattr(runArgs,"randomSeed")
if hasSeed:
    Init.iseed  = str(int(runArgs.randomSeed)) # Random number seed (integer > 0)
Init.genunw  = ".true."
hasNev = hasattr(runArgs,"maxEvents")
if hasNev:
    Init.nev = str(runArgs.maxEvents) # Number of events
Init.erec  = "'lhe'"                # Event record format ('hepmc','lhe','hepevt')
Init.readwt = ".false."             # Set to true to read in pre-calculated maxium weight below
Init.wtmax = "0d0"                  # Maximum weight
Init.gencuts  = ".true."            # Generate cuts below
Init.fwidth = ".true."              # Include finite width
Init.ptxmax  = "100d0"              # max pT of the system
Init.ptamin  = "20d0"               # Minimum pT of outgoing object a
Init.ptbmin  = "20d0"               # Minimum pT of outgoing object b
Init.etaamin  = "-2.4d0"            # Minimum eta of outgoing object a
Init.etaamax   = "2.4d0"            # Maximum eta of outgoing object a
Init.etabmin  = "-2.4d0"            # Minimum eta of outgoing object b
Init.etabmax   = "2.4d0"            # Maximum eta of outgoing object b
Init.acoabmax  = "100d0"
Init.malp = "600d0"                      # ALP mass (GeV)
Init.gax = "200d-6"                    # ALP coupling (GeV^-1)
Init.alpt = "'ps'"                       # ALP type (scalar - 'sc', pseudoscalar - 'ps')

SuperChicRun(Init)

import Superchic_i.EventFiller as EF
ef = EF.LheEVNTFiller()
fileName = "evrecs/evrec"+Init.outtg[1:-1]+".dat"
ef.fileName = fileName
genSeq += ef
