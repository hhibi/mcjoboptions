evgenConfig.description = "Superchic 3.05  ALP  process in UPC collisions at 5020 GeV, 3 < M_gg < 150.0 GeV, pT(gamma) > 0 GeV, |eta(gamma)| < 5.0"
evgenConfig.keywords = ["2photon","2photon"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["prabhakar.palni@cern.ch"]
evgenConfig.nEventsPerJob   = 1000

if not os.path.exists('inputs'):
    os.makedirs('inputs')
if not os.path.exists('evrecs'):
    os.makedirs('evrecs')

from Superchic_i.Superchic_iConf import Superchic_i
genSeq += Superchic_i("Superchic")
genSeq.Superchic.McEventKey = "GEN_EVENT"
evgenConfig.generators += ["Superchic"]

from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
_evgenstream = AthenaPoolOutputStream("StreamEVGEN")
_evgenstream.ItemList = ["2101#*","133273#GEN_EVENT"]

del _evgenstream


# TODO: Sort out proper param setting based on runArgs.ecmEnergy
if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error(" Set beam energy in JO initialization with parameter rts ")
    sys.exit(1)


genSeq.Superchic.Initialize = \
    ["rts 5.02d3",                     # set the COM collision energy (in fortran syntax)
    "isurv 4",                        # Model of soft survival
    "intag 'in5'",                    # for input files
    "PDFname 'MMHT2014lo68cl'",       # PDF set name
    "PDFmember 0",                    # PDF member
    "proc 68",                        # Process number (59 = gg->gg, 56 =gg->ee )
    "beam 'ion'",                     # Beam type ('prot', 'ion')
    "outtg 'out'",                    # for output file name
    "sfaci .true.",                  # Include soft survival effects
    "ncall 10000",                   # Number of calls for preconditioning
    "itmx 10",                        # Number of iterations for preconditioning
    "prec 1d0",                    # precision
    "ymin -5d0",                      # Minimum object rapidity Y_X
    "ymax 5d0",                       # Maximum object rapidity Y_X
    "mmin 3.0d0",                      # Minimum object mass M_X
    "mmax 150d0",                     # Maximum object mass M_X
    "gencuts .true.",                 # Generate cuts below
    "ptamin 0d0",                   # Minimum pT of outgoing object a (gamma)
    "ptbmin 0d0",                   # Minimum pT of outgoing object b (gamma)
    "etaamin -5d0",                 # Minimum eta of outgoing object a
    "etaamax  5d0",                 # Maximum eta of outgoing object a
    "etabmin -5d0",                 # Minimum eta of outgoing object b
    "etabmax  5d0",                 # Maximum eta of outgoing object b
    "malp 120d0",                   # ALP mass (GeV) 
    "gax 10d-4",                    # ALP coupling (GeV^-1)
    "alpt 'ps'"                      # AlP type (scalar - 'sc', pseudoscalar - 'ps')
    ]
