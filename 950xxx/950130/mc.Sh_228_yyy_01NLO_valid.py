include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa yyy + 0,1j@NLO"
evgenConfig.keywords = ["SM", "3photon" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 50

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  ALPHAQED_DEFAULT_SCALE=0.0

  % tags for process setup
  NJET:=1; LJET:=3,4; QCUT:=10;
  METS_BBAR_MODE=5

  % me generator settings
  ME_SIGNAL_GENERATOR Amegic Comix LOOPGEN Internal;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 22 22 22 93{NJET};
  Order (*,3);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]+p[4]));
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  PSI_ItMin 20000 {3}
  Integration_Error 0.02 {3}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.05 {5,6}
  End process;
}(processes)

(selector){
  "PT" 22  20,E_CMS:18,E_CMS:16,E_CMS  [PT_UP]
  "m"  22,22,22  5.0,E_CMS
  RapidityNLO  22  -2.7  2.7
  IsolationCut  22  0.1  2  0.10;
  # need some lower bound on muR=m_yyy:
  DeltaRNLO 22 22 0.2 1000.0; # dR on any diphoton pair?
}(selector)
"""

genSeq.Sherpa_i.NCores = 144
