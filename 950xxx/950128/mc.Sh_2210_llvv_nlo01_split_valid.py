include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "2 charged leptons + 2 neutrinos with 0,1j@NLO + 2,3j@LO and mll>4GeV, pTl1>5 GeV, pTl2>5 GeV."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 200

genSeq.Sherpa_i.RunCard="""
(run){
  # scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=20.;

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  PARTICLE_CONTAINER 900 lminus 11 13 15
  PARTICLE_CONTAINER 901 lplus -11 -13 -15

  # EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5


  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1

  # massive b-quarks such that b-quarks are not included in the 93 container
  # and hence top-quark processes are not considered
  MASSIVE[5]=1;
  OL_PARAMETERS=verbose=1 nq_nondecoupled=5 mass(5)=0.0 ew_renorm_scheme=1 ew_scheme=2
  MCATNLO_MASSIVE_SPLITTINGS=0

  # NWF improvements
  NLO_CSS_PSMODE=1
}(run)

(processes){
  Process 93 93 -> 900 91 901 91 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 4.0 E_CMS
  Mass 13 -13 4.0 E_CMS
  Mass 15 -15 4.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=verbose=1 nq_nondecoupled=5 mass(5)=0.0 ew_renorm_scheme=1 ew_scheme=2" ]
genSeq.Sherpa_i.Parameters += ["OL_PREFIX=Process/OpenLoops"]
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppllll" ,"ppllll_ew", "pp2l2nj", "pp2l2nj_ew"]
genSeq.Sherpa_i.NCores = 16

