# adapted for rel 21.6 from MC15.429009

# Z'->tt JO (as in MC12 + EvtGen)
# zprime mass in GeV
m_zprime=1000.0

evgenConfig.description = "SSM Z prime ("+str(m_zprime)+") to ttbar"
evgenConfig.keywords = ["BSM","Zprime","resonance","ttbar"]
evgenConfig.contact = ['giancarlo.panizzo@cern.ch']

include("Pythia8_i/Pythia8_AU2_MSTW2008LO_EvtGen_Common.py")

# turn on the Z' process
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]
# set mass and disable all decay modes except Z' -> ttbar
genSeq.Pythia8.Commands += ["32:m0 ="+str(m_zprime) ]
genSeq.Pythia8.Commands += ["32:onMode = off"]
genSeq.Pythia8.Commands += ["32:onIfAny = 6 -6"]

# only Z'
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3"]


