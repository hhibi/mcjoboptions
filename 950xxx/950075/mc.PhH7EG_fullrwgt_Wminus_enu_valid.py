# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG CC DY production"
evgenConfig.keywords = ["SM", "W"]
evgenConfig.contact = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune     = "H7.1-Default"

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg W_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_W_EW_Common.py")

PowhegConfig.decay_mode = "w- > e- ve~"
PowhegConfig.no_ew=1
PowhegConfig.fullrwgt=1
PowhegConfig.ptsqmin=0.8
PowhegConfig.PHOTOS_enabled = False
PowhegConfig.nEvents *= 1.2

#Gmu EW scheme inputs matching what used in the Powheg V1 samples
PowhegConfig.scheme=0

# PDF sets
# CT18nnlo, CT10, CT18nlo, CT18nnlo, CT18Znnlo, CT18Annlo, CT18Xnnlo, CT14nnlo, 
# MMHT2014nnlo68, NNPDF31_nnlo_as_0118,NNPDF31_nnlo_as_0118_luxqed 
PowhegConfig.PDF = [14000, 10800, 14400, 14100, 14200, 14300, 13000, 25300, 303600, 325100] 
      
PowhegConfig.PDF.extend(range(304401, 304501))      # NNPDF3.1nnlo Hessian error set
PowhegConfig.PDF.extend(range(13001, 13056))        # CT14nnlo error set
PowhegConfig.PDF.extend(range(14001, 14058))        # CT18nnlo error set
PowhegConfig.PDF.extend(range(14201, 14258))        # CT18Annlo error set
PowhegConfig.PDF.extend(range(25301, 25350))        # MMHT2014nnlo68 error set
                                              
PowhegConfig.mu_F = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0]
PowhegConfig.mu_R = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5]                              

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings              
# --------------------------------------------------------------    
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()



