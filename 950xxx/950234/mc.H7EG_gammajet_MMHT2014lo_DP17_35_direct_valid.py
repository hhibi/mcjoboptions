## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.description = "Photon+jet production with H71UE default tune"
evgenConfig.keywords = ["QCD", "photon", "jets"]
evgenConfig.contact  = [ "olszewsk@cern.ch", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

## hard process setup
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")

command = """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEGammaJet
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/PhotonKtCut:MinKT 8.5*GeV
set /Herwig/Cuts/JetKtCut:MinKT 8.5*GeV
"""

print command

Herwig7Config.add_commands(command)

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 17000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 35000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True

## run the generator
Herwig7Config.run()
