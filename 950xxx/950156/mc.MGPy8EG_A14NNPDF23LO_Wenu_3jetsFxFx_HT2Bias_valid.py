import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Wenu+3Np FxFx HT^2 bias with floor = 2000 GeV^2'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch"]
evgenConfig.keywords+=['Z','jets','FxFx']

# General settings
evgenConfig.nEventsPerJob = 5000
nevents = runArgs.maxEvents*5.0 if runArgs.maxEvents>0 else 5.0*evgenConfig.nEventsPerJob

#Madgraph run card and shower settings
# Shower/merging settings
maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=3
qCut=20.

# MG Particle cuts                                                                                                                                
mllcut=40

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = p b b~
    define j = p
    define e = e+ e-
    define nu = ve ve~
    generate p p > e nu [QCD] @0
    add process p p > e nu j [QCD] @1
    add process p p > e nu j j [QCD] @2
    add process p p > e nu j j j [QCD] @3
    output -f"""

    process_dir = str(new_process(process))
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':325100, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets - NNDP31 LUXqed
    'pdf_variations':[325100], # list of pdfs ids for which all variations (error sets) will be included as weights - NNDP31 LUXqed
    'alternative_pdfs':[304400,13000,25300,14000,14100,14200,61200,42560,303200], #  NNPDF31 hessian, CT14, MMHT14, CT18, CT18Z, CT18A, HERAPDF2.0, ABMP16 5FS, NNPDF30 hessian
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

#Fetch default run_card.dat and set parameters
settings = {
            'event_norm'    : 'bias',    
            'maxjetflavor'  : int(maxjetflavor),
            'parton_shower' : parton_shower,
            'nevents'       : int(nevents),
            'ickkw'         : 3,
            'jetradius'     : 1.0,
            'ptj'           : 8,
            'etaj'          : 10,
            'mll_sf'        : mllcut,
            'mll'           : mllcut,
        }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz'

paramToCopy      = 'param_card.Torrielli.dat'
paramDestination = process_dir+'/Cards/param_card.dat'
paramfile        = subprocess.Popen(['get_files','-data',paramToCopy])
paramfile.wait()

if not os.access(paramToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(paramToCopy))
shutil.copy(paramToCopy,paramDestination)

scaleToCopy      = 'cuts_W_HT2bias.f'                                                                                                    
scaleDestination = process_dir+'/SubProcesses/cuts.f'                                                                               
scalefile        = subprocess.Popen(['get_files','-data',scaleToCopy])                                                                   
scalefile.wait()                                                                                                                          

if not os.access(scaleToCopy,os.R_OK):                                                                                                  
  raise RuntimeError("ERROR: Could not get %s"%(scaleToCopy))                                                                               
shutil.copy(scaleToCopy,scaleDestination)                                                                                                         

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#simil-NLO parameters
genSeq.Pythia8.Commands += ["JetMatching:merge          = on", 
                            "SpaceShower:alphaSuseCMW    = on",              
                            "SpaceShower:alphaSorder     = 2",               
                            "TimeShower:alphaSuseCMW     = on",              
                            "TimeShower:alphaSorder      = 2",               
                            "SpaceShower:alphaSvalue    = 0.118",
                            "TimeShower:alphaSvalue     = 0.118" 
                             ] 

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut
print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

genSeq.Pythia8.Commands += ["JetMatching:merge          = on", 
                            "JetMatching:scheme         = 1",  
                            "JetMatching:setMad         = off",
                            "SpaceShower:rapidityOrder  = off",
                            "SpaceShower:pTmaxFudge     = 1.0",   
                            "JetMatching:qCut            = %f"%PYTHIA8_qCut,
                            "JetMatching:coneRadius      = 1.0",
                            "JetMatching:etaJetMax       = 10.0",
                            "JetMatching:doFxFx          = on",
                            "JetMatching:qCutME          = 8.0",
                            "JetMatching:nJetMax         = %i"%PYTHIA8_nJetMax,
                            'JetMatching:jetAlgorithm = 2', #explicit setting of kt-merging for FxFx (also imposed by Py8-FxFx inteface)
                            'JetMatching:slowJetPower = 1', #explicit setting of kt-merging for FxFx (also imposed by Py8-FxFx inteface)
                            'JetMatching:nQmatch = 5', #4 corresponds to 4-flavour scheme (no matching of b-quarks), 5 for 5-flavour scheme
                            "JetMatching:eTjetMin = %f"%PYTHIA8_qCut #This is 20 in the Pythia default, it should be <= qCut 
                             ]

genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph'] 
genSeq.Pythia8.FxFxXS = True
