
include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa ZZ"
evgenConfig.keywords = ["SM", "ZZ", "2lepton","bbbar", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  % NLO EWK
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=3

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0

  % decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0

  HDH_STATUS[23,6,-6]=0
  HDH_STATUS[23,4,-4]=0
  HDH_STATUS[23,1,-1]=0
  HDH_STATUS[23,2,-2]=0
  HDH_STATUS[23,3,-3]=0
  HDH_STATUS[23,12,-12]=0
  HDH_STATUS[23,14,-14]=0
  HDH_STATUS[23,16,-16]=0

  % Negative weight reduction
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 23 23 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET}
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {2,3,4,5};
  End process;
}(processes)

"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 16

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
decaysfilter = DecaysFinalStateFilter()
filtSeq += decaysfilter
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [ 23 ]
filtSeq.DecaysFinalStateFilter.NbQuarks = 2
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 2
