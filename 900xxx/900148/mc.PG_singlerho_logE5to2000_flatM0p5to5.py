evgenConfig.description = "Single charged rho with log energy and flat mass distributions"
evgenConfig.keywords = ["singleParticle", "rho"]
evgenConfig.contact = ["angerami@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (-213,213)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(5000, 2000000.), eta=[-3.0, 3.0], mass=[500,5000])

include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles+=['inclusive.pdt']
genSeq.EvtInclusiveDecay.allowAllKnownDecays=True
