evgenConfig.description = "Bc(2S)- -> Bc- pi- pi+, Bc- -> J/psi mu- X inclusive sample with BCVEGPY"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2.py")

genSeq.EvtInclusiveDecay.whiteList+=[100541, 100543, -100541, -100543]
genSeq.EvtInclusiveDecay.userDecayFile = "Bc21S0m_Bc11S0m_JpsiMu_inclusive.dec"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG20.pdt','Bc21S0m_Bc11S0m_JpsiMu_inclusive.dec']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter     
filtSeq += MultiMuonFilter("TwoMuonFilter")
filtSeq += MultiMuonFilter("ThreeMuonFilter")

TwoMuonFilter = filtSeq.TwoMuonFilter
TwoMuonFilter.Ptcut = 3500.
TwoMuonFilter.Etacut = 2.7
TwoMuonFilter.NMuons = 2

ThreeMuonFilter = filtSeq.ThreeMuonFilter
ThreeMuonFilter.Ptcut = 2700.
ThreeMuonFilter.Etacut = 2.7
ThreeMuonFilter.NMuons = 3





