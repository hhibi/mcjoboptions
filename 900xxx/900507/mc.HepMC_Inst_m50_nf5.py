evgenConfig.description = "QCD Instanton production with a minimum mass of 50 GeV and 5 active flavors"
evgenConfig.keywords = ["QCD","SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch"]

#include("TruthIO/HepMCReadFromFile_Common.py")
from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]
    
testSeq.TestHepMC.VtxDisplacedTest=False


