evgenConfig.nEventsPerJob = 10000
evgenConfig.generators = ["ParticleGun"]
evgenConfig.description = "single muon events"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["lderamo@cern.ch"]

import ParticleGun as PG

class MyParticleSampler(PG.ParticleSampler):
    """
    A special sampler to generate single particles flat in 1/pT and in
    impact parameter to the beam, with flat z0 between -150 and +150 mm, flat also in d0 between -100 mm and +100 mm.
    """

    def __init__(self, nMuonsPerEvent=1):
        psamp = PG.PtEtaMPhiSampler(pt=PG.InvSampler(5000, 800000), eta=[0.1,0.3], phi=[0.3,0.5])
        xsamp = PG.PosSampler(0, 0, [-150,150], 0)
        PG.ParticleSampler.__init__(self, pid={13,-13}, mom=psamp, pos=xsamp)
        self.ip = PG.mksampler([-100,100])
        self.nMuonsPerEvent = nMuonsPerEvent

    def shoot(self):
        "Return a vector of sampled particles"
        output = []
        for i in range(self.nMuonsPerEvent):
            ps = PG.ParticleSampler.shoot(self)
            assert len(ps) == 1
            p = ps[0]
            from math import sqrt
            m = -p.mom.X() / p.mom.Y() #< gradient of azimuthal IP sampling line, perp to mom
            x = self.ip() / sqrt(1 + m**2) #< just decomposing sampled IP into x component...
            y = m*x #< ... and y-component
            p.pos.SetX(x)
            p.pos.SetY(m*x)
            output.append(p)
        return output

genSeq += PG.ParticleGun()
evgenConfig.generators += ["ParticleGun"]
genSeq.ParticleGun.sampler = MyParticleSampler()

#topSeq += PG.ParticleGun()
#topSeq.ParticleGun.sampler = MyParticleSampler()
