evgenConfig.description = "Bc*+->Bc+ gamma, Bc+->J/psi Pi+ sample with BCVEGPY"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2.py")

genSeq.EvtInclusiveDecay.userDecayFile = "Bc13S1p_JpsiPi.dec"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG20.pdt','Bc13S1p_JpsiPi.dec']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"

include("GeneratorFilters/ParentChildwStatusFilter.py")
filtSeq.ParentChildwStatusFilter.PDGParent  = [541]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
filtSeq.ParentChildwStatusFilter.PtMinParent =  14900.
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 2.7
filtSeq.ParentChildwStatusFilter.PDGChild = [443]
filtSeq.ParentChildwStatusFilter.PtMinChild = 0.
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 999999.
