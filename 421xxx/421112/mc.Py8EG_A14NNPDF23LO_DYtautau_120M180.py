# based on 429697
evgenConfig.description = "Pythia 8 DY->tautau production with NNPDF23LO tune and Tauolapp used"
evgenConfig.contact = ["Will Davey <will.davey@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2tau"]
evgenConfig.generators += ["Pythia8"]
evgenConfig.generators += ["EvtGen"]

if runArgs.trfSubstepName == 'generate' :
  include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
  genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "23:onMode = off",                  # turn off all decays modes
                            "23:onIfAny = 15",                  # turn on the tautau decay mode
                            "PhaseSpace:mHatMin = 120.",       # lower invariant mass
                            "PhaseSpace:mHatMax = 180."]       # upper invariant mass
  include('Pythia8_i/Pythia8_TauolaPP.py')
  include('Pythia8_i/Pythia8_Photospp.py')

if runArgs.trfSubstepName == 'afterburn':
  evgenConfig.generators += ["Pythia8"]
  include("EvtGen_i/EvtGen_Fragment.py")
  genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
  genSeq.EvtInclusiveDecay.readExisting = True
  evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
  genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
  genSeq.EvtInclusiveDecay.whiteList+=[-5334, 5334]


