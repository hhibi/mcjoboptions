##############################################################
# B0d -> J/psi (mu3.5mu3.5) K0*(K+ pi-) and Bplus -> J/psi Kplus
##############################################################

evgenConfig.description = "Exclusive B0d -> J/psi (mu3.5mu3.5) K0*(K+ pi-) and Bplus -> J/psi Kplus with a tau cut"
evgenConfig.keywords    = ["exclusive", "B0", "2muon", "Jpsi", "Bplus"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.contact = ["adam.edward.barton@cern.ch"]

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py")
include("Pythia8B_i/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 9.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True


# define pseudo-J/psi for exclusive decay
genSeq.Pythia8B.Commands += ['999443:all = myJ/psi antimyJ/psi 3 0 0 3.09692 0.00009 3.09602 3.09782 0']  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
genSeq.Pythia8B.Commands += ['999313:all = myK*0 antimyK*0 3 0 0 0.89594 0.04870 0.65000 1.20000 0']  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0

# force its decay to mu+ mu-
genSeq.Pythia8B.Commands += ['999443:onMode = off'] 
genSeq.Pythia8B.Commands += ['999443:oneChannel = on 1. 0 -13 13'] # onMode bRatio meMode products

genSeq.Pythia8B.Commands += ['999313:onMode = off'] 
genSeq.Pythia8B.Commands += ['999313:oneChannel = on 1. 0 321 -211'] # onMode bRatio meMode products

genSeq.Pythia8B.Commands += ['521:m0 = 5.27929'] # PDG2014 mass
genSeq.Pythia8B.Commands += ['521:addChannel = 2 1. 0 999443 321'] # onMode bRatio meMode products
genSeq.Pythia8B.Commands += ['521:onPosIfMatch = 999443 321']

genSeq.Pythia8B.Commands += ['511:addChannel = 2 1. 0 999443 999313'] # onMode bRatio meMode products
genSeq.Pythia8B.Commands += ['511:onPosIfMatch = 999443 999313']

genSeq.Pythia8B.Commands += ['511:tau0 = 2.2935']
genSeq.Pythia8B.Commands += ['521:tau0 = 2.4555']

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True


genSeq.Pythia8B.NHadronizationLoops = 10

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

#genSeq.Pythia8B.UserSelection = "BD_BPLUS_TAUCUT"
#genSeq.Pythia8B.UserSelectionVariables = [ 8.0, 999999.]

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
BSignalFilter511 = BSignalFilter("BSignalFilter511")
filtSeq += BSignalFilter511
BSignalFilter521 = BSignalFilter("BSignalFilter521")
filtSeq += BSignalFilter521

# Filtering B0 hadron daughters
filtSeq.BSignalFilter511.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter511.Cuts_Final_hadrons_pT = 900. 
filtSeq.BSignalFilter511.Cuts_Final_hadrons_eta = 2.6
filtSeq.BSignalFilter511.B_PDGCode = 511
filtSeq.BSignalFilter511.LVL1MuonCutOn = True
filtSeq.BSignalFilter511.LVL2MuonCutOn = True
filtSeq.BSignalFilter511.LVL1MuonCutPT = 3500 
filtSeq.BSignalFilter511.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter511.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter511.LVL2MuonCutEta = 2.6

# Filtering Bplus hadron daughters
filtSeq.BSignalFilter521.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter521.Cuts_Final_hadrons_pT = 900.
filtSeq.BSignalFilter521.Cuts_Final_hadrons_eta = 2.6
filtSeq.BSignalFilter521.B_PDGCode = 521
filtSeq.BSignalFilter521.LVL1MuonCutOn = True
filtSeq.BSignalFilter521.LVL2MuonCutOn = True
filtSeq.BSignalFilter521.LVL1MuonCutPT = 3500 
filtSeq.BSignalFilter521.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter521.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter521.LVL2MuonCutEta = 2.6

filtSeq.Expression = "BSignalFilter511 or BSignalFilter521" 


