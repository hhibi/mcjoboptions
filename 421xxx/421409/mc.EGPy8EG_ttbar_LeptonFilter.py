# based on 950085

from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*2.5 if runArgs.maxEvents>0 else 2.5*evgenConfig.nEventsPerJob

### DSID lists (extensions can include systematics samples)
process_def = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~
output -f"""

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'pdlabel':"'nn23lo1'",
             'use_syst':"False",
             'nevents':int(nevents)}

process_dir = new_process(process_def)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True for testing only
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords+=['ttbar','jets']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 13.*GeV

evgenConfig.nEventsPerJob = 1000


