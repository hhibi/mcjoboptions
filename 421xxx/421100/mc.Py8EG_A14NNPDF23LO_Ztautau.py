#based on 429110 - only here we use EvtGen in afterburn mode to avoid crashes seen otherwise 
#JO for test of Pythia internal PDF info

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Pythia 8 Z->tautau production to test PDF info'
evgenConfig.keywords    = [ 'SM', 'Z', 'tau', 'lepton']
evgenConfig.contact     = [ 'jmonk@cern.ch' ]

#include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
if runArgs.trfSubstepName == 'generate' :
  include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
  include('Pythia8_i/Pythia8_Photospp.py')
  evgenConfig.generators += ["EvtGen"]
  genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays]
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Pythia8"]
   include("EvtGen_i/EvtGen_Fragment.py")
   genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
   genSeq.EvtInclusiveDecay.readExisting = True
   evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
   genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
   genSeq.EvtInclusiveDecay.whiteList+=[-5334, 5334]

