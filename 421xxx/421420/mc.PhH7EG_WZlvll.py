#--------------------------------------------------------------
# Powheg WZ setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361601

include('PowhegControl/PowhegControl_WZ_Common.py')
PowhegConfig.decay_mode = 'w z > l v l+ l-'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = [10800,13000, 303600, 25100] #CT10nnlo, CT14nnlo, NNPDF3.1nnlo, MMHT2014nlo68cl
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 1.2
PowhegConfig.generate()

#-----------------------------------------------------------
# Herwig7 showering 
#--------------------------------------------------------------
# --------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7  
Herwig7Config.me_pdf_commands(order="NLO", name="CT10nlo") # not possible to use CT18
## Please note from: https://herwig.hepforge.org/tutorials/faq/pdf.html#set-pdf-of-the-lhereader
# For POWHEG matching, there is basically no cross talk between the hard subprocess PDFs and the shower PDFs,
# so choosing a LO shower PDF different from whatever (NLO) PDF has been used with Powheg should not be a problem.

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()


