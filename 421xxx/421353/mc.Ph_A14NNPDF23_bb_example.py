# taken from PowhegControl/share/example/processes/bb
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 bb production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "2jet", "bottom"]
evgenConfig.generators = ["Powheg"]
evgenConfig.contact = ["james.robinson@cern.ch"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg bb process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bb_Common.py")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
#include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("Pythia8_i/Pythia8_Powheg.py")
