from MadGraphControl.MadGraphUtils import *

#<CHANGE THESE SETTINGS. keywords MUST BE IN LIST OF ALLOWED KEYWORDS>
evgenConfig.description="My Description"
evgenConfig.keywords+=['exotic','BSMHiggs','darkPhoton','longLived']
evgenConfig.contact = ['victoria.sanchez.sebastian@cern.ch']
evgenConfig.process = " --> LLPs"

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)

# Number of events
# safe factor applied to nevents, to account for the filter efficiency
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = int(nevents)

safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor

process= """
import model HAHM_variableMW_v3_UFO
define l+ = e+ mu+
define l- = e- mu-
define f+ = d s b u~ c~ t~
define f- = d~ s~ b~ u c t
generate g g > h HIG=1 HIW=0 QED=0 QCD=0, (h > z zp, z > l+ l-, zp > f+ f-)

# Output processes to MadEvent directory
output -f  
"""
params = {}
params = { "HIDDEN": { 'epsilon': '1e-6', #kinetic mixing parameter
                                 'kap': '1e-10', #higgs mixing parameter
                                 'mzdinput': mZd, #Zd mass
                                 'mhsinput':'1000.0' }, #dark higgs mass
                      "HIGGS": { 'mhinput':mH}, #higgs mass
                     "DECAY": { 'wzp':'Auto', 'wh':hwidth, 'wt':'Auto' } #auto-calculate decay widths and BR of Zp, t
                  }

settings = {}
settings = { 'lhe_version':'2.0',
                   'cut_decays':'F',
                   'ptj':'0',
                   'ptb':'0',
                   'pta':'0',
                   'ptl':'0',
                   'etaj':'-1',
                   'etab':'-1',
                   'etaa':'-1',
                   'etal':'-1',
                   'drjj':'0',
                   'drbb':'0',
                   'drll':'0',
                   'draa':'0',
                   'drbj':'0',
                   'draj':'0',
                   'drjl':'0',
                   'drab':'0',
                   'drbl':'0',
                   'dral':'0',
             'nevents': nevents,
             'small_width_treatment'  :'1e-12',
             'pdlabel':'lhapdf',
             'lhaid':'247000'}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)

# hacking LHE file
# basename for madgraph LHEF file
rname = 'run_01'

unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')

# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)

init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:
        if 'vent' in line:
            newlhe.write(line)
            continue

        newline=line.rstrip('\n')
        columns=newline.split()
        pdgid=columns[0]

        if pdgid == '32' and avgtau>0:
            part1 = line[:-22]
            part2 = "%.5e" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        else:
            newlhe.write(line)
oldlhe.close()
newlhe.close()

# re-zipping hacked LHE
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(runArgs=runArgs, process_dir=process_dir, saveProcDir=False) 

from MadGraphControl.MadGraphUtils import check_reset_proc_number
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

# Turn off checks for displaced vertices. 
# Other checks are fine.  
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm 
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

evgenConfig.inputconfcheck=""
