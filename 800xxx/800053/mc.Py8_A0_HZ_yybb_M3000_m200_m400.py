# https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID450xxx/MC15.450279.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_M3000_m80_m80.py
# W' -> WZ -> qqqq
# Wprime Mass (in GeV)

M_Wprime = 3000.
M_w = 200.
M_z = 400.

evgenConfig.contact = ["bnachman@cern.ch"]
evgenConfig.description = "A0->HZ->yybb mA0 = "+str(M_Wprime)+" GeV with NNPDF23LO PDF and A14 tune and m[H] = "+str(M_w)+", , m[Z] = "+str(M_z)
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>A0>HZ>yybb"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ["HiggsBSM:gg2A3 = on"]
genSeq.Pythia8.Commands += ["Higgs:useBSM = on"]
genSeq.Pythia8.Commands += ["36:m0 ="+str(M_Wprime)]
genSeq.Pythia8.Commands += ["36:mWidth = 0.1"]
genSeq.Pythia8.Commands += ["36:doForceWidth = on"]
genSeq.Pythia8.Commands += ["36:onMode = off"]
genSeq.Pythia8.Commands += ["36:onIfMatch = 25 23"]

genSeq.Pythia8.Commands += ["25:m0="+str(M_w)]
genSeq.Pythia8.Commands += ["23:m0="+str(M_z)]

genSeq.Pythia8.Commands += ["23:doForceWidth=on"]
genSeq.Pythia8.Commands += ["23:mWidth=0.1"] 
genSeq.Pythia8.Commands += ["23:doForceWidth=on"]
genSeq.Pythia8.Commands += ["23:mWidth=0.1"]
genSeq.Pythia8.Commands += ["25:doForceWidth=on"]
genSeq.Pythia8.Commands += ["25:mWidth=0.1"]
genSeq.Pythia8.Commands += ["23:onMode = off"]
genSeq.Pythia8.Commands += ["23:onIfAny = 5"]
genSeq.Pythia8.Commands += ["25:onMode = off"]
genSeq.Pythia8.Commands += ["25:onIfAny = 22"]
genSeq.Pythia8.Commands += ["WeakZ0:gmZmode = 2"]

