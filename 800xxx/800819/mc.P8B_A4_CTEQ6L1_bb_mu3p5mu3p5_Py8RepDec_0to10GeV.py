##############################################################
# Job options fragment for bbbar->mu3p5mu3p5X (inv. mass range 0-10 GeV)
# Wide mass-range equivalent of MC15.300306.Pythia8B_A14_CTEQ6L1_bb_mu3p5mu3p5_Py8RepDec.py
##############################################################

evgenConfig.description   = "Inclusive bb->mu3p5mu3p5X production with Photos (dimuon inv.mass range 0-10 GeV)"
evgenConfig.process       = "bb -> mumuX"
evgenConfig.keywords      = [ "bbbar", "bottom", "2muon", "inclusive" ]
evgenConfig.contact       = [ "pavel.reznicek@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("MultiMuonFilter")
include('GeneratorFilters/DiLeptonMassFilter.py')

genSeq.Pythia8B.Commands += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 9.' ]
genSeq.Pythia8B.Commands += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.QuarkPtCut                = 5.0
genSeq.Pythia8B.AntiQuarkPtCut            = 5.0
genSeq.Pythia8B.QuarkEtaCut               = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False

# List of B- and C-species
genSeq.Pythia8B.BPDGCodes = [511,521,531,541,553,5122,5132,5232,5332,100553,200553,300553,-511,-521,-531,-541,-5122,-5132,-5232,-5332,
                             411,421,431,4122,4132,4232,4332,4432,4412,4422,-411,-421,-431,-4122,-4132,-4232,-4332,-4432,-4412,-4422]

genSeq.Pythia8B.NHadronizationLoops = 10
genSeq.Pythia8B.NDecayLoops         = 200

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
genSeq.Pythia8B.TriggerStateEtaCut = 3.0
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]

filtSeq.MultiMuonFilter.Ptcut              = 3500.
filtSeq.MultiMuonFilter.Etacut             = 3.0
filtSeq.MultiMuonFilter.NMuons             = 2
filtSeq.DiLeptonMassFilter.MinPt           = 3500.
filtSeq.DiLeptonMassFilter.MaxEta          = 3.0
filtSeq.DiLeptonMassFilter.MinMass         = 0.
filtSeq.DiLeptonMassFilter.MaxMass         = 10000.
filtSeq.DiLeptonMassFilter.MinDilepPt      = 0.
filtSeq.DiLeptonMassFilter.AllowSameCharge = False
