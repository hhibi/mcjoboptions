##############################################################
# Job options fragment for pp->J/psi(mu10mu10)X
# High-pT equivalent of MC15.300001.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu4mu4.py
##############################################################

evgenConfig.description   = "Inclusive pp->J/psi(mu10mu10) production with Photos"
evgenConfig.process       = "J/psi -> 2mu"
evgenConfig.keywords      = [ "charmonium", "Jpsi", "2muon", "inclusive" ]
evgenConfig.contact       = [ "pavel.reznicek@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 18.' ]

genSeq.Pythia8B.Commands += [ '443:onMode = off' ]
genSeq.Pythia8B.Commands += [ '443:2:onMode = on' ]

genSeq.Pythia8B.SignalPDGCodes = [ 443, -13,13 ]

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 10.0 ]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]
