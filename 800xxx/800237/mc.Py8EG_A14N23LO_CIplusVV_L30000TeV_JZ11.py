# JO for Pythia 8 jet jet + CI JZ11 slice with showering weights

evgenConfig.description = "Dijet+CI - JZ11, A14 NNPDF23 LO tune, withSW"
evgenConfig.process = "QCD dijet + Contact Interaction"
evgenConfig.keywords = ["exotic","QCD","contactInteraction","jets","BSM"]
evgenConfig.contact = ["matteo.bauce@cern.ch","simone.francescato@cern.ch"]
evgenConfig.nEventsPerJob = 1000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "ContactInteractions:QCqq2qq = on",
                            "ContactInteractions:QCqqbar2qqbar  = on",
                            "ContactInteractions:Lambda = 30000.",
                            "ContactInteractions:etaLL = +1",
                            "ContactInteractions:etaRR = +1",
                            "ContactInteractions:etaLR = +1",
                            "PhaseSpace:Bias2Selection = on",
                            "PhaseSpace:pTHatMin = 3500."
                            ]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy,0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(11,filtSeq)
