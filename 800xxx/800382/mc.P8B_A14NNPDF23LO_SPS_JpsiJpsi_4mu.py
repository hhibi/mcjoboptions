evgenConfig.description = "Pythia8B J/Psi J/Psi -> 4 muon in single parton scattering"
evgenConfig.process = "J/Psi J/Psi -> 4mu"
evgenConfig.keywords = ["J/Psi","4Muon","SM","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["Xin.Chen@cern.ch,Yue.Xu@cern.ch"]
evgenConfig.nEventsPerJob = 1000

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

genSeq.Pythia8B.Commands += [
        "Charmonium:gg2doubleccbar(3S1)[3S1(1)] = on,on,on",
        "Charmonium:qqbar2doubleccbar(3S1)[3S1(1)] = on,on,on",
        "Charmonium:states(3S1)1  = 443,443,100443",
        "Charmonium:states(3S1)2  = 443,100443,100443",
        "PartonLevel:MPI = on",
        "PartonLevel:ISR = on",
        "PartonLevel:FSR = on",
        "HadronLevel:all = on",
        "HadronLevel:Hadronize = on",
        "HadronLevel:Decay = on"]

genSeq.Pythia8B.Commands += [
        "100443:onMode = off",
        "100443:onIfMatch = 11 11", # ee
        "100443:onIfMatch = 13 13", # mumu
        "100443:onIfMatch = 15 15", # tautau
        "100443:onIfAny = 443", # Jpsi
        "100443:onIfAny = 445", # chi_2c
        "100443:onIfAny = 10441", # chi_0c
        "100443:onIfAny = 10443", # h_1c
        "100443:onIfAny = 20443", # chi_1c
        "100443:0:bRatio = 0.01658", # ee
        "100443:1:bRatio = 0.1231", # mumu
        "100443:2:bRatio = 0.04722", # tautau
        "100443:21:bRatio = 0.0013", # Jpsi+pi0
        "100443:22:bRatio = 0.0324", # Jpsi+eta
        "100443:43:bRatio = 0.1658", # Jpsi+2pi0
        "100443:44:bRatio = 0.3366", # Jpsi+2pi
        "100443:23:bRatio = 0.0933", # chi_2c+gamma
        "100443:35:bRatio = 0.0922", # chi_0c+gamma
        "100443:36:bRatio = 0.0008", # h_1c+pi0
        "100443:37:bRatio = 0.0907", # chi_1c+gamma
        "443:onMode = off", #
        "443:onIfMatch = 13 13"]  # Close all J/psi decays apart from J/psi->mumu

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   muonfilter3 = MultiMuonFilter("muonfilter3")
   filtSeq += muonfilter1
   filtSeq += muonfilter2
   filtSeq += muonfilter3

filtSeq.muonfilter1.Ptcut = 1500.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 4 #minimum

filtSeq.muonfilter2.Ptcut = 2500.0 #MeV
filtSeq.muonfilter2.Etacut = 2.7
filtSeq.muonfilter2.NMuons = 3 #minimum

filtSeq.muonfilter3.Ptcut = 3500.0 #MeV
filtSeq.muonfilter3.Etacut = 2.7
filtSeq.muonfilter3.NMuons = 2 #minimum

