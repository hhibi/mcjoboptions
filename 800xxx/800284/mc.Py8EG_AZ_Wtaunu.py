evgenConfig.description = "Pythia8 W -> enu events with AZ tune"
evgenConfig.keywords = ["W", "SM"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact = ["Simone Amoroso <simone.amoroso@cern.ch>"]

include("Pythia8_i/Pythia8_AZ_CTEQ6L1_EvtGen_Common.py")  
include('Pythia8_i/Pythia8_Photospp.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on",
                            "-24:onMode = off",
                            "-24:onIfAny = 15 -15",
                            "24:onMode = off",
                            "24:onIfAny = 15 -15",
                            "BeamRemnants:primordialKThard = 1.713",
                            "SpaceShower:pT0Ref = 0.586",
                            "SpaceShower:alphaSvalue = 0.12374",
                            "MultipartonInteractions:pT0Ref = 2.18",
                            "TimeShower:QEDshowerByL=off",
                            "TimeShower:QEDshowerByOther=off"]





