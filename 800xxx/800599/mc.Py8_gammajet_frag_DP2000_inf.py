include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ReducedShowerWeights.py")

evgenConfig.description = "Pythia8 dijet events with prompt (fragmentation) photons in 2000 < pT_ylead."
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 10
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch"]

## Configure Pythia
genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PhaseSpace:Bias2Selection=on", 
                            "PhaseSpace:bias2SelectionPow = 6",
                            "PhaseSpace:pTHatMin = 1000",
]



include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 2000000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
