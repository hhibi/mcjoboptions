####################################
#
# JO file for production of excited tau-leptons signal samples with Pythia8
# pp coll. -> tau + excited tau -> tau + tau + jet + jet 
#-----------------------------------

evgenConfig.description = "Single excited tau-lepton produced via contact interaction, with the A14 tune and NNPDF23LO PDF, m tauStar=4250, Lambda=10000"
evgenConfig.process = "p + p -> excited tau + tau -> tau + tau + jet + jet"
evgenConfig.keywords = ["BSM","exotic","compositeness","contact interaction","excited leptons","excited tau","excited tau-lepton","tau","jets"]
evgenConfig.contact = ["Krystsina Petukhova <kristina.mihule@cern.ch>"]
evgenConfig.generators += ["Pythia8"]

# Excited tau-lepton Monte Carlo numbering ID
leptID = 4000015
# SM tau-lepton ID
SMlepID = 15

# Excited tau-lepton Mass 
M_ExLep = 4250. # [GeV]

# Contact interaction: compositeness scale parameter (Lambda) 
M_Lam = 10000. # [GeV]

# Contact interaction: coupling constants
f = 1.0 # Strength f of the SU(2), SU(3) coupling. 
fPrime = 1.0 # Strength f' of the U(1) coupling. 
etaLL = 1.0 # Strength for CI left-left current

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["ExcitedFermion:qqbar2tauStartau = on", #Scatterings q qbar to tau^*+- tau^-+
                            str(leptID)+":m0 = "+str(M_ExLep), # Setting mass of excited tau-lepton
                            str(leptID)+':mayDecay = on',
                            
                            # switchin on only excited tau decays to quarks (via CI)
                            str(leptID)+":onMode = off",
                            str(leptID)+":onIfAll = -1 1 "+str(SMlepID),
                            str(leptID)+":onIfAll = -2 2 "+str(SMlepID),
                            str(leptID)+":onIfAll = -3 3 "+str(SMlepID),
                            str(leptID)+":onIfAll = -4 4 "+str(SMlepID),
                            str(leptID)+":onIfAll = -5 5 "+str(SMlepID),
                            str(leptID)+":onIfAll = -6 6 "+str(SMlepID),
                            
                            "ExcitedFermion:Lambda = "+str(M_Lam), # Compositeness scale
                            "ExcitedFermion:coupF = "+str(f), # Strength f of the SU(2) coupling. 
                            "ExcitedFermion:coupFprime = "+str(fPrime), # Strength f' of the U(1) coupling. 
                            "ExcitedFermion:coupFcol = "+str(f), #Strength f_c of the SU(3) coupling. 
                            "ContactInteractions:etaLL = "+str(etaLL), # Factors for left-left currents
                            
                        ]

#===================================
#
# End of job options file
#
####################################
    
