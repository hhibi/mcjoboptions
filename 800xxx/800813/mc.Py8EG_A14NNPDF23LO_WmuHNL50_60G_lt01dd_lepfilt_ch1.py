## Pythia8 W->mu+HNL

evgenConfig.description = "W->mu+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W", "exotic", "BSM"]
evgenConfig.contact = ["Monika Wielers, monika.wielers@cern.ch"]
evgenConfig.process = "W -> HNL mu+"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 60.0 0.0 0.0 0.0 0.1 0 1 0 1 0",
                            "50:isResonance = false",
                            "50:addChannel = 1 1.0 23 -13 11 -12",#decay in mu e ve
                            "50:mayDecay = on",
                            "WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:addchannel = 1 1.0 103 -13 50",
                            "ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
                            "ParticleDecays:tau0Max = 600.0"]

testSeq.TestHepMC.MaxTransVtxDisp = 200000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 1000000 #in mm
