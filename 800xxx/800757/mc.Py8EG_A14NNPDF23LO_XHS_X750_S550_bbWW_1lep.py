include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.process     = "gg->X->SH->bb+l+jets"
evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.description = "Generation of gg > X > SH where S decays to W+W- which decay to l+jets and H decays to bb"
evgenConfig.keywords = ["BSMHiggs"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'HiggsA3:parity = 1',
                            'Higgs:clipWings = off',
                            '36:m0 = 750.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 550.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes',
                            '25:onMode = off',
                            '25:onIfMatch = 5 -5',
                            '35:onMode = off',
                            '35:onIfMatch = 24 -24',
                            ]

if not hasattr( filtSeq, "DecaysFinalStateFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
  filtSeq += DecaysFinalStateFilter()
  pass

DecaysFinalStateFilter = filtSeq.DecaysFinalStateFilter
DecaysFinalStateFilter.PDGAllowedParents = [ -24, 24 ]
DecaysFinalStateFilter.NChargedLeptons = 1
