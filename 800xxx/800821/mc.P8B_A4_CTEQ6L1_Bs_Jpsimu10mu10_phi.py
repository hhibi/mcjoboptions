##############################################################
# Job options fragment for Bs->J/psi(mu10m10)phi(KK)
# High-pT equivalent of MC15.300438.Pythia8BPhotospp_A14_CTEQ6L1_Bs_Jpsimu3p5mu3p5_phi.py
##############################################################

evgenConfig.description   = "Exclusive Bs->J/psi(mu10mu10)phi(KK) decay production with Photos, flat decay angles"
evgenConfig.process       = "Bs -> J/psi(mumu) phi(KK)"
evgenConfig.keywords      = [ "bottom", "Bs", "Jpsi", "2muon", "exclusive" ]
evgenConfig.contact       = [ "pavel.reznicek@cern.ch" ]
evgenConfig.nEventsPerJob = 200

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 18.' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 18.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Define pseudo-J/psi for exclusive decay
genSeq.Pythia8B.Commands += [ '999443:all = myJ/psi void 3 0 0 3.09692 0.00009 3.09602 3.09782 0' ]  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
# Force its decay to mu+ mu-
genSeq.Pythia8B.Commands += [ '999443:onMode = off' ] 
genSeq.Pythia8B.Commands += [ '999443:oneChannel = on 1. 0 -13 13' ] # onMode bRatio meMode products
# Force Bs -> myJ/psi phi
genSeq.Pythia8B.Commands += [ '531:addChannel = 2 1. 0 999443 333' ] # onMode bRatio meMode products
# Fix Bs lifetime
genSeq.Pythia8B.Commands += [ '531:tau0 = 0.4527' ]

genSeq.Pythia8B.SignalPDGCodes = [   531, 999443,   -13,    13,   333,  321,  -321 ]
genSeq.Pythia8B.SignalPtCuts   = [   0.0,    0.0,   0.0,   0.0,   0.0,  0.8,   0.8 ] # no cuts on muons here -- apply trigger-like selection below
genSeq.Pythia8B.SignalEtaCuts  = [ 102.5,  102.5, 102.5, 102.5, 102.5,  2.6,   2.6 ] # no cuts on muons here -- apply trigger-like selection below

genSeq.Pythia8B.NHadronizationLoops = 8

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 10.0 ]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]
