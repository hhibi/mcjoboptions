######################################################################
# Graviton to e e decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 1000. GeV, kappaMG = 0.524."
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = ["sean.dean.lawlor@cern.ch"]
evgenConfig.process = "RS Graviton -> e e"
evgenConfig.nEventsPerJob = 10000

include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
"ExtraDimensionsG*:gg2G* = on",
"ExtraDimensionsG*:ffbar2G* = on",
"5100039:m0 = 1000.",
"5100039:onMode = off",
"5100039:onIfAny = 11",
"ExtraDimensionsG*:kappaMG = 0.524",
"5100039:mMin = 50.",
"PhaseSpace:mHatMin = 50.",
]

# genSeq.Pythia8.UserModes += ["GravFlat:EnergyMode = 13"]
