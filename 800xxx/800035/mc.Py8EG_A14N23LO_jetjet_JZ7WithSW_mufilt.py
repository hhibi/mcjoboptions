include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ7, with the A14 NNPDF23 LO tune, mu-filtered"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch","alex.emerman@cern.ch"]

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 950."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(7,filtSeq) 
include("GeneratorFilters/LowPtMuonFilter.py")
evgenConfig.nEventsPerJob = 1000
