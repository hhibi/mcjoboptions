# JO for Pythia 8 jet JZ4 slice with fat jet filter of 350 GeV

evgenConfig.description   = "Dijet truth jet slice JZ4 with fat jet filter of 350 GeV, with the A14 NNPDF23 LO tune"
evgenConfig.keywords      = ["QCD", "jets", "SM"]
evgenConfig.contact       = ["dominik.duda@cern.ch"]
evgenConfig.nEventsPerJob = 2000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 150."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(4,filtSeq) 

CreateJets(prefiltSeq, 1.0)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 350*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt10TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 1.0
filtSeq += HeavyFlavorBHadronFilter

