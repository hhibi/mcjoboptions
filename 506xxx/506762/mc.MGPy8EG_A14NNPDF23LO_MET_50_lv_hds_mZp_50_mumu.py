model="LightVector" 
fs = "mumu" 
mDM1 = 25. 
mDM2 = 100. 
mZp = 50. 
mHD = 125. 
filteff = 3.484321e-01 

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")