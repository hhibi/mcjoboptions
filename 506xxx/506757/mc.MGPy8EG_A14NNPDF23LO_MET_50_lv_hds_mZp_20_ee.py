model="LightVector" 
fs = "ee" 
mDM1 = 10. 
mDM2 = 40. 
mZp = 20. 
mHD = 125. 
filteff = 6.097561e-02 

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")