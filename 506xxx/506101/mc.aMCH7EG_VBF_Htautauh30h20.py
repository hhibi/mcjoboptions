from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


evgenConfig.keywords    = ['VBF', 'Higgs', 'mH125', 'resonance']
evgenConfig.contact     = ['antonio.de.maria@cern.ch']
evgenConfig.generators  = ['aMcAtNlo', 'Herwig7', 'EvtGen']
evgenConfig.description = "MG+H71 VBF at NLO"
evgenConfig.tune = "H71-Default"

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
# make sure to generate enough events to pass the filter
nevents = runArgs.maxEvents*10 if runArgs.maxEvents>0 else 10*evgenConfig.nEventsPerJob

#Used for shell commands.
import subprocess

######################################################################
# Configure event generation.
######################################################################

gridpack_mode=False
if not is_gen_from_gridpack():
    process = """
    import model loop_qcd_qed_sm_Gmu 
    define p = g u c b d s u~ c~ d~ s~ b~                                                                      
    define j = g u c b d s u~ c~ d~ s~ b~                                                                      
    generate p p > h j j $$ w+ w- z / a [QCD]  
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION



#Fetch default run_card.dat and set parameters
settings= {'ptj'           : 10,
          'jetradius'     : 1.0,
          'maxjetflavor'  : 5,
          'parton_shower' :'HERWIGPP',
          'mll_sf'        : 10.0,
          'muR_ref_fixed' : 125.0,
          'muF1_ref_fixed': 125.0,
          'muF2_ref_fixed': 125.0,
          'QES_ref_fixed' : 125.0,
          'nevents'      :int(nevents)
          }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


masses={'25': '1.250000e+02'}
params={}
params['MASS']=masses
modify_param_card(process_dir=process_dir,params=params)

# fix check_poles issue
replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
#arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True,outputDS=runName+'._00001.events.tar.gz')
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)




print "Now performing parton showering ..."
   
######################################################################
# End of event generation, start configuring parton shower here.
######################################################################

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
include("Herwig7_i/Herwig72_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
#Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="NLO")
include("Herwig7_i/Herwig71_EvtGen.py")

# To modify Higgs BR:
Herwig7Config.add_commands("""
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
 
set /Herwig/Particles/h0:NominalMass 125*GeV
set /Herwig/Particles/h0:Width 0.00407*GeV
set /Herwig/Particles/h0:WidthCut 0.00407*GeV
set /Herwig/Particles/h0:WidthLoCut 0.00407*GeV
set /Herwig/Particles/h0:WidthUpCut 0.00407*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->s,sbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 1

set /Herwig/Particles/tau+/tau+->nu_taubar,nu_e,e+;:OnOff 0
set /Herwig/Particles/tau+/tau+->nu_taubar,nu_mu,mu+;:OnOff 0
set /Herwig/Particles/tau-/tau-->nu_tau,nu_ebar,e-;:OnOff 0
set /Herwig/Particles/tau-/tau-->nu_tau,nu_mubar,mu-;:OnOff 0

set /Herwig/Particles/tau+/tau+->f_1,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,nu_taubar,[omega->pi0,gamma;];:OnOff 1
set /Herwig/Particles/tau+/tau+->phi,K+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K0,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K0,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,gamma,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,Kbar0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,Kbar0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,K-,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->eta,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K_S0,pi+,K_L0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi+,pi-,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K0,Kbar0,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi0,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,omega,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,pi+,pi-,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K_S0,K_S0,pi+,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->pi+,K_L0,K_L0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,eta,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K*+,eta,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->eta,pi+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->eta,pi+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->omega,pi+,pi+,pi-,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->omega,pi+,pi0,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau+/tau+->K+,K-,pi+,pi0,nu_taubar;:OnOff 1
set /Herwig/Particles/tau-/tau-->f_1,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,pi0,nu_tau,[omega->pi0,gamma;];:OnOff 1
set /Herwig/Particles/tau-/tau-->phi,K-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->Kbar0,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->Kbar0,pi-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,pi+,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,pi0,gamma,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,K0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,K0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K+,K-,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->eta,pi-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K_S0,pi-,K_L0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi+,pi+,pi-,pi-,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K0,Kbar0,pi-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,pi0,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,omega,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,pi+,pi-,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K_S0,K_S0,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->pi-,K_L0,K_L0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K-,eta,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K*-,eta,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->eta,pi-,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->eta,pi+,pi-,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->omega,pi+,pi-,pi-,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->omega,pi-,pi0,pi0,nu_tau;:OnOff 1
set /Herwig/Particles/tau-/tau-->K+,K-,pi-,pi0,nu_tau;:OnOff 1

do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/tau-:PrintDecayModes
do /Herwig/Particles/tau+:PrintDecayModes

""")
# run Herwig7
Herwig7Config.run()

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  had30had20filter = TauFilter("had30had20filter")
  filtSeq += had30had20filter

filtSeq.had30had20filter.UseNewOptions = True
filtSeq.had30had20filter.Ntaus = 2
filtSeq.had30had20filter.Nleptaus = 0
filtSeq.had30had20filter.Nhadtaus = 2
filtSeq.had30had20filter.EtaMaxlep = 2.6
filtSeq.had30had20filter.EtaMaxhad = 2.6
filtSeq.had30had20filter.Ptcutlep = 7000.0 #MeV
filtSeq.had30had20filter.Ptcutlep_lead = 7000.0 #MeV
filtSeq.had30had20filter.Ptcuthad = 20000.0 #MeV
filtSeq.had30had20filter.Ptcuthad_lead = 30000.0 #MeV


