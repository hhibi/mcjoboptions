evgenConfig.description = "A/H->ttbar, interference study"
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["BSMHiggs", "ttbar"]
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact  = ["katharina.behr@cern.ch", "yizhou.cai@cern.ch", "hanfei.ye@cern.ch"]

### Shower 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

### Event filter
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
