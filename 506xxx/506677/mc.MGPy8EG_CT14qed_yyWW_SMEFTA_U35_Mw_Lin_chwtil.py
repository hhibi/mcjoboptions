import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *


# ----------------------------------------------
#  Setting of EFT parameters          
# ----------------------------------------------
cw = 0.0 
cwtil = 0.0           
chdd = 0.0
chw = 0.0
chwtil = 1.0
chb = 0.0
chbtil = 0.0
chwb = 0.0
chwbtil = 0.0
chl3 = 0.0
cll1 = 0.0


# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=3*runArgs.maxEvents if runArgs.maxEvents>0 else 5500

# ----------------------------------------------
#  Actual process / MG5 run card             
# ----------------------------------------------

process = """
    import model SMEFTsim_A_U35_MwScheme_UFO
    define p = a g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate a a > w+ w- > l+ vl l- vl~ NP<=1 NP^2==1 
    output -f"""

#l+ l- l+ l- #w+ w- > l+ vl l- vl~


process_dir = new_process(process) 
#Fetch default LO run_card.dat and set parameters 
settings = {'lhe_version':'3.0', 
        'cut_decays' :'F', 
        'lpp1'      : '2',
        'lpp2'      : '2',
        'dsqrt_q2fact1' : 2., 
        'dsqrt_q2fact2' : 2.,
        'fixed_fac_scale' : 'T', 
        'use_syst':'F',
        'nevents'   : int(nevents) } 

#modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 


#==========================================================================
#Building param_card setting to 0 c_is that are not of interest  
replacefrblock= {
	'19' : 0.0,
	'20' : 0.0,
        '21' : 0.0,
        '22' : 0.0,
        '23' : 0.0,
        '24' : 0.0,
        '25' : 0.0,
        '26' : 0.0,
        '27' : 0.0,
        '28' : 0.0,
        '29' : 0.0,
        '30' : 0.0,
        '31' : 0.0,
        '32' : 0.0,
        '33' : 0.0,
        '34' : 0.0,
        '35' : 0.0,
        '36' : 0.0,
        '37' : 0.0,
        '38' : 0.0,
        '39' : 0.0,
        '40' : 0.0,
        '41' : 0.0,
        '42' : 0.0,
        '43' : 0.0,
        '44' : 0.0,
        '45' : 0.0,
        '46' : 0.0,
        '47' : 0.0,
        '48' : 0.0,
        '49' : 0.0,
        '50' : 0.0,
        '51' : 0.0,
        '52' : 0.0,
        '53' : 0.0,
        '54' : 0.0,
        '55' : 0.0,
        '56' : 0.0,
        '57' : 0.0,
        '58' : 0.0,
        '59' : 0.0,
        '60' : 0.0,
        '61' : 0.0,
        '62' : 0.0,
        '63' : 0.0,
        '64' : 0.0,
        '65' : 0.0,
        '66' : 0.0,
        '67' : 0.0,
        '68' : 0.0,
        '69' : 0.0,
        '70' : 0.0,
        '71' : 0.0,
        '72' : 0.0,
        '73' : 0.0,
        '74' : 0.0,
        '75' : 0.0,
        '76' : 0.0,
        '77' : 0.0,
        '78' : 0.0,
        '79' : 0.0,
        '80' : 0.0,
        '81' : 0.0,
        '82' : 0.0,
        '21'     : cw, #21  
        '22'  : cwtil, #22           
        '25'   : chdd, #25
        '28'    : chw, #28
        '29' : chwtil, #29
        '30'    : chb, #
        '31' : chbtil, #
        '32'   : chwb, #
        '33': chwbtil, 
        '46'   : chl3,
        '54'   : cll1
           } 

print replacefrblock

modify_param_card(process_dir=process_dir,params={'frblock' : replacefrblock}  )

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 


print_cards()

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=False)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.generators = ["MadGraph"]

runName='PROC_sm_0/run_01'  
############################
# Shower JOs will go here
#runName=

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")
    
include("Pythia8_i//Pythia8_MadGraph.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 2.6
MultiLeptonFilter.NLeptons = 2

#-------------------------------------------------------------- 
# Evgen 
#-------------------------------------------------------------- 
evgenConfig.description = 'MadGraphPy8EG_yyWW'
evgenConfig.keywords+=["SM", "Z"]
evgenConfig.contact=["kristin.lohwasser@cern.ch"]
