from MadGraphControl.MadGraphUtils import *

#############

mscard = open("madspin_card.dat",'w')

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************

#Some options (uncomment to apply)
#

 set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
 set seed %i

# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
decay w+ > l+ vl
decay w- > l- vl~
decay z > all all
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

import tarfile
if tarfile.is_tarfile(runArgs.inputGeneratorFile):
  myTarball = tarfile.open(runArgs.inputGeneratorFile)
  myEvents = None
  for afile in myTarball.getnames():
    if afile.endswith('.events'): myEvents = afile
    if myEvents is None:
      raise RuntimeError('No input events file found!')
    else:
      events_file = myTarball.extractfile( myEvents )
      madspin_on_lhe(myEvents,"madspin_card.dat",runArgs=runArgs,keep_original=True)
myTarball.close()

#### Shower
## run Pythia8 on-the-fly -----------------------------------------------------
## Provide config information
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = "MG5aMCatNLO+MadSpin+Pythia8 tW dilepton mtop=171.5 DS1"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["olga.bylund@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
