from MadGraphControl.MadGraphUtils import *
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  beamEnergy = 6500.


process_str="import model Ypp331 -modelname\ndefine l~ = e1 e2 e3\ndefine l = e1bar e2bar e3bar\ndefine p = g d d~ s s~ u u~ c c~\ngenerate p p  > ypp yppc\noutput -f"
process_dir = new_process(process_str)


# Create run card
run_card_extras = {}
run_card_extras['lhaid'] = '263000' # NNPDF30_lo_as_0130 pdf set
run_card_extras['nevents'] = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
#run_card_extras['nevents'] = 5000
run_card_extras['pdlabel'] = 'lhapdf'
#run_card_extras['lhe_version'] = '2.0'
#run_card_extras['ptheavy'] = '%s' % ptcut
modify_run_card(process_dir=process_dir, settings=run_card_extras)


# Create param_card                                                           
param_card_extras={'MASS': {'MDpm2': mBoson, 'MYpp': mBoson}}
decay_string = "1.5e+00 \n3.333e-01  2 -11 -11\n3.333e-01  2 -13 -13\n3.333e-01  2 -15 -15"
decay_br_Dpm2 = {'43': decay_string, '44': decay_string}

param_card_extras['DECAY'] = decay_br_Dpm2


modify_param_card(process_dir=process_dir,params=param_card_extras)
print_cards()


generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=2, saveProcDir=True)

#--------------------------------------------------------------
# General MC15 configuration
#--------------------------------------------------------------
include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )
include( "Pythia8_i/Pythia8_MadGraph.py" )
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Drell-Yan spin 0 Double Charged Higgs for 331 Model Mass=%s with MadGraph+Pythia8 and the A14_NNPDF23LO tune in MC16" % (mBoson)
evgenConfig.keywords = ["exotic", "drellYan", "BSM"]
evgenConfig.contact = ["antonio.sidoti@cern.ch"]
#evgenConfig.specialConfig = 'MASS=%s;preInclude=SimulationJobOptions/preInclude.Monopole.py' % (mBoson)

