import os
import glob
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.DiagramRemoval import do_MadSpin_DRX

#############

DR_mode=2
madspin_dir = 'my_madspin'

mscard = open("madspin_card.dat",'w')

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************

#Some options (uncomment to apply)
#

 set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
 set seed %i
 set ms_dir %s
 #set use_old_dir True

# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
decay w+ > l+ vl
decay w- > l- vl~
decay z > all all
# running the actual code
launch"""%(runArgs.randomSeed,madspin_dir))
mscard.close()

import tarfile
if tarfile.is_tarfile(runArgs.inputGeneratorFile):
  myTarball = tarfile.open(runArgs.inputGeneratorFile)
  myEvents = None
  for afile in myTarball.getnames():
    if afile.endswith('.events'): myEvents = afile
    if myEvents is None:
      raise RuntimeError('No input events file found!')
    else:
      events_file = myTarball.extractfile( myEvents )
      madspin_on_lhe(myEvents,"madspin_card.dat",runArgs=runArgs,keep_original=True)
myTarball.close()

#ugly hack to be able to redecay the events

for name in glob.glob("tmp_LHE_events.events*"):
  subprocess.call(["rm", name])
for origfile in glob.glob("TXT.*.events.original"):
  for newfile in glob.glob("TXT.*.events"):
    subprocess.call(["cp", origfile, newfile])
    subprocess.call(["cp", origfile, "tmp_LHE_events.events"])
    subprocess.call(["rm", origfile])

# MadSpin hack

do_MadSpin_DRX(DR_mode,madspin_dir)


# Make sure the code is taken from the madspin directory when you redo the decays
msfile='madspin_card.dat'
mstilde='madspin_card.dat~'
shutil.copyfile(msfile,mstilde)
with open(msfile,"w") as myfile, open(mstilde,'r') as f:
    for line in f:
        if '#set use_old_dir True' in line:
            line = line.replace('#',' ') #uncomment set use_old_dir True
        myfile.write(line)
os.remove(mstilde)


# Decay with the correct MadSpin code


if tarfile.is_tarfile(runArgs.inputGeneratorFile):
  myTarball = tarfile.open(runArgs.inputGeneratorFile)
  myEvents = None
  for afile in myTarball.getnames():
    if afile.endswith('.events'): myEvents = afile
    if myEvents is None:
      raise RuntimeError('No input events file found!')
    else:
      events_file = myTarball.extractfile( myEvents )
      madspin_on_lhe(myEvents,"madspin_card.dat",runArgs=runArgs,keep_original=True)
myTarball.close()



#### Shower
## run Pythia8 on-the-fly -----------------------------------------------------
## Provide config information
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = "MG5aMCatNLO+MadSpin+Pythia8 tW dilepton mtop=173.5 DR2"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["olga.bylund@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
