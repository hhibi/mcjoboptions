# EVGEN configuration
evgenConfig.description = 'MG5Py8 ttbarX to bbar'
evgenConfig.keywords+=['ttbar','jets','bbbar']
evgenConfig.contact = ['Yuan-Tang Chou <yuan-tang.chou@cern.ch>']

# Setting up variables
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphUtils import *

phys_short = get_physics_short()
evgenLog.info('phys_short = {:s}'.format(phys_short))

# Format is like MGPy8EG_A14NNPDF23_ttbarX_15_bb_lplusjet.py
# Format is like MGPy8EG_A14NNPDF23_ttbarX_15_bb_lminusjet.py
# Format is like MGPy8EG_A14NNPDF23_ttbarX_15_bb_dilep.py
data = phys_short.split('_')
generator = data[0]
tune = data[1]
mass = float(data[3])
process = data[5]

maxjetflavor=5
parton_shower='PYTHIA8'
mllcut = 4

safefactor=4
card = '''
import model Pseudoscalar_2HDM-bbMET_5FS -modelname
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > t t~ h4 [QCD]
output -f'''

decaystring=""
if process == 'lplusjet':
    decaystring = """decay t > w+ b, w+ > l+ vl
    decay t~ > w- b~, w- > j j """
elif process == 'lminusjet':
    decaystring = """decay t > w+ b, w+ > j j 
    decay t~ > w- b~, w- > l- vl~"""
elif process == 'dilep':
    decaystring = """decay t > w+ b, w+ > l+ vl 
    decay t~ > w- b~, w- > l- vl~"""
else:
    evgenLog.error('I do not know this process')

# Now do parameter/run cards.
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError('No center of mass energy found.')


#Fetch default LO run_card.dat and set parameters
nevents = runArgs.maxEvents * safefactor
extras = {
    'maxjetflavor'  : maxjetflavor,
    'ptl'           : 0.,
    'drll'          : 0.,
    'ptj'           : 1,
    'etaj'          : 10,
    'mll_sf'        : mllcut,
    'mll'           : mllcut,
    'lhe_version'   : '3.0',
    'parton_shower' : parton_shower,
    'nevents'       : int(nevents)
 }

process_dir = new_process(card)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params={}

THDMparams_DMINPUTS={}
THDMparams_DMINPUTS['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.

THDMparams_FRBlock={}
THDMparams_FRBlock['tanbeta'] = 1.0 # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams_FRBlock['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427. 

THDMparams_Higgs={}
THDMparams_Higgs['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams_Higgs['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams_Higgs['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams_Higgs['sinp'] = 0.7 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.

THDMparams_mass={}
THDMparams_mass['MXd'] = 100000 # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.
THDMparams_mass['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.
THDMparams_mass['mh2'] = 100000 # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams_mass['mh3'] = 100000 # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams_mass['mhc'] = 100000 # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams_mass['mh4'] = mass # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams_mass["5"] = 0.0 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

## auto calculation of decay width
THDMparams_decay={
"25": "0.00407",
"35": "1.0",
"36": "1.0",
"37": "1.0",
"55": "0.00407",
}

THDMparams_yukawa={
"5": "4.700000", # ymb
"6": "172.0000", # ymt
"13": "0.1057",  # ymmu
"15": "1.777000" # ymtau
}

params["FRBlock"]=THDMparams_Higgs
params["FRBlock"]=THDMparams_FRBlock
params["DMINPUTS"]=THDMparams_DMINPUTS
params["MASS"]=THDMparams_mass
params["DECAY"]=THDMparams_decay
#params["YUKAWA"]=THDMparams_yukawa

print("Updating parameters:")
print(params)
modify_param_card(params=params,process_dir=process_dir)

# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""
set BW_cut 40
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set Nevents_for_max_weight 200 # number of events for the estimate of the max. weight
set seed %i
%s
launch""" % (runArgs.randomSeed,decaystring))
mscard.close()

generate(process_dir=process_dir,runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

runArgs.inputGeneratorFile=outputDS

if tune == 'A14NNPDF23':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var1Down':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var1Up':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var2Down':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var2Up':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3aDown':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3aUp':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3bDown':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3bUp':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3cDown':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3cUp':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py')
else:
    evgenLog.error('I do not know this tune. Sorry...')
    
include('Pythia8_i/Pythia8_MadGraph.py')

genSeq.Pythia8.Commands += [
    'Zp:decayMode = 2',
    'Zp:vX = 0',
    'Zp:aX = 1',
    '55:oneChannel = 1 1.0 0 13 -13',
]

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepOnePtFilter")
filtSeq.LepOnePtFilter.IncludeHadTaus = False
filtSeq.LepOnePtFilter.NLeptons = 1
filtSeq.LepOnePtFilter.MinPt = 22000.
filtSeq.LepOnePtFilter.MaxEta = 3.
