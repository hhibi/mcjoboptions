from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


# MadGraph Config
process = """
import model CompLepton
define p = g u u~ d d~ c c~ s s~ b b~
define j = p
define l = l+ l-
define v = vl vl~
generate p p > zp > xl l, (xl > h l, h > j j)
output -f
"""

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
                settings={
                'nevents':runArgs.maxEvents*1.1,
                'lhe_version':'3.0',
		'cut_decays':'True'
                }
                )

print_cards()

params = {     'mass' : { 'MZp' : MZp,
                          'mX'  : mX },
              'DECAY' : { '1010100' : str(WZp),
                          '1010101' : str(wx),
                        }
         }

modify_param_card(process_dir=process_dir, params=params)

pdgid_file = open('pdg_extras.dat','w')
pdgid_file.write("""
1010100
1010101
""")
pdgid_file.close()
testSeq.TestHepMC.UnknownPDGIDFile='pdg_extras.dat'

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

#### Shower 

evgenConfig.description = 'p p > zp > xl l, (xl > h l, h > j j) - model Composite Lepton'  
# keywords must from list: https://svnweb.cern.ch/cern/wsvn/atlasoff/Generators/MC15JobOptions/trunk/common/evgenkeywords.txt
evgenConfig.keywords+=["exotic","BSM","WIMP"]
evgenConfig.process = 'p p > zp > xl l, (xl > h l, h > j j)'
evgenConfig.contact = ['huan.yu.meng@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")        
include("Pythia8_i/Pythia8_MadGraph.py")       


#########
