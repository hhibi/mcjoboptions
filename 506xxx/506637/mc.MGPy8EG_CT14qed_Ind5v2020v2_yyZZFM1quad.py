import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

# ----------------------------------------------
#  Setting of EFT parameters          
# ----------------------------------------------

aqgcS0=0.0
aqgcS1=0.0
aqgcS2=0.0
aqgcM0=0.0
aqgcM1=1.e-8
aqgcM2=0.0
aqgcM3=0.0
aqgcM4=0.0
aqgcM5=0.0
aqgcM6=0.0
aqgcM7=0.0
aqgcT0=0.0
aqgcT1=0.0
aqgcT2=0.0
aqgcT3=0.0
aqgcT4=0.0
aqgcT5=0.0
aqgcT6=0.0
aqgcT7=0.0
aqgcT8=0.0
aqgcT9=0.0


# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=3*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
#mode=0


process = """
    import model SM_Ltotal_Ind5v2020v2_UFO
    define p = a g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate a a > Z Z > l+ l- l+ l- M1==1 M1^2==2 
    output -f"""


process_dir = new_process(process) 
#Fetch default LO run_card.dat and set parameters 
settings = {'lhe_version':'3.0', 
        'cut_decays' :'F', 
        'lpp1'      : '2',
        'lpp2'      : '2',
        'dsqrt_q2fact1' : 2., 
        'dsqrt_q2fact2' : 2.,
        'fixed_fac_scale' : 'T',
        'ptl':'2', 
        'use_syst':'F',
        'nevents'   : int(nevents) } 

#modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 


#==========================================================================

#Building param_card setting to 0 c_is that are not of interest  
replacefrblock= {
        '1' : aqgcS0,
        '2' : aqgcS1,
        '3' : aqgcS2,
        '4' : aqgcM0,
        '5' : aqgcM1,
        '6' : aqgcM2,
        '7' : aqgcM3,
        '8' : aqgcM4,
        '9' : aqgcM5,
        '10' : aqgcM6,
        '11' : aqgcM7,
        '12' : aqgcT0,
        '13' : aqgcT1,
        '14' : aqgcT2,
        '15' : aqgcT3,
        '16' : aqgcT4,
        '17' : aqgcT5,
        '18' : aqgcT6,
        '19' : aqgcT7,
        '20' : aqgcT8,
        '21' : aqgcT9
           } 

print replacefrblock

modify_param_card(process_dir=process_dir,params={'ANOINPUTS' : replacefrblock}  )


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 


print_cards()

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=False)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.generators = ["MadGraph"]

runName='PROC_sm_0/run_01'  
############################
# Shower JOs will go here
#runName=

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")
    
include("Pythia8_i//Pythia8_MadGraph.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 2.5
MultiLeptonFilter.NLeptons = 4

#-------------------------------------------------------------- 
# Evgen 
#-------------------------------------------------------------- 
evgenConfig.description = 'MadGraphPy8EG_yyWW'
evgenConfig.keywords+=["SM", "Z"]

evgenConfig.contact=["kristin.lohwasser@cern.ch"]