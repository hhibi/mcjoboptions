include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/EW_scheme_sinthetaW_mZ.py")
include("Sherpa_i/Fusing_Fragmentation.py")

evgenConfig.description = "Sherpa Z->vv + 1,2j@NLO + 3,4,5j@LO, pTV>70 GeV"
evgenConfig.keywords = ["SM", "Z", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch","matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # Shower improvements
  NLO_SUBTRACTION_SCHEME=2;

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  # tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  # EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5

  # speed and neg weight fraction improvements
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 91 91 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Enhance_Function VAR{PPerp2(p[2]+p[3])/400.0} {3,4,5,6,7,8}
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 91 91 2.0 E_CMS
  PT2NLO 91 91 70.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppllj","pplljj", "ppllj_ew", "ppnnjj_ew"]
