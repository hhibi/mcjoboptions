include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.x, QCD: 2 jets in ME (no electro-weak processes included)"
evgenConfig.keywords = [ "jets", "multijet", "NLO", "QCD", "SM"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob   = 2000

genSeq.Sherpa_i.RunCard="""
(run){
 ACTIVE[25]=0;
 CORE_SCALE QCD;
 CSS_FS_AS_FAC 1;
 CSS_FS_PT2MIN 3;
 CSS_IS_AS_FAC 1;
 CSS_IS_PT2MIN 3;
}(run)

(processes){
  Process 93 93 -> 93 93
  Order (*,0)
  Integration_Error 0.02 {2,3};
  End process;
}(processes)

(selector){
  NJetFinder  2  1.0  0.0  0.4  -1  999.0  10.0
  NJetFinder  1  6.0  0.0  0.4  -1  999.0  10.0
}(selector)
"""
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(1,filtSeq)
