include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa llgamma + 0-3j@LO with pT_y>15 GeV, merging scale 30 GeV"
evgenConfig.keywords = ["SM", "2electron", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=3; QCUT:=30;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix;
}(run)

(processes){
  Process 93 93 -> 22 11 -11 93{NJET}
  Enhance_Observable VAR{log10(max(PPerp(p[2]+p[3]),PPerp(p[4])))}|1|2.7 {3,4,5,6} 
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PT 22  15 E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaR  22  90  0.1 1000.0
  Mass  11 -11  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 16 
