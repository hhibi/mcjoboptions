include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa lvgamma + 0,1j@NLO + 2,3j@LO with 7<pT_y"
evgenConfig.keywords = ["SM", "muon", "neutrino", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 500

genSeq.Sherpa_i.RunCard="""
(run){
  % Reduction in negative weights
  NLO_CSS_PSMODE=1

  # HT prime scale 
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=3; LJET:=3,4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 22 13 -14 93{NJET}
  Associated_Contributions EW|LO1 {LJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process

  Process 93 93 -> 22 -13 14 93{NJET}
  Associated_Contributions EW|LO1 {LJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PTNLO  22  7  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  90  91  1.0  E_CMS
  PT2NLO 90 91  90.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 24

## Include NLO EW weights
# first set consistent EW scheme
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1"]
# enable storage of associated EW contributions as OTF weights
# (they first have to be enabled in process block above)
genSeq.Sherpa_i.Parameters += [ "ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1" ]
# the following is needed for EW corrections in LO jet multiplicities
genSeq.Sherpa_i.Parameters += [ "METS_BBAR_MODE=5" ]
