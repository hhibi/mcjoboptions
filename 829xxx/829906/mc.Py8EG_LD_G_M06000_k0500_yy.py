evgenConfig.description = "Virtual graviton production in the linear dilaton model (n = 105, M = 6 TeV, k = 500 GeV, A14_NNPDF23LO)"
evgenConfig.process = "G* -> diphoton"
evgenConfig.keywords = ["BSM", "exotic", "extraDimensions", "warpedED", "graviton", "spin2"]
evgenConfig.generators += ["Lhef"]
evgenConfig.tune = "A14"
evgenConfig.notes = "NNPDF23LO"
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )
include("Pythia8_i/Pythia8_LHEF.py")

# Increase tolerance on displaced vertieces due to highly boosted heavy flavours.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000 #in mm
