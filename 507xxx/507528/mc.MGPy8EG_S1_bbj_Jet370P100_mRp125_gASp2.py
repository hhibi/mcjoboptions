mR          = 125
mDM         = 10000
gVSM        = 0.00
gASM        = 0.20
gVDM        = 0.00
gADM        = 1.00
xptj        = 100
filteff     = 0.008008
jetminpt    = 370
quark_decays= ['b']

evgenConfig.nEventsPerJob = 1000

include("MadGraphControl_MGPy8EG_DMS1_dijetjet.py")

evgenConfig.description = "Zprime with ISR - mR125 - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Chris Delitzsch <chris.malena.delitzsch>, Karol Krizka <kkrizka@cern.ch>"]

