import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re
		
# This control file makes use of UFO v2, registered in https://its.cern.ch/jira/browse/AGENE-1652 
# Z's don't contribute because sinbma (sin(beta-alpha)) is 1.0, so excluding them with '/z' reduces runtime for the LHE generation,
# without changing the physics/cross-sections/kinematic ditributions

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
if initialGluons:
	# For the gluon-gluon fusion production use the 4FS to take into account the b-quark loops
	process="""
	import model Pseudoscalar_2HDM -modelname
	define p = g d u s c d~ u~ s~ c~
	define j = g d u s c d~ u~ s~ c~
	generate g g > h1 xd xd~ / z [QCD]
	output -f
	"""
else:
	# For b-initiated production use 5FS
	process="""
	import model Pseudoscalar_2HDM-bbMET_5FS -modelname
	define p = g d u s c b d~ u~ s~ c~ b~
	define j = g d u s c b d~ u~ s~ c~ b~
	generate p p > h1 xd xd~ / z
	output -f
	"""   

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob
if decayChannel is "monoHyy": multiplier=2 # to take into account ~50% filter efficiency + safety margin (perhaps can be fine-tuned further)
if decayChannel is "monoHtautau": multiplier=5

if runArgs.maxEvents>0:
    nevents = runArgs.maxEvents * multiplier
else:
    nevents *= multiplier
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents


#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {}
if initialGluons:
  extras = {
        'maxjetflavor'  : 4,
        'asrwgtflavor'  : 4,
        'lhe_version'	: '3.0',
        'cut_decays'	: 'F',
        'nevents'     	: nevents,
    }
else:
  extras = {
        'maxjetflavor'  : 5,
        'asrwgtflavor'  : 5,
        'lhe_version'	: '3.0',
        'cut_decays'	: 'F',
        'nevents'     	: nevents,
    }

# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}
## blocks might be modified
dict_blocks={
"mass": ["MB", "MXd", "mh2", "mh3", "mhc", "mh4"],
"DMINPUTS" : ["gPXd", ],
"FRBlock": ["tanbeta", "sinbma", ],
"Higgs": ["lam3", "laP1", "laP2", "sinp"],
}

for bl in dict_blocks.keys():
  for pa in dict_blocks[bl]:
    if pa in THDMparams.keys():
      if bl not in params: params[bl]={}
      if pa=="MB": 
        params[bl]["5"]=THDMparams[pa]
      else:
        params[bl][pa]=THDMparams[pa]

## auto calculation of decay width
THDMparams_decay={
"25": "Auto",
"35": "Auto",
"36": "Auto",
"37": "Auto",
"55": "Auto",
}

params["decay"]=THDMparams_decay

print("Updating parameters:")
print(params)

modify_param_card(process_dir=process_dir,params=params)

# Build reweight_card.dat
if reweight:
	# Create reweighting card
	reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
	rwcard = open(reweight_card_loc,'w')

	for rw_name in reweights:
		rwcard.write("launch --rwgt_name=%s\n" % rw_name)
		for param in rw_name.split('-'):
			param_name, value = param.split('_')
			if param_name == "SINP":
				rwcard.write("set Higgs 5  %s\n" % value)
			elif param_name == "TANB":
				rwcard.write("set FRBlock 2  %s\n" % value)
			rwcard.write("set decay 25 Auto\nset decay 35 Auto\nset decay 36 Auto\nset decay 37 Auto\nset decay 55 Auto\n") 
	rwcard.close()

print_cards()
    
#---------------------------------------------------------------------------
# Generate the events    
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)                                                                         

#---------------------------------------------------------------------------                                                                                                                                                                       
# Metadata                                                                                                                                                          
#---------------------------------------------------------------------------
if initialGluons:
	evgenConfig.process = "g g > xd xd~ h1"
	initialStateString = "gluon fusion"
else:
	evgenConfig.process = "p p > xd xd~ h1"
	initialStateString = "b quark annihilation"

if decayChannel is "monoHyy":
	evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Higgs(-> gamma gamma) " +initialStateString + " initiated process  \
	with tan(beta) = " + str(THDMparams['tanbeta']) + ", sin(theta) = " + str(THDMparams['sinp']) + ", mA = " + str(THDMparams['mh3']) + ", ma"\
	+ str(THDMparams['mh4'])
	evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel"]
	evgenConfig.contact = ["Kristian Bjoerke <kristian.bjoerke@cern.ch>, Lailin Xu <lailin.xu@cern.ch>"]

if decayChannel is "monoHbb":
	evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Higgs(-> b bbar) " +initialStateString + " initiated process  \
	with tan(beta) = " + str(THDMparams['tanbeta']) + ", sin(theta) = " + str(THDMparams['sinp']) + ", mA = " + str(THDMparams['mh3']) + ", ma"\
	+ str(THDMparams['mh4'])
	evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel","bbbar"]
	evgenConfig.contact = ["Spyros Argyropoulos <spyridon.argyropoulos@cern.ch>, Lailin Xu <lailin.xu@cern.ch>"]

if decayChannel is "monoHtautau":
	evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Higgs(-> tau tau) " +initialStateString + " initiated process  \
	with tan(beta) = " + str(THDMparams['tanbeta']) + ", sin(theta) = " + str(THDMparams['sinp']) + ", mA = " + str(THDMparams['mh3']) + ", ma"\
	+ str(THDMparams['mh4'])
	evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel"]
	evgenConfig.contact = ["Julia Djuvsland <julia.djuvsland@cern.ch>, Lailin Xu <lailin.xu@cern.ch>"]

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false",
			    			"1000022:mayDecay = off"
                            ]

#---------------------------------------------------------------------------
# Filters
#---------------------------------------------------------------------------
if decayChannel is "monoHyy":
	# Force h->gamgam decay in Pythia
	genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 22 22 "]

	# Generator filter
	if not hasattr( filtSeq, "DiPhotonFilter" ):
		from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
		filtSeq += DiPhotonFilter ()
   
	DiPhotonFilter = filtSeq.DiPhotonFilter
	DiPhotonFilter.PtCut1st = 30000.
	DiPhotonFilter.PtCut2nd = 20000.

if decayChannel is "monoHbb":
	# Force h->bb decay in Pythia
	genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 5 -5 "]
   
	# Generator filter
	include("GeneratorFilters/MissingEtFilter.py")
	filtSeq.MissingEtFilter.METCut = 100*GeV
	filtSeq.MissingEtFilter.UseNeutrinosFromHadrons = True

if decayChannel is "monoHtautau":
	# Force h->tautau decay in Pythia
	genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 15 -15 "]
        
	include('GeneratorFilters/MultiElecMuTauFilter.py')
	MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
	MultiElecMuTauFilter.NLeptons = 2
	MultiElecMuTauFilter.MinPt = 1e10
	MultiElecMuTauFilter.MaxEta = 2.8
	MultiElecMuTauFilter.MinVisPtHadTau =  20000
	MultiElecMuTauFilter.IncludeHadTaus = 1
        

