#---------------------------------------------------
# on-the-fly generation of FV heavy Higgs MG5 events
#---------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import fnmatch
import os
import sys

# Common factor working for all processes, minimum filter eff of the order of 30%
if input_process == 'tA_tzh' or input_process == 'ttA_ttzh':
    nevents=int(2.5*runArgs.maxEvents)
else: 
    nevents=int(3.0*runArgs.maxEvents)
mode=0

# Initial pdf set NNPDF23_lo_as_0130_qed (247000)
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf': 315000, # NNPDF31_lo_as_0118 as nominal pdf set
    'pdf_variations':[315000], # NNPDF31_nnlo_as_0118 variations                                   
    'alternative_pdfs':[247000,263000,247000,315200], # NNPDF23_lo_as_0130_qed, NNPDF30_lo_as_0130, NNPDF30_lo_as_0118, NNPDF31_lo_as_0130
    'scale_variations':[0.5,1,2], # muR and muF variations (6-points scale variations)                
}

process_string=str(0)

# Input process comes from individual JO
if input_process == 'tS_ttt':
    process_string='generate p p > t S0, (S0 > t t~, (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec)), (t > b w+, w+ > wdec wdec)'
elif input_process == 'tS_ttq':
    process_string='generate p p > t S0, (S0 > t qdec, (t > b w+, w+ > wdec wdec)), (t > b w+, w+ > wdec wdec) \n add process p p > t S0, (S0 > t~ qdec, (t~ > b~ w-, w- > wdec wdec)) , (t > b w+, w+ > wdec wdec)'
elif input_process == 'tbarS_ttt':
    process_string='generate p p > t~ S0, (S0 > t t~, (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec)), (t~ > b~ w-, w- > wdec wdec)'
elif input_process == 'tbarS_ttq':
    process_string='generate p p > t~ S0, (S0 > t qdec, (t > b w+, w+ > wdec wdec)), (t~ > b~ w-, w- > wdec wdec) \n add process p p > t~ S0, (S0 > t~ qdec, (t~ > b~ w-, w- > wdec wdec)) , (t~ > b~ w-, w- > wdec wdec)'
elif input_process == 'tA_tzh':
    process_string='generate p p > t A0, (A0 > z h, (z > zdec zdec)), (t > b w+, w+ > wdec wdec) \n add process p p > t~ A0, (A0 > z h, (z > zdec zdec)), (t~ > b~ w-, w- > wdec wdec)'
elif input_process == 'ttS_tttq':
    process_string='generate p p > t t~ S0, (S0 > t qdec, (t > b w+, w+ > wdec wdec)), (t~ > b~ w-, w- > wdec wdec), (t > b w+, w+ > wdec wdec)  \n add process p p > t t~ S0, (S0 > t~ qdec, (t~ > b~ w-, w- > wdec wdec)), (t~ > b~ w-, w- > wdec wdec), (t > b w+, w+ > wdec wdec)'
elif input_process == 'ttA_ttzh':
    process_string='generate p p > t t~ A0, (A0 > z h, (z > zdec zdec)), (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec)'
elif input_process == 'ttS_tttt':
    process_string='generate p p > t t~ S0, (S0 > t t~ , (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec)), (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec)'
elif input_process == 'sstt':
    process_string='generate p p > t t, (t > b w+, w+ > wdec wdec) \n add process p p > t~ t~, (t~ > b~ w-, w- > wdec wdec)'
elif input_process == 'tA_ttt':
    process_string='generate p p > t A0, (A0 > t t~, (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec)), (t > b w+, w+ > wdec wdec)'
else: 
    raise RuntimeError("Process %s not recognised in these jobOptions."%input_process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_str="""
set group_subprocesses Auto
set ignore_six_quark_processes False   
set loop_optimized_output True
set complex_mass_scheme False
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~       
define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~   
define zdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~   
define qdec = u c u~ c~
import model gen2HDM_UFO --modelname              
"""+process_string+"""
output -f
"""

#--------------------------------------------------------------
# Masses in GeV, plus model specific couplings
#--------------------------------------------------------------
###
# Parameters in the model parsed via JO
# kAZH, rtu, rtc, rtt, MS0, MA
model_pars_str = str(jofile) 
kAZH=int(0) 
rtu=int(0) 
rtc=int(0) 
rtt=int(0) 
lSHH=int(0) 
MS=int(15000)
MA=int(15000)

for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    if 'MS' in s:
        ss=s.replace("MS","")  
        if ss.isdigit():
            MS = int(ss)        
            print  "BSM Higgs mass S0 set to %i"%MS
    if 'MA' in s:
        ss=s.replace("MA","")  
        if ss.isdigit():
            MA = int(ss)        
            print  "BSM Higgs mass A0 set to %i"%MA
    if 'AZH' in s:
        ss=s.replace("AZH","")  
        if ss.isdigit():
            kAZH = int(ss)        
            print  "AZH coupling set to %i"%kAZH
    if 'tu' in s:
        ss=s.replace("tu","")  
        if ss.isdigit():
            rtu = int(ss)        
            print  "tu coupling set to %i"%rtu
    if 'tc' in s:
        ss=s.replace("tc","")  
        if ss.isdigit():
            rtc = int(ss)        
            print  "tc coupling set to %i"%rtc
    if 'tt' in s:
        ss=s.replace("tt","")  
        if ss.isdigit():
            rtt = int(ss)        
            print  "rtt coupling set to %i"%rtt
    if 'lSHH' in s:
        ss=s.replace("lSHH","")
        if ss.isdigit():
            lSHH = int(ss)
            print  "lSHH coupling set to %i"%lSHH

extras  = { 'lhe_version':'2.0',
            'cut_decays':'F',
            'scale':'125',
            'dsqrt_q2fact1':'125',
            'dsqrt_q2fact2':'125',
            'nevents' :int(nevents) }

# Hack to avoid shower getting stuck .. 
#if input_process == 'ttA_ttzh' and MA == 600:
#    nevents=int(3.0*runArgs.maxEvents)

process_dir = new_process(process_str)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

c_fact=0.1
masses={'35':str(MS)+' # MS0','36':str(MA)+' # MA0'}    
parameters={'kAZH': c_fact*int(kAZH), 'rtu': c_fact*int(rtu), 'rtc': c_fact*int(rtc), 'rtt': c_fact*int(rtt)} 
params={}
params['MASS']=masses
params['FRBlock']=parameters
modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Lepton filter
if not hasattr(filtSeq,"LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut = 15000.0#MeV

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.description = 'MadGraph control flavour violating Higgs'
evgenConfig.keywords+=['Higgs','BSMHiggs']
evgenConfig.contact = ['Nicola Orlando <nicola.orlando@cern.ch>']
