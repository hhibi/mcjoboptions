doNLO=0    # for EFT only, 0: LO, 1: NLO
dogg=1     # production mode, 0: pp, 1: gg
doZZ=-1     # 0: off-shell, 1: on-shell ZZ, ZZ->4l/2l2v, 2: h->ZZ, 3: gg->Z no H; -1: gg->H->4l
do2l2v=1   # 0: 4l, 1: 2l2v

QCDmode=None # QCD order: None, for LO; "[QCD]", for NLO; "[noborn=QCD]", for loop-induced only

EFTorder=2 # 0 for SM only, 1 for EFT linear term, 2 for EFT quadratic term, 3 for all
EFTop="cpG" # EFT parameter name
EFTval='1.0'   # value of the EFT parameter, 1 by default 

fixModel=0    # turn off cpG via the restriction card

doReweight=2  # 0, no reweighting, 1: linear terms only; 2: quad terms only; 3: both linear and quad terms
              # format is [ 'paramName1_paraValue1', ...] for 1D reweighting
              # or [ 'paramName1_paraValue1--paramName2_paraValue2', ...] for 2D reweighting, etc
reweights=['cpG_0.0000000001--ctp_10000000000.0']

include("MadGraphControl_Pythia8EvtGen_SMEFTNLO_ZZ.py")
