#These three lines are needed for r21.
from MadGraphControl.MadGraphUtils import * 
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')  #this name is different from the r19 PreInclude.py
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short #get_physics_short is the r21 was to return the name of the file

JOName = get_physics_short()
jobConfigParts = JOName.split('_')

run_settings.update({'time_of_flight':1E-25, 'event_norm':'average'}) 

masses['1000024'] = float(jobConfigParts[4])
masses['1000023'] = float(jobConfigParts[4])
masses['1000022'] = float(jobConfigParts[5])

if masses['1000022']<0.5: masses['1000022']=0.5
gentype = str(jobConfigParts[2])
decaytype = str(jobConfigParts[3])
process = """
import model MSSM_SLHA2
generate p p > x1+ n2 / susystrong @1
add process p p > x1- n2 / susystrong @1
add process p p > x1+ n2 j / susystrong @2
add process p p > x1- n2 j / susystrong @2
add process p p > x1+ n2 j j / susystrong @3
add process p p > x1- n2 j j / susystrong @3
output -f -nojpeg
"""

njets = 2
evgenLog.info('Registered generation of ~chi1+/- ~chi20 production, decay via Wh; mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))

evgenConfig.contact  = [ "risa.ushioda@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'gaugino', 'chargino', 'neutralino']
evgenConfig.description = '~chi1+/- ~chi20 production, decay via W and h with displaced vertex'

keepOutput=True

if 'p01ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 0.01ns, decay via Higgs')
  N2Width = 6.58211928E-14
  decays ={'1000023':'''DECAY 1000023 6.58211928E-14 #neutralino2 lifetime ~ 0.01ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''',
    '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''',
     '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''}

elif 'p03ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 0.03ns, decay via Higgs')
  N2Width = 2.19403976E-14
  decays ={'1000023':'''DECAY 1000023 2.19403976E-14 #neutralino2 lifetime ~ 0.03ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''', 
    '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''', 
     '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''}

elif 'p1ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 0.1ns, decay via Higgs')
  N2Width = 6.58211928E-15
  decays ={'1000023':'''DECAY 1000023 6.58211928E-15 #neutralino2 lifetime ~ 0.1ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''',   
    '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''',
    '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''}

elif 'p3ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 0.3ns, decay via Higgs')
  N2Width = 2.19403976E-15
  decays ={'1000023':'''DECAY 1000023 2.19403976E-15 #neutralino2 lifetime ~ 0.3ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''',
    '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''',
    '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''}

elif '1ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 1ns, decay via Higgs')
  N2Width = 6.58211928E-16
  decays ={'1000023':'''DECAY 1000023 6.58211928E-16 #neutralino2 lifetime ~ 1ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''',
     '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''',
    '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''}

elif '3ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 3ns, decay via Higgs')
  N2Width = 2.19403976E-16
  decays ={'1000023':'''DECAY 1000023 2.19403976E-16 #neutralino2 lifetime ~ 3ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''', 
    '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''',
    '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''} 


elif '10ns' in jobConfigParts[6]:
  evgenLog.info('lifetime of 1000023 is set as 10ns, decay via Higgs')
  N2Width = 6.58211928E-17
  decays ={'1000023':'''DECAY 1000023 6.58211928E-17 #neutralino2 lifetime ~ 10ns
  # BR  NDA ID1   ID2
     1.00000000E+00    2     1000022        25   #BR(~chi_20 -> ~chi_10 h)
     0.00000000E+00    3     1000022        11     -11   # BR(~chi_20 -> ~chi_10 e+ e-)
  #
     ''', 
    '25':'''DECAY 25 2.00000000E+00   # h decays (width is reduced by a factor of 100 to allow for off-shell decay)
  # BR      NDA ID1   ID2
     1.000000000E+00   2     5        -5   # BR(H1 -> bb)
  #
     ''',
    '1000024':'''DECAY 1000024 1.70414503E-02   # chargino1+ decays
  # BR  NDA ID1   ID2  
     1.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11   12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
  #
     '''} 

genSeq.Pythia8.Commands += ["23:mMin = 0.2"]
genSeq.Pythia8.Commands += ["24:mMin = 0.2"]
genSeq.Pythia8.Commands += ["25:mMin = 0.2"]

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
  genSeq.Pythia8.Commands += [ "Merging:Process = guess", "1000024:spinType = 1", "1000023:spinType = 1" ] 
  genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm                                                     
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000
