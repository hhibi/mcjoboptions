#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------
THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] = 1
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427. 
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.350000
THDMparams['MXd'] = 150.000000
THDMparams['mh2'] = 600
THDMparams['mh3'] = 600
THDMparams['mhc'] = 600
THDMparams['mh4'] = 250
THDMparams['MB'] = 4.7 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

initialGluons = True

# Reweighting in tanb and sinp
reweight = False # If set to True additional OTF weights are written in the LHE file, if False no additional weights written
reweights=[
'SINP_0.35-TANB_0.3',
'SINP_0.35-TANB_0.5',
'SINP_0.35-TANB_1.0',
'SINP_0.35-TANB_2.0',
'SINP_0.35-TANB_3.0',
'SINP_0.35-TANB_5.0',
'SINP_0.35-TANB_10.0',
'SINP_0.35-TANB_20.0',
'SINP_0.7-TANB_0.3',
'SINP_0.7-TANB_0.5',
'SINP_0.7-TANB_1.0',
'SINP_0.7-TANB_2.0',
'SINP_0.7-TANB_3.0',
'SINP_0.7-TANB_5.0',
'SINP_0.7-TANB_10.0',
'SINP_0.7-TANB_20.0',
]

# Define that we want H->bb decays
decayChannel="monoHbb"

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob=5000

# Event multiplier
multiplier=5

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------    
include("MadGraphControl_Py8EG_2HDMa_monoH_common.py")

